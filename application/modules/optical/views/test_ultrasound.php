<?php

if(!empty($service_charge_id))
{
	$rs = $this->optical_model->get_ultrasound_visit($visit_id, $service_charge_id);
	$num_visit = count($rs);
	
	
	$this->optical_model->save_ultrasound_visit($visit_id, $service_charge_id,$charge_date);
	
}
$rs = $this->optical_model->get_ultrasound_visit2($visit_id);
$num_rows = count($rs);

echo "

<table class='table table-striped table-hover table-condensed'>
	<tr>
		<th>No.</th>
		<th>Date Done</th>
		<th>Time</th>
    	<th>Service</th>
		<th>Units</th>
		<th>Unit Cost</th>
		<th>Total Ksh.</th>
		<th></th>
		<th></th>
	</tr>
	<tbody>
";

$total = 0;
$s=0;
foreach ($rs as $key6):
	
	$visit_charge_id = $key6->visit_charge_id;
	$test = $key6->service_charge_name;
	$service_charge_id = $key6->service_charge_id;
	$price = $key6->visit_charge_amount;
	$date_done = $key6->date;
	$time = $key6->time;
	$units = $key6->visit_charge_units;

	$visit_date = date('l d F Y',strtotime($date_done));
	$visit_time = date('H:i A',strtotime($time));
	$service_charge_id = $key6->service_charge_id;
	$charge = $price*$units;
	$total = $total + $charge;
	$s++;
	echo "
		<tr>
        	<td>".($s)."</td>
        	<td>".$visit_date."</td>
        	<td>".$visit_time."</td>
			<td>".$test."</td>
			<td align='center'>
				<input type='text' id='units".$visit_charge_id."' class='form-control' value='".$units."' size='3' />
			</td>
			<td align='center'><input type='text' id='unit_price".$visit_charge_id."' class='form-control'  size='5' value='".$price."'></div></td>

			<td >Ksh. ".number_format($price*$units)."</td>
			<td>
			<a class='btn btn-sm btn-primary'  onclick='calculatetotal(".$price.",".$visit_charge_id.", ".$service_charge_id.",".$visit_id.")'><i class='fa fa-pencil'></i></a>
			</td>
			<td>
				<a class='btn btn-sm btn-danger'  onclick='delete_ultrasound_cost(".$visit_charge_id.", ".$visit_id.")'><i class='fa fa-trash'></i></a>
			</td>
		</tr>
	";

endforeach;
$ultrasound_visit = 0;
echo "
	<tr bgcolor='#D9EDF7'>
		<th colspan='6'>Total</th>
		<th align='center'> Ksh. ".number_format($total, 2)."</th>
		<th colspan='2'></th>
	</tr>
	";

echo "
</tbody>
</table>

";

?>