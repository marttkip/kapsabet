<?php
class Optical_model extends CI_Model 
{
	function submitvisitbilling($procedure_id,$visit_id,$suck){
		$visit_data = array('procedure_id'=>$procedure_id,'visit_id'=>$visit_id,'units'=>$suck);
		$this->db->insert('visit_procedure', $visit_data);
	}



	public function get_optical($table, $where,$order)
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('service_charge.service_charge_amount, service_charge.service_charge_id, service_charge.service_charge_name');
		$this->db->where($where);
		$this->db->order_by($order,'asc');
		$query = $this->db->get('');
		
		return $query;
	}


	function get_ultrasound_visit($visit_id, $service_charge_id=NULL){
		$table = "visit_charge";
		$where = "visit_charge_delete = 0 AND visit_id = ". $visit_id ." AND service_charge_id = ". $service_charge_id;
		
		$items = "*";
		$order = "visit_id";
		
		$result = $this->database->select_entries_where($table, $where, $items, $order);
		
		return $result;
		
	}

	function save_ultrasound_visit($visit_id, $service_charge_id,$charge_date)
	{
		if(empty($charge_date))
		{
			$charge_date = date('Y-m-d');
		}

		$table = "service_charge";
		$where = "service_charge_id = ". $service_charge_id;
		$items = "service_charge_amount,product_id";
		$order = "service_charge_id";
		
		$result = $this->database->select_entries_where($table, $where, $items, $order);
		$service_charge_amount = 0;
		$product_id = 0;
		if(count($result) > 0)
		{
			foreach ($result as $key): 
				$service_charge_amount = $key->service_charge_amount;
				$product_id = $key->product_id;
			endforeach;
		}

		$visit_data = array(
			'visit_id'=>$visit_id,
			'service_charge_id'=>$service_charge_id,
			'visit_charge_amount'=>$service_charge_amount,
			'date'=>$charge_date,
			'charged'=>1,
			'product_id'=>$product_id,
			'time'=>date('H:i:s'),
			'created_by'=>$this->session->userdata("personnel_id")
		);
		if($this->db->insert('visit_charge', $visit_data))
		{
			return TRUE;
		}
		
		else
		{
			return FALSE;
		}
	}

	Public function get_ultrasound_visit2($visit_id)
		{
		$table = "visit_charge, service_charge, service";
		$where = 'visit_charge_delete = 0 AND service.service_id = service_charge.service_id AND service.service_delete = 0  AND (service.service_name = "Optical" OR service.service_name = "Optical" )
		  AND visit_charge_delete = 0 AND service_charge.service_charge_id = visit_charge.service_charge_id AND visit_charge.visit_id = '.$visit_id;
		$items = "*";
		$order = "visit_charge.date,visit_charge.time";
		
		$result = $this->database->select_entries_where($table, $where, $items, $order);
		
		return $result;
		
	}

}
?>