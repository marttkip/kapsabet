<section class="panel">
	<div class="row">
		<?php if ($module == 0){?>
		<div class="col-md-2">
			<div class="center-align">
				<?php echo form_open("nurse/send_to_doctor/".$visit_id, array("class" => "form-horizontal"));?>
				<input type="submit" class="btn btn-large btn-primary" value="Send To Doctor" onclick="return confirm('Send to Doctor?');"/>
				<?php echo form_close();?>
			</div>
		</div>
		<?php } ?>
		<div class="col-md-2">
			<div class="center-align">
				<?php echo form_open("nurse/send_to_pharmacy/".$visit_id."/".$module, array("class" => "form-horizontal"));?>
				<input type="submit" class="btn btn-large btn-warning center-align" value="Send To Pharmacy" onclick="return confirm('Send to Pharmacy?');"/>
				<?php echo form_close();?>
			</div>
		</div>
		<div class="col-md-2">
			<div class="center-align">
				<?php echo form_open("nurse/send_to_labs/".$visit_id."/".$module, array("class" => "form-horizontal"));?>
				<input type="submit" class="btn btn-large btn-success center-align" value="Send To Laboratory" onclick="return confirm('Send to Laboratory?');"/>
				<?php echo form_close();?>
			</div>
		</div>
		<div class="col-md-2">
			<div class="center-align">
				<?php echo form_open("nurse/send_to_accounts/".$visit_id."/".$module, array("class" => "form-horizontal"));?>
				<input type="submit" class="btn btn-large btn-danger center-align" value="Send To Accounts" onclick="return confirm('Send to Accounts?');"/>
				<?php echo form_close();?>
			</div>
		</div>
		<div class="col-md-2">
			<div class="center-align">
				<?php echo form_open("nurse/send_to_xray/".$visit_id."/".$module, array("class" => "form-horizontal"));?>
				<input type="submit" class="btn btn-large btn-primary" value="Send To Xray" onclick="return confirm('Send to X-Ray?');"/>
				<?php echo form_close();?>
			</div>
		</div>
		<div class="col-md-2">
			<div class="center-align">
				<?php echo form_open("nurse/send_to_ultrasound/".$visit_id."/".$module, array("class" => "form-horizontal"));?>
				<input type="submit" class="btn btn-large btn-default" value="Send To Ultrasound" onclick="return confirm('Send to Ultrasound?');"/>
				<?php echo form_close();?>
			</div>
		</div>
	</div>
</section>

<section class="panel">
	<header class="panel-heading">
		<h2 class="panel-title">Patient card</h2>
	</header>
	
	<!-- Widget content -->
	
	<div class="panel-body">
		
		<div class="well well-sm info">
			<h5 style="margin:0;">
				<div class="row">
					<div class="col-md-2">
						<div class="row">
							<div class="col-lg-6">
								<strong>First name:</strong>
							</div>
							<div class="col-lg-6">
								<?php echo $patient_surname;?>
							</div>
						</div>
					</div>
					
					<div class="col-md-3">
						<div class="row">
							<div class="col-lg-6">
								<strong>Other names:</strong>
							</div>
							<div class="col-lg-6">
								<?php echo $patient_othernames;?>
							</div>
						</div>
					</div>
					
					<div class="col-md-2">
						<div class="row">
							<div class="col-lg-6">
								<strong>Gender:</strong>
							</div>
							<div class="col-lg-6">
								<?php echo $gender;?>
							</div>
						</div>
					</div>
					
					<div class="col-md-2">
						<div class="row">
							<div class="col-lg-6">
								<strong>Age:</strong>
							</div>
							<div class="col-lg-6">
								<?php echo $age;?>
							</div>
						</div>
					</div>
					
					<div class="col-md-3">
						<div class="row">
							<div class="col-lg-6">
								<strong>Account balance:</strong>
							</div>
							<div class="col-lg-6">
								Kes <?php echo number_format($account_balance, 2);?>
							</div>
						</div>
					</div>
				</div>
			</h5>
		</div>
        
        <div class="center-align">
          	<?php
              	$error = $this->session->userdata('error_message');
              	$validation_error = validation_errors();
				$success = $this->session->userdata('success_message');
				
				if(!empty($error))
				{
					echo '<div class="alert alert-danger">'.$error.'</div>';
					$this->session->unset_userdata('error_message');
				}
				
				if(!empty($validation_error))
				{
					echo '<div class="alert alert-danger">'.$validation_error.'</div>';
				}
				
				if(!empty($success))
				{
					echo '<div class="alert alert-success">'.$success.'</div>';
					$this->session->unset_userdata('success_message');
				}
      		?>
            <?php echo $this->load->view("nurse/allergies_brief", '', TRUE);?>
			<div class="clearfix"></div>
        </div>
		<nav class="navbar navbar-default">
        	<div class="container">
            	<div class="navbar-header">
                  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                  </button>
                </div>
                <ul class="nav nav-tabs nav-justified patient-card-nav">
                    <li><a href="<?php echo site_url().'medical_record/vitals/'.$visit_id.'/'.$patient_id;?>">Vitals</a></li>
                    <li><a href="<?php echo site_url().'medical_record/lifestyle/'.$visit_id.'/'.$patient_id;?>">Lifestyle</a></li>
                    <li><a href="<?php echo site_url().'medical_record/previous_vitals/'.$visit_id.'/'.$patient_id;?>">Previous Vitals</a></li>
                    <li><a href="<?php echo site_url().'medical_record/patient_history/'.$visit_id.'/'.$patient_id;?>">Patient History</a></li>
                    <li><a href="<?php echo site_url().'medical_record/soap/'.$visit_id.'/'.$patient_id;?>">SOAP</a></li>
                    <li><a href="<?php echo site_url().'medical_record/leave/'.$visit_id.'/'.$patient_id;?>">Leave</a></li>
                    <li><a href="<?php echo site_url().'medical_record/medical_checkup/'.$visit_id.'/'.$patient_id;?>">Medical Checkup</a></li>
                    <li><a href="<?php echo site_url().'medical_record/visit_trail/'.$visit_id.'/'.$patient_id;?>">Visit Trail</a></li>
                </ul>
                
            </div>
        </nav>
        <div class="wrap-loading">
    		<div class="loading loading-4"></div>
		</div>
        <div id="patient-card-nav-display"></div>
    </div>
</section>
<div class="row">
		<?php if ($module == 0){?>
		<div class="col-md-2">
			<div class="center-align">
				<?php echo form_open("nurse/send_to_doctor/".$visit_id, array("class" => "form-horizontal"));?>
				<input type="submit" class="btn btn-large btn-primary" value="Send To Doctor" onclick="return confirm('Send to Doctor?');"/>
				<?php echo form_close();?>
			</div>
		</div>
		<?php } ?>
		<div class="col-md-2">
			<div class="center-align">
				<?php echo form_open("nurse/send_to_pharmacy/".$visit_id."/".$module, array("class" => "form-horizontal"));?>
				<input type="submit" class="btn btn-large btn-warning center-align" value="Send To Pharmacy" onclick="return confirm('Send to Pharmacy?');"/>
				<?php echo form_close();?>
			</div>
		</div>
		<div class="col-md-2">
			<div class="center-align">
				<?php echo form_open("nurse/send_to_labs/".$visit_id."/".$module, array("class" => "form-horizontal"));?>
				<input type="submit" class="btn btn-large btn-success center-align" value="Send To Laboratory" onclick="return confirm('Send to Laboratory?');"/>
				<?php echo form_close();?>
			</div>
		</div>
		<div class="col-md-2">
			<div class="center-align">
				<?php echo form_open("nurse/send_to_accounts/".$visit_id."/".$module, array("class" => "form-horizontal"));?>
				<input type="submit" class="btn btn-large btn-danger center-align" value="Send To Accounts" onclick="return confirm('Send to Accounts?');"/>
				<?php echo form_close();?>
			</div>
		</div>
		<div class="col-md-2">
			<div class="center-align">
				<?php echo form_open("nurse/send_to_xray/".$visit_id."/".$module, array("class" => "form-horizontal"));?>
				<input type="submit" class="btn btn-large btn-primary" value="Send To Xray" onclick="return confirm('Send to X-Ray?');"/>
				<?php echo form_close();?>
			</div>
		</div>
		<div class="col-md-2">
			<div class="center-align">
				<?php echo form_open("nurse/send_to_ultrasound/".$visit_id."/".$module, array("class" => "form-horizontal"));?>
				<input type="submit" class="btn btn-large btn-default" value="Send To Ultrasound" onclick="return confirm('Send to Ultrasound?');"/>
				<?php echo form_close();?>
			</div>
		</div>
	</div>
<script type="text/javascript">
  	var config_url = document.getElementById("config_url").value;
	
	$(document).ready(function()
	{
		var nav_link = "<?php echo site_url().'medical_record/vitals/'.$visit_id.'/'.$patient_id;?>";
		$.get(nav_link, function( data ) 
		{
			$("#patient-card-nav-display").html(data);
			$(".wrap-loading").addClass('display-none');
		});
	});
	
	$(document).on("click",".patient-card-nav a",function(e)
	{
		e.preventDefault();
		$(".wrap-loading").removeClass('display-none');
		var nav_link = $(this).attr('href');
		$.get(nav_link, function( data ) 
		{
			$("#patient-card-nav-display").html(data);
			$(".wrap-loading").addClass('display-none');
		});
		
		return false;
	});
</script>