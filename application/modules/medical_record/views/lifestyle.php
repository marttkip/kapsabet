<?php
$data['visit_id'] = $visit_id;
?>
<div class="row">
    <div class="col-md-12">
		<!-- Widget -->
        <section class="panel panel-featured panel-featured-info">
            <header class="panel-heading">
                <h2 class="panel-title">Nurse Notes</h2>
            </header>
            <div class="panel-body">
                  <div class="padd">
    				<?php
    				$visit_data1['visit_id'] = $visit_id;
    				?>
    				<?php echo $this->load->view('soap/nurse_notes',$visit_data1, TRUE); ?>
    				<!-- end of vitals data -->
    			</div>
            </div>
        </section>
    </div>
</div>
