<!-- end #header -->
<section class="panel">
    <header class="panel-heading">
        <div class="panel-title">
        	<?php echo $title;?>
		</div>
		<div class="pull-right">
			
		</div>
    </header>

    <div class="panel-body">
		
		
<div class="row">
  <div class="col-md-12">
  		<?php
  		
  			$personnel_id = $this->session->userdata('personnel_id');
			$is_admin = $this->reception_model->check_if_admin($personnel_id,1);


			$is_phamacist = $this->reception_model->check_if_admin($personnel_id,27);

			$is_accountant = $this->reception_model->check_if_admin($personnel_id,28);

			if(($is_admin OR $personnel_id == 0) OR $is_phamacist)
			{
				$visit_id_search = $this->session->userdata('visit_id');
		  		// var_dump($visit_id_search); die();
		  		if($visit_id_search)
		  		{
  				?>
  				<section class="panel panel-featured panel-featured-info">
		            <header class="panel-heading">
		                <h2 class="panel-title">Prescription</h2>
		            </header>
		            <div class="panel-body">
		                <div class="col-lg-8 col-md-8 col-sm-8">
		                  <div class="form-group">
		                    <select id='drug_id' name='drug_id' class='form-control custom-select ' onchange="get_drug_to_prescribe(<?php echo $visit_id;?>);">
		                      <option value=''>None - Please Select an drug</option>
		                      <?php echo $drugs;?>
		                    </select>
		                  </div>
		                
		                </div>
		            </div>
		            <div id="prescription_view"></div>
		            <div id="visit_prescription"></div>
					<div class="center-align">
					<?php echo '<a href="'.site_url().'send-pharmarcy" onclick="return confirm(\'Send to accounts?\');" class="btn btn-sm btn-success">Send to Accounts</a>';?>
					<?php echo '<a href="'.site_url().'pharmacy/print-prescription/'.$visit_id.'" class="btn btn-sm btn-warning print_prescription" target="_blank"><i class="fa fa-print"></i> Print Prescription</a>';?>

				 	</div>
		    	</section>
		    <?php
			}
			else
			{
				?>
	  			<section class="panel">
	  				<div class="panel-body">
	  					<div class="row">
						  	<div class="col-md-12 center-align">
	  							<a href="<?php echo site_url();?>create-walkin-visit" class="btn btn-info btn-md " onclick="return confirm('Do you want to start a new walkin visit ?')"><i class="fa fa-plus"></i> New Walkin </a>
	  						</div>
	  					</div>
	  				</div>
	  			</section>

	  			<?php
			}
		}

		if(($is_admin OR $personnel_id == 0) OR $is_accountant)
		{
			if($accounts_mode == TRUE)
			{
				?>
			    	<section class="panel panel-featured panel-featured-info">
				    	<div class="col-md-12">
							<div class="row">
								<header class="panel-heading">						
									<div id="page_header"></div>
								</header>
							</div>
							<div class="row">
								<div id="patient_bill"></div>
							</div>
						</div>
					</section>
				<?php

			}
			else
			{

				?>
				<section class="panel">
	  				<div class="panel-body">
	  					<div class="row">
						  	<div class="col-md-12 center-align">
	  							<a href="<?php echo site_url();?>walkins" class="btn btn-info btn-md " ><i class="fa fa-plus"></i> REFRESH TO CHECK FOR NEW WALKIN </a>
	  						</div>
	  					</div>
	  				</div>
	  			</section>
				<?php

			}
		
		}
		?>
  			
  		
    </div>
</div>


<div class="modal fade bs-example-modal-lg" id="add_payment_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Add to Bill</h4>
            </div>
             <?php echo form_open("accounts/add_accounts_personnel", array("class" => "form-horizontal","id"=>"add_payment"));?>
            <div class="modal-body">
            	<div class="row">
            		<div class="col-md-2 ">
            		</div>
		            	<div class="col-md-10 ">
		            		<div class="form-group" style="display: none;">
								<div class="col-lg-4">
                                	<div class="radio">
                                        <label>
                                            <input id="optionsRadios2" type="radio" name="type_payment" id="type_payment" value="1" checked="checked" onclick="getservices(1)"> 
                                            Normal
                                        </label>
                                    </div>
								</div>
								<div class="col-lg-4" style="display: none;">
                                	<div class="radio">
                                        <label>
                                            <input id="optionsRadios2" type="radio" name="type_payment" id="type_payment" value="2" onclick="getservices(2)"> 
                                            Debit Note
                                        </label>
                                    </div>
								</div>
								<div class="col-lg-4" style="display: none;">
                                	<div class="radio">
                                        <label>
                                            <input id="optionsRadios2" type="radio" name="type_payment" id="type_payment" value="3" onclick="getservices(3)"> 
                                            Credit Note
                                        </label>
                                    </div>
								</div>
							</div>
							 
                           	<input type="hidden" name="type_payment" value="1">
                           	<input type="hidden" name="service_id" id="service_id" value="0">
							
                            

							<div class="form-group">
								<label class="col-lg-2 control-label">Amount: </label>
							  
								<div class="col-lg-8">
									<input type="text" class="form-control" name="amount_paid" id="amount_paid" placeholder="" autocomplete="off"  onkeyup="get_change()">
								</div>
							</div>
							<input type="hidden" class="form-control" name="change_payment" id="change_payment" placeholder="" autocomplete="off" >
							
							<div class="form-group" >
								<label class="col-lg-2 control-label">Payment Method: </label>
								  
								<div class="col-lg-8">
									<select class="form-control" name="payment_method" id="payment_method" onchange="check_payment_type(this.value)">
										<option value="0">Select a group</option>
                                    	<?php
										  $method_rs = $this->accounts_model->get_payment_methods();
										  $num_rows = count($method_rs);
										 if($num_rows > 0)
										  {
											
											foreach($method_rs as $res)
											{
											  $payment_method_id = $res->payment_method_id;
											  $payment_method = $res->payment_method;
											  
												echo '<option value="'.$payment_method_id.'">'.$payment_method.'</option>';
											  
											}
										  }
									  ?>
									</select>
								  </div>
							</div>
							<div id="mpesa_div" class="form-group" style="display:none;" >
								<label class="col-lg-2 control-label"> Mpesa TX Code: </label>

								<div class="col-lg-8">
									<input type="text" class="form-control" name="mpesa_code" id="mpesa_code" placeholder="">
								</div>
							</div>
						  
							<div id="insuarance_div" class="form-group" style="display:none;" >
								<label class="col-lg-2 control-label"> Credit Card Detail: </label>
								<div class="col-lg-8">
									<input type="text" class="form-control" name="insuarance_number" id="insuarance_number" placeholder="">
								</div>
							</div>
						  
							<div id="cheque_div" class="form-group" style="display:none;" >
								<label class="col-lg-2 control-label"> Cheque Number: </label>
							  
								<div class="col-lg-8">
									<input type="text" class="form-control" name="cheque_number" id="cheque_number" placeholder="">
								</div>
							</div>
							<div id="bank_deposit_div" class="form-group" style="display:none;" >
								<label class="col-lg-2 control-label"> Deposit Detail: </label>
							  
								<div class="col-lg-8">
									<input type="text" class="form-control" name="deposit_detail" id="deposit_detail" placeholder="">
								</div>
							</div>
							<div id="debit_card_div" class="form-group" style="display:none;" >
								<label class="col-lg-2 control-label"> Debit Card Detail: </label>
							  
								<div class="col-lg-8">
									<input type="text" class="form-control" name="debit_card_detail" id="debit_card_detail" placeholder="">
								</div>
							</div>
						  
							<div id="username_div" class="form-group" style="display:none;" >
								<label class="col-lg-2 control-label"> Username: </label>
							  
								<div class="col-lg-8">
									<input type="text" class="form-control" name="username" id="username" placeholder="">
								</div>
							</div>
						  
							<div id="password_div" class="form-group" style="display:none;" >
								<label class="col-lg-2 control-label"> Password: </label>
							  
								<div class="col-lg-8">
									<input type="password" class="form-control" name="password" id="password" placeholder="">
								</div>
							</div>
			            </div>
			           <input type="hidden" name="visit_id_payments" id="visit_id_payments">
			        </div>
            </div>
            <div class="modal-footer">
            	<h4 class="pull-left" > Change : <span id="change_item"></span></h4>
            	<button type="submit" class='btn btn-info btn-sm' type='submit' >Add Payment</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                
            </div>
            <?php echo form_close();?>
        </div>
    </div>
</div>
  
 </div>
 </section>

  
<script type="text/javascript">
	$(document).ready(function(){
	  display_inpatient_prescription(<?php echo $visit_id;?>,1);
	  display_patient_bill(<?php echo $visit_id;?>);
	  // document.getElementById("visit_id_checked").value = <?php echo $visit_id;?>;
	  document.getElementById("visit_id_payments").value = <?php echo $visit_id;?>;
	  // document.getElementById("visit_id_visit").value = <?php echo $visit_id;?>;
	});

	function display_patient_bill(visit_id){

		var XMLHttpRequestObject = false;
		  
		if (window.XMLHttpRequest) {

		  XMLHttpRequestObject = new XMLHttpRequest();
		} 
		  
		else if (window.ActiveXObject) {
		  XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
		}

		var config_url = document.getElementById("config_url").value;
		var url = config_url+"walkins/view_patient_bill/"+visit_id;
		// alert(url);
		if(XMLHttpRequestObject) {
		          
		  XMLHttpRequestObject.open("GET", url);
		          
		  XMLHttpRequestObject.onreadystatechange = function(){
		      
		      if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

		        document.getElementById("patient_bill").innerHTML=XMLHttpRequestObject.responseText;
		        get_patient_receipt(visit_id);
				get_page_header(visit_id);
		      }
		  }   
		  XMLHttpRequestObject.send(null);
		}
      
  	}
  	function get_patient_receipt(visit_id){

      var XMLHttpRequestObject = false;
          
      if (window.XMLHttpRequest) {
      
          XMLHttpRequestObject = new XMLHttpRequest();
      } 
          
      else if (window.ActiveXObject) {
          XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
      }
      
      var config_url = document.getElementById("config_url").value;
      var url = config_url+"walkins/get_patient_receipt/"+visit_id;
      // alert(url);
      if(XMLHttpRequestObject) {
                  
          XMLHttpRequestObject.open("GET", url);
                  
          XMLHttpRequestObject.onreadystatechange = function(){
              
              if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

                  document.getElementById("payments-made").innerHTML=XMLHttpRequestObject.responseText;
              }
          }
                  
          XMLHttpRequestObject.send(null);
      }
      get_page_header(visit_id);
  }



  	function get_page_header(visit_id)
	{
		// alert()
		 var XMLHttpRequestObject = false;
          
      if (window.XMLHttpRequest) {
      
          XMLHttpRequestObject = new XMLHttpRequest();
      } 
          
      else if (window.ActiveXObject) {
          XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
      }
      
      var config_url = document.getElementById("config_url").value;
      var url = config_url+"walkins/get_patient_details_header/"+visit_id;
      // alert(url);
      if(XMLHttpRequestObject) {
                  
          XMLHttpRequestObject.open("GET", url);
                  
          XMLHttpRequestObject.onreadystatechange = function(){
              
              if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

                  document.getElementById("page_header").innerHTML=XMLHttpRequestObject.responseText;
              }
          }
                  
          XMLHttpRequestObject.send(null);
      }

	}



	function get_change()
	{

		var visit_id = $('#visit_id_payments').val();
	
		var amount_paid = $('#amount_paid').val();
		var url = "<?php echo base_url();?>accounts/get_change/"+visit_id;
	
		$.ajax({
		type:'POST',
		url: url,
		data:{visit_id: visit_id, amount_paid: amount_paid},
		dataType: 'json',
		success:function(data){
			var change = data.change;

			document.getElementById("change_payment").value = change;
			$('#change_item').html("Kes."+data.change);

		},
		error: function(xhr, status, error) {
		alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
			display_patient_bill(visit_id);
		}
		});
		return false;
	}
	function check_payment_type(payment_type_id){
   
    var myTarget1 = document.getElementById("cheque_div");

    var myTarget2 = document.getElementById("mpesa_div");

    var myTarget4 = document.getElementById("debit_card_div");

    var myTarget5 = document.getElementById("bank_deposit_div");

    var myTarget3 = document.getElementById("insuarance_div");

    if(payment_type_id == 1)
    {
      // this is a check     
      myTarget1.style.display = 'block';
      myTarget2.style.display = 'none';
      myTarget3.style.display = 'none';
      myTarget4.style.display = 'none';
      myTarget5.style.display = 'none';
    }
    else if(payment_type_id == 2)
    {
      myTarget1.style.display = 'none';
      myTarget2.style.display = 'none';
      myTarget3.style.display = 'none';
      myTarget4.style.display = 'none';
      myTarget5.style.display = 'none';
    }
    else if(payment_type_id == 7)
    {
      myTarget1.style.display = 'none';
      myTarget2.style.display = 'none';
      myTarget3.style.display = 'none';
      myTarget4.style.display = 'none';
      myTarget5.style.display = 'block';
    }
    else if(payment_type_id == 8)
    {
      myTarget1.style.display = 'none';
      myTarget2.style.display = 'none';
      myTarget3.style.display = 'none';
      myTarget4.style.display = 'block';
      myTarget5.style.display = 'none';
    }
    else if(payment_type_id == 5)
    {
      myTarget1.style.display = 'none';
      myTarget2.style.display = 'block';
      myTarget3.style.display = 'none';
      myTarget4.style.display = 'none';
      myTarget5.style.display = 'none';
    }
    else if(payment_type_id == 6)
    {
       myTarget1.style.display = 'none';
      myTarget2.style.display = 'none';
      myTarget3.style.display = 'block';
      myTarget4.style.display = 'none';
      myTarget5.style.display = 'none';  
    }

  }

	function get_next_invoice_page(page,visit_id)
	{
		// alert(page);
		 var XMLHttpRequestObject = false;
          
      if (window.XMLHttpRequest) {
      
          XMLHttpRequestObject = new XMLHttpRequest();
      } 
          
      else if (window.ActiveXObject) {
          XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
      }
      
      var config_url = document.getElementById("config_url").value;
      var url = config_url+"walkins/view_patient_bill/"+visit_id+"/"+page;
      // alert(url);
      if(XMLHttpRequestObject) {
                  
          XMLHttpRequestObject.open("GET", url);
                  
          XMLHttpRequestObject.onreadystatechange = function(){
              
              if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

                  document.getElementById("patient_bill").innerHTML=XMLHttpRequestObject.responseText;
              }
          }
                  
          XMLHttpRequestObject.send(null);
      }

	}

	function get_next_payments_page(page,visit_id)
	{
		// alert(page);
		 var XMLHttpRequestObject = false;
          
      if (window.XMLHttpRequest) {
      
          XMLHttpRequestObject = new XMLHttpRequest();
      } 
          
      else if (window.ActiveXObject) {
          XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
      }
      
      var config_url = document.getElementById("config_url").value;
      var url = config_url+"walkins/get_patient_receipt/"+visit_id+"/"+page;
      // alert(url);
      if(XMLHttpRequestObject) {
                  
          XMLHttpRequestObject.open("GET", url);
                  
          XMLHttpRequestObject.onreadystatechange = function(){
              
              if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

                  document.getElementById("payments-made").innerHTML=XMLHttpRequestObject.responseText;
              }
          }
                  
          XMLHttpRequestObject.send(null);
      }

	}

	//Calculate procedure total
	function calculatetotal(amount, id, procedure_id, v_id){
	       
	    var units = document.getElementById('units'+id).value;  
	    var billed_amount = document.getElementById('billed_amount'+id).value;  

	    grand_total(id, units, billed_amount, v_id);
	}
	function grand_total(procedure_id, units, amount, v_id){
    	var config_url = document.getElementById("config_url").value;
    	var url = config_url+"accounts/update_service_total/"+procedure_id+"/"+units+"/"+amount+"/"+v_id;
	
		$.ajax({
		type:'POST',
		url: url,
		data:{visit_id: v_id},
		dataType: 'json',
		success:function(data){
			alert(data.message);
			display_patient_bill(v_id);
		},
		error: function(xhr, status, error) {
		alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
			display_patient_bill(v_id);
		}
		});
		return false;

	    
	   
	}

	$(document).on("submit","form#add_payment",function(e)
	{
		e.preventDefault();	

		var payment_method = $('#payment_method').val();
		var amount_paid = $('#amount_paid').val();
		var type_payment =  $("input[name='type_payment']:checked").val(); //$('#type_payment').val();
		var service_id = $('#service_id').val();
		var cheque_number = $('#cheque_number').val();
		var insuarance_number = $('#insuarance_number').val();
		var mpesa_code = $('#mpesa_code').val();
		var username = $('#username').val();
		var password = $('#password').val();
		var change_payment = $('#change_payment').val();

		var debit_card_detail = $('#debit_card_detail').val();
		var deposit_detail = $('#deposit_detail').val();
		var password = $('#password').val();


		var visit_id = $('#visit_id_payments').val();

		var payment_service_id = $('#payment_service_id').val();

	
		var url = "<?php echo base_url();?>accounts/make_payments/"+visit_id;
		// alert(type_payment);
		$.ajax({
		type:'POST',
		url: url,
		data:{payment_method: payment_method, amount_paid: amount_paid, type_payment: type_payment,service_id: service_id, cheque_number: cheque_number, insuarance_number: insuarance_number, mpesa_code: mpesa_code,username: username,password: password, payment_service_id: payment_service_id,debit_card_detail: debit_card_detail,deposit_detail: deposit_detail,change_payment:change_payment},
		dataType: 'json',
		success:function(data){

			if(data.result == 'success')
        	{
				alert(data.message);

			 	$('#add_payment_modal').modal('toggle');
			 	get_page_header(visit_id);
      			get_patient_receipt(visit_id,null);
				 
			}
			else
			{
				alert(data.message);
			}
		},
		error: function(xhr, status, error) {
		alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
			display_patient_bill(visit_id);
		}
		});
		return false;
	});
	function delete_service(id, visit_id){

		var res = confirm('Are you sure you want to delete this charge?');
     
	    if(res)
	    {

	    	var config_url = document.getElementById("config_url").value;
	    	var url = config_url+"accounts/delete_service_billed/"+id+"/"+visit_id;
		
			$.ajax({
			type:'POST',
			url: url,
			data:{visit_id: visit_id,id: id},
			dataType: 'json',
			success:function(data){
				alert(data.message);
				display_patient_bill(visit_id);
			},
			error: function(xhr, status, error) {
			alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
				display_patient_bill(visit_id);
			}
			});
			return false;
		    var XMLHttpRequestObject = false;
		        
		    if (window.XMLHttpRequest) {
		    
		        XMLHttpRequestObject = new XMLHttpRequest();
		    } 
		        
		    else if (window.ActiveXObject) {
		        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
		    }
		     var config_url = document.getElementById("config_url").value;
		    var url = config_url+"accounts/delete_service_billed/"+id;
		    
		    if(XMLHttpRequestObject) {
		                
		        XMLHttpRequestObject.open("GET", url);
		                
		        XMLHttpRequestObject.onreadystatechange = function(){
		            
		            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

		                display_patient_bill(visit_id);
		            }
		        }
		                
		        XMLHttpRequestObject.send(null);
		    }
		}
	}

	$(document).on("submit","form#payments-paid-form",function(e)
	{
		// alert("changed");
		e.preventDefault();	

		
		var visit_id = $('#visit_id').val();
		var payment_id = $('#payment_id').val();
		var cancel_action_id = $('#cancel_action_id').val();
		var cancel_description = $('#cancel_description').val();
		var url = "<?php echo base_url();?>accounts/cancel_payment/"+payment_id+"/"+visit_id;	

		
		// alert(cancel_action_id);	
		$.ajax({
		type:'POST',
		url: url,
		data:{cancel_description: cancel_description, cancel_action_id: cancel_action_id},
		dataType: 'text',
		success:function(data){
			alert("You have successfully cancelled a payment");
		 	$('#refund_payment'+visit_id).modal('toggle');
		 	get_page_header(visit_id);
			get_patient_receipt(visit_id);
		},
		error: function(xhr, status, error) {
		alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
			get_page_header(visit_id);
			get_patient_receipt(visit_id);
		}
		});
		return false;
	});

	$(function() {
	    $("#drug_id").customselect();
	     $("#service_id_item").customselect();
       	$("#provider_id_item").customselect();

	  });
	
	function myPopup2(visit_id,module) {
		var config_url = $('#config_url').val();
		var win_drugs = window.open(config_url+"pharmacy/drugs/"+visit_id+"/"+module,"Popup3","height=1200,width=1000,,scrollbars=yes,"+ 
							"directories=yes,location=yes,menubar=yes," + 
							 "resizable=no status=no,history=no top = 50 left = 100"); 
  		win_drugs.focus();
	}
	
	function myPopup2_soap(visit_id) {
		var config_url = $('#config_url').val();
		var win_drugs = window.open(config_url+"pharmacy/drugs/"+visit_id,"Popup2","height=1200,width=1000,,scrollbars=yes,"+ 
							"directories=yes,location=yes,menubar=yes," + 
							 "resizable=no status=no,history=no top = 50 left = 100"); 
  		win_drugs.focus();
	}
	
	function send_to_pharmacy2(visit_id)
	{
		var config_url = $('#config_url').val();
		var url = config_url+"pharmacy/display_prescription/"+visit_id;
	
		$.get(url, function( data ) {
			var obj = window.opener.document.getElementById("prescription");
			obj.innerHTML = data;
			window.close(this);
		});
	}
</script>



<script type="text/javascript">

	function get_visit_trail(visit_id){

		var myTarget2 = document.getElementById("visit_trail"+visit_id);
		var button = document.getElementById("open_visit"+visit_id);
		var button2 = document.getElementById("close_visit"+visit_id);

		myTarget2.style.display = '';
		button.style.display = 'none';
		button2.style.display = '';
	}
	function close_visit_trail(visit_id){

		var myTarget2 = document.getElementById("visit_trail"+visit_id);
		var button = document.getElementById("open_visit"+visit_id);
		var button2 = document.getElementById("close_visit"+visit_id);

		myTarget2.style.display = 'none';
		button.style.display = '';
		button2.style.display = 'none';
	}

	function update_prescription_values(visit_id,visit_charge_id,prescription_id,module)
    {
      
       //var product_deductions_id = $(this).attr('href');
       var quantity = $('#quantity'+prescription_id).val();
       var x = $('#x'+prescription_id).val();
       var duration = $('#duration'+prescription_id).val();
       var consumption = $('#consumption'+prescription_id).val();


       var url = "<?php echo base_url();?>pharmacy/update_prescription/"+visit_id+'/'+visit_charge_id+'/'+prescription_id+'/'+module;
  
        //window.alert(data_url);
		  $.ajax({
		  type:'POST',
		  url: data_url,
		  data:{quantity: quantity, x: x, duration: duration,consumption: consumption},
		  dataType: 'text',
           success:function(data){
            
            window.alert(data.result);
            if(module == 1){
				window.location.href = "<?php echo base_url();?>pharmacy/prescription1/"+visit_id+"/1'";
			
			}else{
				window.location.href = "<?php echo base_url();?>pharmacy/prescription1/"+visit_id+"";
			}
           },
           error: function(xhr, status, error) {
            alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
           
           }
        });
        return false;
     }
  </script>

<script type="text/javascript">
	function get_drug_to_prescribe(visit_id)
	{
	  var XMLHttpRequestObject = false;
	        
	    if (window.XMLHttpRequest) {
	    
	        XMLHttpRequestObject = new XMLHttpRequest();
	    } 
	        
	    else if (window.ActiveXObject) {
	        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
	    }
	    var drug_id = document.getElementById("drug_id").value;

	    var url = "<?php echo site_url();?>pharmacy/inpatient_prescription/"+visit_id+"/"+drug_id+"/1";

	     if(XMLHttpRequestObject) {
	                
	        XMLHttpRequestObject.open("GET", url);
	                
	        XMLHttpRequestObject.onreadystatechange = function(){
	            
	            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
	              var prescription_view = document.getElementById("prescription_view");
	             
	              document.getElementById("prescription_view").innerHTML=XMLHttpRequestObject.responseText;
	               prescription_view.style.display = 'block';
	            }
	        }
	                
	        XMLHttpRequestObject.send(null);
	    }

	}
	function pass_prescription()
    {

	   var quantity = document.getElementById("quantity_value").value;
	   var x = document.getElementById("x_value").value;
	   var dose_value = document.getElementById("dose_value").value;
	   var duration = 1;//document.getElementById("duration_value").value;
	   var consumption = document.getElementById("consumption_value").value;
	   var number_of_days = document.getElementById("number_of_days_value").value;
	   var service_charge_id = document.getElementById("drug_id").value;
	   var visit_id = document.getElementById("visit_id").value;
	   var input_total_units = document.getElementById("input-total-value").value;
	   var module = document.getElementById("module").value;
	   var passed_value = document.getElementById("passed_value").value;
	   var type_of_drug = document.getElementById("type_of_drug").value;
	   
	   var url = "<?php echo base_url();?>pharmacy/prescribe_prescription";
	   
	  
	   $.ajax({
	   type:'POST',
	   url: url,
	   data:{quantity: quantity, x: x, duration: duration,consumption: consumption, service_charge_id : service_charge_id, visit_id : visit_id, number_of_days: number_of_days,module: module,passed_value:passed_value,input_total_units:input_total_units,dose_value: dose_value,type_of_drug: type_of_drug},
	   dataType: 'text',
	   success:function(data){
	   
	   var prescription_view = document.getElementById("prescription_view");
	   prescription_view.style.display = 'none';
	   display_inpatient_prescription(visit_id,1);
	   
	   },
	   error: function(xhr, status, error) {
	   alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
	   
	   }
	   });
	   
	   return false;
    }


	function check_frequency_type()
   {
   
     var x = document.getElementById("x_value").value;
     var type_of_drug = document.getElementById("type_of_drug").value;
       
     var number_of_days = document.getElementById("number_of_days_value").value;
     var quantity = document.getElementById("quantity_value").value;
     var dose_value = document.getElementById("dose_value").value;

    var service_charge_id = document.getElementById("drug_id").value;
    var visit_id = document.getElementById("visit_id").value;
    var consumption = document.getElementById("consumption_value").value;

     if(x == "" || x == 0)
     {

       // alert("Please select the frequency of the medicine");
       x = 1;
     }
     
     if(number_of_days == "" || number_of_days == 0)
     {
        number_of_days = 1;
     }
     
         
      var url = "<?php echo base_url();?>pharmacy/get_values";
       $.ajax({
       type:'POST',
       url: url,
       data:{quantity: quantity, x: x, service_charge_id : service_charge_id, visit_id : visit_id, number_of_days: number_of_days,dose_value:dose_value,consumption: consumption,type_of_drug: type_of_drug},
       dataType: 'text',
       success:function(data){
          var data = jQuery.parseJSON(data);

          var amount = data.amount;
          var frequency = data.frequency;
           var item = data.item;

          // {"message":"success","amount":"35","frequency":"5"}

          if(type_of_drug == 3)
          {
           
            var total_units = number_of_days * frequency * quantity;
            var total_amount =  number_of_days * frequency * amount * quantity;
          }
          else
          {

             var total_units =   quantity;
             var total_amount =   amount * quantity;
          }
        

        // document.getElementById("total_units").innerHTML = "<h2>"+ amount +" units</h2>";
         $( "#total_units" ).html("<h2>"+ total_units +" units</h2>");
         $( "#total_amount" ).html("<h3>Ksh. "+ total_amount +"</h3>");
         $( "#item_description" ).html("<p> "+ item +"</p>");

         document.getElementById("input-total-value").value = total_units;
         // document.getElementById("total_amount").innerHTML = "<h3>Ksh. "+ frequency +" units</h3>";


       
       },
       error: function(xhr, status, error) {
       alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
       
       }
       });
   
        
     
  }
	function display_inpatient_prescription(visit_id,module){

	    var XMLHttpRequestObject = false;
	        
	    if (window.XMLHttpRequest) {
	    
	        XMLHttpRequestObject = new XMLHttpRequest();
	    } 
	        
	    else if (window.ActiveXObject) {
	        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
	    }
	    
	    var config_url = document.getElementById("config_url").value;
	    var url = config_url+"pharmacy/display_inpatient_prescription/"+visit_id+"/"+module;
	    // alert(url);
	    if(XMLHttpRequestObject) {
	                
	        XMLHttpRequestObject.open("GET", url);
	                
	        XMLHttpRequestObject.onreadystatechange = function(){
	            
	            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

	                document.getElementById("visit_prescription").innerHTML=XMLHttpRequestObject.responseText;
	            }
	        }
	                
	        XMLHttpRequestObject.send(null);
	    }
	}
	function button_update_prescription(visit_id,visit_charge_id,prescription_id,module)
	{
	  var quantity = $('#quantity'+prescription_id).val();
	  var x = $('#x'+prescription_id).val();
	  var duration = $('#duration'+prescription_id).val();
	  var consumption = $('#consumption'+prescription_id).val();
	
	  var url = "<?php echo site_url();?>pharmacy/update_inpatient_prescription/"+visit_id+"/"+visit_charge_id+"/"+prescription_id+"/"+module;


	  $.ajax({
	  type:'POST',
	  url: url,
	  data:{quantity: quantity, x: x, duration: duration,consumption: consumption},
	  dataType: 'text',
	  success:function(data){

	  
	  },
	  error: function(xhr, status, error) {
	  alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

	  }
	  });
	  display_inpatient_prescription(visit_id,1);
	  return false;
	}

	function dispense_prescription(visit_id,visit_charge_id,prescription_id,module)
	{

	  var quantity =  $('#quantity'+prescription_id).val();
	  var x = $('#x'+prescription_id).val();
	  var duration = $('#duration'+prescription_id).val();
	  var consumption = $('#consumption'+prescription_id).val();
	  var charge = $('#charge'+prescription_id).val();
	  var units_given = $('#units_given'+prescription_id).val();
	  
	  var url = "<?php echo site_url();?>pharmacy/dispense_inpatient_prescription/"+visit_id+"/"+visit_charge_id+"/"+prescription_id+"/"+module+"/"+quantity;
	 
	  $.ajax({
	  type:'POST',
	  url: url,
	  data:{quantity: quantity, x: x, duration: duration,consumption: consumption,charge: charge, units_given: units_given},
	  dataType: 'json',
	  success:function(data){
	    window.alert(data.result);
	  },
	  error: function(xhr, status, error) {
	  alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

	  }
	  });
	  display_inpatient_prescription(visit_id,1);
	  return false;
	}

function delete_prescription(prescription_id, visit_id,visit_charge_id,module)
{
  var res = confirm('Are you sure you want to delete this prescription ?');
  
  if(res)
  {
    var XMLHttpRequestObject = false;
    
    if (window.XMLHttpRequest) {
      XMLHttpRequestObject = new XMLHttpRequest();
    } 
    
    else if (window.ActiveXObject) {
      XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
    }
    var url = "<?php echo site_url();?>pharmacy/delete_inpatient_prescription/"+prescription_id+"/"+visit_id+"/"+visit_charge_id+"/"+module;
    // alert(url);
    if(XMLHttpRequestObject) {
      
      XMLHttpRequestObject.open("GET", url);
      
      XMLHttpRequestObject.onreadystatechange = function(){
        
        if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
          
           display_inpatient_prescription(visit_id,1);
        }
      }
      XMLHttpRequestObject.send(null);
    }
  }
}

$(document).on("click","a.print_prescription",function(e)
{
	e.preventDefault();
	
	//get checkbox values
	var val = [];
	$(':checkbox:checked').each(function(i){
	  	val[i] = $(this).val();
	});
	var request = '<?php echo site_url();?>pharmacy/print_selected_drugs/<?php echo $visit_id;?>';
	$.ajax({
		type:'POST',
		url: request,
		data:{prescription_id: val},
		dataType: 'text',
		success:function(data)
		{
			var win = window.open("<?php echo site_url();?>pharmacy/print-prescription/<?php echo $visit_id;?>", '_blank');
			if(win){
				//Browser has allowed it to be opened
				win.focus();
			}else{
				//Broswer has blocked it
				alert('Please allow popups for this site');
			}
		},
		error: function(xhr, status, error) 
		{
			alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
		}
	});
	
	return false;
});
</script>                        