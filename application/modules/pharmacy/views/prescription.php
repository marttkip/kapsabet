<!-- end #header -->
<?php
$data['visit_id'] = $v_data['visit_id'] = $visit_id;
$data['lab_test'] = 100;

if(!isset($mobile_personnel_id))
{
	$mobile_personnel_id = NULL;
}
$v_data['mobile_personnel_id'] = $mobile_personnel_id;
$v_data['visit_history'] = 1;
//Symptoms


//Plan
// $v_data['query'] = $this->nurse_model->get_visit_pan_detail($visit_id);

// $plan = $this->load->view('medical_record/plan', $v_data, TRUE);

?>
<div id="loader" style="display: none;"></div>
<section class="panel">
    <header class="panel-heading">
        <div class="panel-title"><strong>Name:</strong> <?php echo $patient_surname.' '.$patient_othernames;?>. <strong> GENDER: </strong> <?php echo $gender;?><strong> AGE: </strong>  <?php echo $age;?>  <strong> Account Bal: </strong> Kes <?php echo number_format($account_balance, 2);?>
		</div>
		<div class="pull-right">
			<?php
			if($inpatient > 0)
			{
				?>
				<a href="<?php echo site_url();?>queues/inpatient-queue" class="btn btn-info btn-sm pull-right " style="margin-top:-25px"><i class="fa fa-arrow-left"></i> Back to Inpatient Queue</a>
		
				<?php
			}
			else
			{
				?>
				<a href="<?php echo site_url();?>queues/outpatient-queue" class="btn btn-info btn-sm pull-right " style="margin-top:-25px"><i class="fa fa-arrow-left"></i> Back to Outpatient Queue</a>
		
				<?php
			}
			?>
		</div>
    </header>

    <div class="panel-body">
		
		
<div class="row">
  <div class="col-md-12">
  		<div class="tabs">
            <ul class="nav nav-tabs nav-justified">
                <li class="active">
                    <a class="text-center" data-toggle="tab" href="#outpatient">Prescription</a>
                </li>
                <li>
                    <a class="text-center" data-toggle="tab" href="#visit-prescription" onclick="get_past_prescriptions(<?php echo $visit_id;?>,<?php echo $patient_id;?>)">Previous Prescription</a>
                </li>                
                <li>
                    <a class="text-center" data-toggle="tab" href="#plan-visit1" onclick="get_visit_trail(<?php echo $visit_id;?>)">Visit Trail</a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="outpatient">
                    <section class="panel panel-featured panel-featured-info">
			            <header class="panel-heading">
			                <h2 class="panel-title">Prescription</h2>
			                <div class="pull-right">
			                	<?php  echo '<a href="'.site_url().'pharmacy/hold_card/'.$visit_id.'" onclick="return confirm(\'Are you sure you want to hold this card ?\');" class="btn btn-sm btn-danger" style="margin-top:-40px;"> Hold Card </a>';?>
			                </div>
			            </header>
			            <div class="panel-body">
			                <!-- <div class="col-lg-8 col-md-8 col-sm-8">
			                  <div class="form-group">
			                    <select id='drug_id' name='drug_id' class='form-control custom-select ' onchange="get_drug_to_prescribe(<?php echo $visit_id;?>);">
			                      <option value=''>None - Please Select an drug</option>
			                      <?php echo $drugs;?>
			                    </select>
			                  </div>
			                
			                </div> -->

			                <div class="col-lg-8 col-md-8 col-sm-8">
				              <div class="form-group">
				              	<label class="col-md-2 control-label">Drugs: </label>
            
            						<div class="col-md-10">
						                <select id='service_id_item' name='service_charge_id' class='form-control custom-select ' onchange="get_service_charge_amount(this.value)">
						                  <option value=''>None - Please Select a Drugs to prescribe</option>
						                  <?php echo $drugs;?>
						                </select>
						         </div>
				              </div>
				            </div>
				            <input type="hidden" name="charge" id="drug-charge" >
				            <div class="col-lg-4 col-md-4 col-sm-4">
							  <div class="form-group">
								  <button type='submit' class="btn btn-sm btn-success"  onclick="parse_drug_charge(<?php echo $visit_id;?>);"> Add Drug</button>
							  </div>
							</div>

				             <div id="prescription_view"></div>
				             <div id="visit_prescription"></div>

			            </div>
			              
						
						<div class="center-align">
						<?php 
							// var_dump($module); die();
						echo '<a href="'.site_url().'pharmacy/send_to_walkin_accounts/'.$visit_id.'" onclick="return confirm(\'Send to accounts?\');" class="btn btn-sm btn-success"> Send to Accounts</a>'; ?>
						<?php 
						if($module == 2 OR $patient_type_id = 1)
						{
							

							if($account_balance == 0)
							{
								echo '<a href="'.site_url().'pharmacy/close_visit/'.$visit_id.'" onclick="return confirm(\' You are about to post these drugs ?\');" class="btn btn-sm btn-info"> Post Drugs </a>';
							}
							
						}

						?>
						<?php echo '<a href="'.site_url().'pharmacy/print-prescription/'.$visit_id.'" class="btn btn-sm btn-warning print_prescription" target="_blank"><i class="fa fa-print"></i> Print Prescription</a>';?>



					 	</div>
			        </section>
                </div>
                <div class="tab-pane" id="visit-prescription">
                    <h4 class="center-align" style="margin-bottom:10px;">Visit Prescriptions </h4>
                     <div id="patient-previous-prescription"></div>                   
                </div>
                  <div class="tab-pane" id="plan-visit1">
                    <h4 class="center-align" style="margin-bottom:10px;">Visit Trail</h4>	
                    <div id="patient-visit-trail"></div>
                   
                   
                </div>
            </div>
        </div>
        
    </div>
</div>

  
 </div>
 </section>

  
<script type="text/javascript">
	$(document).ready(function(){
	  display_inpatient_prescription(<?php echo $visit_id;?>,1);
	  // display_inpatient_visit_prescription(<?php echo $visit_id;?>,0);
	});

	$(function() {
	    $("#service_id_item").customselect();

	  });

	function myPopup2(visit_id,module) {
		var config_url = $('#config_url').val();
		var win_drugs = window.open(config_url+"pharmacy/drugs/"+visit_id+"/"+module,"Popup3","height=1200,width=1000,,scrollbars=yes,"+ 
							"directories=yes,location=yes,menubar=yes," + 
							 "resizable=no status=no,history=no top = 50 left = 100"); 
  		win_drugs.focus();
	}
	
	function myPopup2_soap(visit_id) {
		var config_url = $('#config_url').val();
		var win_drugs = window.open(config_url+"pharmacy/drugs/"+visit_id,"Popup2","height=1200,width=1000,,scrollbars=yes,"+ 
							"directories=yes,location=yes,menubar=yes," + 
							 "resizable=no status=no,history=no top = 50 left = 100"); 
  		win_drugs.focus();
	}
	
	function send_to_pharmacy2(visit_id)
	{
		var config_url = $('#config_url').val();
		var url = config_url+"pharmacy/display_prescription/"+visit_id;
	
		$.get(url, function( data ) {
			var obj = window.opener.document.getElementById("prescription");
			obj.innerHTML = data;
			window.close(this);
		});
	}
</script>



<script type="text/javascript">

	function get_visit_trail(visit_id){

		var myTarget2 = document.getElementById("visit_trail"+visit_id);
		var button = document.getElementById("open_visit"+visit_id);
		var button2 = document.getElementById("close_visit"+visit_id);

		myTarget2.style.display = '';
		button.style.display = 'none';
		button2.style.display = '';
	}
	function close_visit_trail(visit_id){

		var myTarget2 = document.getElementById("visit_trail"+visit_id);
		var button = document.getElementById("open_visit"+visit_id);
		var button2 = document.getElementById("close_visit"+visit_id);

		myTarget2.style.display = 'none';
		button.style.display = '';
		button2.style.display = 'none';
	}

	function update_prescription_values(visit_id,visit_charge_id,prescription_id,module)
    {
      
       //var product_deductions_id = $(this).attr('href');
       var quantity = $('#quantity'+prescription_id).val();
       var x = $('#x'+prescription_id).val();
       var duration = $('#duration'+prescription_id).val();
       var consumption = $('#consumption'+prescription_id).val();


       var url = "<?php echo base_url();?>pharmacy/update_prescription/"+visit_id+'/'+visit_charge_id+'/'+prescription_id+'/'+module;
  
        //window.alert(data_url);
		  $.ajax({
		  type:'POST',
		  url: data_url,
		  data:{quantity: quantity, x: x, duration: duration,consumption: consumption},
		  dataType: 'text',
           success:function(data){
            
            window.alert(data.result);
            if(module == 1){
				window.location.href = "<?php echo base_url();?>pharmacy/prescription1/"+visit_id+"/1'";
			
			}else{
				window.location.href = "<?php echo base_url();?>pharmacy/prescription1/"+visit_id+"";
			}
           },
           error: function(xhr, status, error) {
            alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
           
           }
        });
        return false;
     }
  </script>

<script type="text/javascript">
	function get_drug_to_prescribe(visit_id)
	{
	  var XMLHttpRequestObject = false;
	        
	    if (window.XMLHttpRequest) {
	    
	        XMLHttpRequestObject = new XMLHttpRequest();
	    } 
	        
	    else if (window.ActiveXObject) {
	        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
	    }
	    var drug_id = document.getElementById("drug_id").value;

	    var url = "<?php echo site_url();?>pharmacy/inpatient_prescription/"+visit_id+"/"+drug_id+"/1";

	     if(XMLHttpRequestObject) {
	                
	        XMLHttpRequestObject.open("GET", url);
	                
	        XMLHttpRequestObject.onreadystatechange = function(){
	            
	            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
	              var prescription_view = document.getElementById("prescription_view");
	             
	              document.getElementById("prescription_view").innerHTML=XMLHttpRequestObject.responseText;
	               prescription_view.style.display = 'block';
	            }
	        }
	                
	        XMLHttpRequestObject.send(null);
	    }

	}
	function pass_prescription()
    {

	   var quantity = document.getElementById("quantity_value").value;
	   var x = document.getElementById("x_value").value;
	   var dose_value = document.getElementById("dose_value").value;
	   var duration = 1;//document.getElementById("duration_value").value;
	   var consumption = document.getElementById("consumption_value").value;
	   var number_of_days = document.getElementById("number_of_days_value").value;
	   var service_charge_id = document.getElementById("drug_id").value;
	   var visit_id = document.getElementById("visit_id").value;
	   var input_total_units = document.getElementById("input-total-value").value;
	   var module = document.getElementById("module").value;
	   var passed_value = document.getElementById("passed_value").value;
	   var type_of_drug = document.getElementById("type_of_drug").value;
	   var charge_date = document.getElementById("charge_date").value;
	   
	   var url = "<?php echo base_url();?>pharmacy/prescribe_prescription";
	   
	  
	   $.ajax({
	   type:'POST',
	   url: url,
	   data:{quantity: quantity, x: x, duration: duration,consumption: consumption, service_charge_id : service_charge_id, visit_id : visit_id, number_of_days: number_of_days,module: module,passed_value:passed_value,input_total_units:input_total_units,dose_value: dose_value,type_of_drug: type_of_drug,charge_date: charge_date},
	   dataType: 'text',
	   success:function(data){
	   
	   var prescription_view = document.getElementById("prescription_view");
	   prescription_view.style.display = 'none';
	   display_inpatient_prescription(visit_id,1);
	   
	   },
	   error: function(xhr, status, error) {
	   alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
	   
	   }
	   });
	   
	   return false;
    }


	function check_frequency_type()
   {
   
     var x = document.getElementById("x_value").value;
     var type_of_drug = document.getElementById("type_of_drug").value;
       
     var number_of_days = document.getElementById("number_of_days_value").value;
     var quantity = document.getElementById("quantity_value").value;
     var dose_value = document.getElementById("dose_value").value;

    var service_charge_id = document.getElementById("drug_id").value;
    var visit_id = document.getElementById("visit_id").value;
    var consumption = document.getElementById("consumption_value").value;

     if(x == "" || x == 0)
     {

       // alert("Please select the frequency of the medicine");
       x = 1;
     }
     
     if(number_of_days == "" || number_of_days == 0)
     {
        number_of_days = 1;
     }
     
         
      var url = "<?php echo base_url();?>pharmacy/get_values";
       $.ajax({
       type:'POST',
       url: url,
       data:{quantity: quantity, x: x, service_charge_id : service_charge_id, visit_id : visit_id, number_of_days: number_of_days,dose_value:dose_value,consumption: consumption,type_of_drug: type_of_drug},
       dataType: 'text',
       success:function(data){
          var data = jQuery.parseJSON(data);

          var amount = data.amount;
          var frequency = data.frequency;
           var item = data.item;

          // {"message":"success","amount":"35","frequency":"5"}

          if(type_of_drug == 3)
          {
           
            var total_units = number_of_days * frequency * quantity;
            var total_amount =  number_of_days * frequency * amount * quantity;
          }
          else
          {

             var total_units =   quantity;
             var total_amount =   amount * quantity;
          }
        

        // document.getElementById("total_units").innerHTML = "<h2>"+ amount +" units</h2>";
         $( "#total_units" ).html("<h2>"+ total_units +" units</h2>");
         $( "#total_amount" ).html("<h3>Ksh. "+ total_amount +"</h3>");
         
         $( "#item_description" ).html("<p> "+ item +"</p>");

         document.getElementById("input-total-value").value = total_units;
         // document.getElementById("total_amount").innerHTML = "<h3>Ksh. "+ frequency +" units</h3>";


       
       },
       error: function(xhr, status, error) {
       alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
       
       }
       });
   
        
     
  }
	function display_inpatient_prescription(visit_id,module){

	    var XMLHttpRequestObject = false;
	        
	    if (window.XMLHttpRequest) {
	    
	        XMLHttpRequestObject = new XMLHttpRequest();
	    } 
	        
	    else if (window.ActiveXObject) {
	        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
	    }
	    
	    var config_url = document.getElementById("config_url").value;
	    var url = config_url+"pharmacy/display_inpatient_prescription/"+visit_id+"/"+module;
	    // alert(url);
	    if(XMLHttpRequestObject) {
	                
	        XMLHttpRequestObject.open("GET", url);
	                
	        XMLHttpRequestObject.onreadystatechange = function(){
	            
	            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

	                document.getElementById("visit_prescription").innerHTML=XMLHttpRequestObject.responseText;
	            }
	        }
	                
	        XMLHttpRequestObject.send(null);
	    }
	}

	function display_inpatient_visit_prescription(visit_id,module){

	    var XMLHttpRequestObject = false;
	        
	    if (window.XMLHttpRequest) {
	    
	        XMLHttpRequestObject = new XMLHttpRequest();
	    } 
	        
	    else if (window.ActiveXObject) {
	        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
	    }
	    
	    var config_url = document.getElementById("config_url").value;
	    var url = config_url+"pharmacy/display_inpatient_visit_prescription/"+visit_id+"/"+module;
	    // alert(url);
	    if(XMLHttpRequestObject) {
	                
	        XMLHttpRequestObject.open("GET", url);
	                
	        XMLHttpRequestObject.onreadystatechange = function(){
	            
	            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

	                document.getElementById("all_visit_prescription").innerHTML=XMLHttpRequestObject.responseText;
	            }
	        }
	                
	        XMLHttpRequestObject.send(null);
	    }
	}
	function button_update_prescription(visit_id,visit_charge_id,prescription_id,module)
	{
	  var quantity = $('#quantity'+prescription_id).val();
	  var x = $('#x'+prescription_id).val();
	  var duration = $('#duration'+prescription_id).val();
	  var consumption = $('#consumption'+prescription_id).val();
	
	  var url = "<?php echo site_url();?>pharmacy/update_inpatient_prescription/"+visit_id+"/"+visit_charge_id+"/"+prescription_id+"/"+module;


	  $.ajax({
	  type:'POST',
	  url: url,
	  data:{quantity: quantity, x: x, duration: duration,consumption: consumption},
	  dataType: 'text',
	  success:function(data){

	  
	  },
	  error: function(xhr, status, error) {
	  alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

	  }
	  });
	  display_inpatient_prescription(visit_id,1);
	  return false;
	}

	function dispense_prescription(visit_id,visit_charge_id,prescription_id,module)
	{

	  var quantity =  $('#quantity'+prescription_id).val();
	  var x = $('#x'+prescription_id).val();
	  var duration = $('#duration'+prescription_id).val();
	  var consumption = $('#consumption'+prescription_id).val();
	  var charge = $('#charge'+prescription_id).val();
	  var units_given = $('#units_given'+prescription_id).val();
	  var service_charge_amount = $('#service_charge_amount'+prescription_id).val();
	  var product_id = $('#product_id'+prescription_id).val();
	 var product_deleted = $('#product_deleted'+prescription_id).val();
	  
	  var url = "<?php echo site_url();?>pharmacy/dispense_inpatient_prescription/"+visit_id+"/"+visit_charge_id+"/"+prescription_id+"/"+module+"/"+quantity;
	 // alert(url);
	  $.ajax({
	  type:'POST',
	  url: url,
	  data:{quantity: quantity, x: x, duration: duration,consumption: consumption,charge: charge, units_given: units_given,service_charge_amount: service_charge_amount, product_id: product_id,product_deleted: product_deleted},
	  dataType: 'text',
	  success:function(data){
	  	var data = jQuery.parseJSON(data);
	  	display_inpatient_prescription(visit_id,1);
	  	if(data.status == 1)
	  	{
	    	window.alert(data.result);
	  	}
	  	else
	  	{
	  		var res = confirm(data.result);  
			if(res)
			{
				var url = "<?php echo site_url();?>pharmacy/borrow_inpatient_prescription/"+visit_id+"/"+visit_charge_id+"/"+prescription_id+"/"+module+"/"+quantity;
				$.ajax({
				type:'POST',
				url: url,
				data:{quantity: quantity, x: x, duration: duration,consumption: consumption,charge: charge, units_given: units_given},
				dataType: 'json',
				success:function(data){

					display_inpatient_prescription(visit_id,1);					
					// window.alert(data.result);
				},
				error: function(xhr, status, error) {
				alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

				}
				});
				return false;
			}

	  	}
	  },
	  error: function(xhr, status, error) {
	  alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

	  }
	  });
	  return false;
	}

	function undispense_prescription(visit_id,visit_charge_id,prescription_id,module)
	{

	  var quantity =  $('#quantity'+prescription_id).val();
	  var x = $('#x'+prescription_id).val();
	  var duration = $('#duration'+prescription_id).val();
	  var consumption = $('#consumption'+prescription_id).val();
	  var charge = $('#charge'+prescription_id).val();
	  var units_given = $('#units_given'+prescription_id).val();
	  
	  var url = "<?php echo site_url();?>pharmacy/undispense_inpatient_prescription/"+visit_id+"/"+visit_charge_id+"/"+prescription_id+"/"+module+"/"+quantity;
	 
	  $.ajax({
	  type:'POST',
	  url: url,
	  data:{quantity: quantity, x: x, duration: duration,consumption: consumption,charge: charge, units_given: units_given},
	  dataType: 'json',
	  success:function(data){
	  	display_inpatient_prescription(visit_id,1);
	    window.alert(data.result);
	  },
	  error: function(xhr, status, error) {
	  alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

	  }
	  });
	  display_inpatient_prescription(visit_id,1);
	  return false;
	}

function delete_prescription(prescription_id, visit_id,visit_charge_id,module)
{
  var res = confirm('Are you sure you want to delete this prescription ?');
  
  if(res)
  {
    var XMLHttpRequestObject = false;
    
    if (window.XMLHttpRequest) {
      XMLHttpRequestObject = new XMLHttpRequest();
    } 
    
    else if (window.ActiveXObject) {
      XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
    }
    var url = "<?php echo site_url();?>pharmacy/delete_inpatient_prescription/"+prescription_id+"/"+visit_id+"/"+visit_charge_id+"/"+module;
    // alert(url);
    if(XMLHttpRequestObject) {
      
      XMLHttpRequestObject.open("GET", url);
      
      XMLHttpRequestObject.onreadystatechange = function(){
        
        if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
          
           display_inpatient_prescription(visit_id,1);
        }
      }
      XMLHttpRequestObject.send(null);
    }
  }
}

$(document).on("click","a.print_prescription",function(e)
{
	e.preventDefault();
	
	//get checkbox values
	var val = [];
	$(':checkbox:checked').each(function(i){
	  	val[i] = $(this).val();
	});
	var request = '<?php echo site_url();?>pharmacy/print_selected_drugs/<?php echo $visit_id;?>';
	$.ajax({
		type:'POST',
		url: request,
		data:{prescription_id: val},
		dataType: 'text',
		success:function(data)
		{
			var win = window.open("<?php echo site_url();?>pharmacy/print-prescription/<?php echo $visit_id;?>", '_blank');
			if(win){
				//Browser has allowed it to be opened
				win.focus();
			}else{
				//Broswer has blocked it
				alert('Please allow popups for this site');
			}
		},
		error: function(xhr, status, error) 
		{
			alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
		}
	});
	
	return false;
});

function get_visit_trail(visit_id)
{
	document.getElementById("loader").style.display = "block";
	var XMLHttpRequestObject = false;       
	if (window.XMLHttpRequest) {

	XMLHttpRequestObject = new XMLHttpRequest();
	} 

	else if (window.ActiveXObject) {
	XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
	}
	var config_url = $('#config_url').val();
	var url = config_url+"nurse/get_visit_trail/"+visit_id;
	// alert(url);
	if(XMLHttpRequestObject) {
	var obj = document.getElementById("patient-visit-trail");
	XMLHttpRequestObject.open("GET", url);
	   
	XMLHttpRequestObject.onreadystatechange = function(){
	 
	 if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

	   obj.innerHTML = XMLHttpRequestObject.responseText;
	   //obj2.innerHTML = XMLHttpRequestObject.responseText;
	  
	   document.getElementById("loader").style.display = "none";
	   // get_lab_table(visit_id);

	 }
	}

	XMLHttpRequestObject.send(null);
	}
}

function get_past_prescriptions(visit_id,patient_id)
{
	document.getElementById("loader").style.display = "block";
	var XMLHttpRequestObject = false;       
	if (window.XMLHttpRequest) {

	XMLHttpRequestObject = new XMLHttpRequest();
	} 

	else if (window.ActiveXObject) {
	XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
	}
	var config_url = $('#config_url').val();
	var url = config_url+"pharmacy/get_patient_previous_prescriptions/"+visit_id+"/"+patient_id;
	// alert(url);
	if(XMLHttpRequestObject) {
	var obj = document.getElementById("patient-previous-prescription");
	XMLHttpRequestObject.open("GET", url);
	   
	XMLHttpRequestObject.onreadystatechange = function(){
	 
	 if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

	   obj.innerHTML = XMLHttpRequestObject.responseText;
	   //obj2.innerHTML = XMLHttpRequestObject.responseText;
	  
	   document.getElementById("loader").style.display = "none";
	   // get_lab_table(visit_id);

	 }
	}

	XMLHttpRequestObject.send(null);
	}

}


	function get_service_charge_amount(service_charge_id)
	{
  	   var url = "<?php echo base_url();?>pharmacy/get_drug_price/"+service_charge_id;
  	   // alert(url);
	   $.ajax({
	   type:'POST',
	   url: url,
	   data:{service_charge_id: service_charge_id},
	   dataType: 'text',
	   success:function(data){
	   	 var data = jQuery.parseJSON(data);
         var amount = data.amount
         // alert(amount);
	   	 document.getElementById("drug-charge").value = amount;
	   },
	   error: function(xhr, status, error) {
	   alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
	   
	   }
	   });
	   
	   return false;
	}

	 function parse_drug_charge(visit_id)
	{
		var service_charge_id = document.getElementById("service_id_item").value;
		prescibe_drug(service_charge_id, visit_id);	    
	}
	function prescibe_drug(service_charge_id, visit_id){
    
	   var quantity = 1;
	   var x = 1;
	   var duration = 1;
	   var consumption = 1;
	   var charge = $('#drug-charge').val();;
	   var units_given = 1;
	   var input_total_units = 1;
	   
	   var url = "<?php echo base_url();?>pharmacy/add_pharmacy_charge/"+service_charge_id+"/"+visit_id;
	   
	   $.ajax({
	   type:'POST',
	   url: url,
	   data:{quantity: quantity, x: x, duration: duration,consumption: consumption,charge: charge, units_given: units_given,service_charge_id: service_charge_id,input_total_units: input_total_units},
	   dataType: 'text',
	   success:function(data){
	     // window.alert(data.result);
	      display_inpatient_prescription(visit_id,1);
	   },
	   error: function(xhr, status, error) {
	   alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
	   	 display_inpatient_prescription(visit_id,1);
	   }
	   });
	   // display_inpatient_prescription(visit_id,1);
	   return false;
	}



</script>                        