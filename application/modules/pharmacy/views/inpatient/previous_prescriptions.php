<div class="row">
	<div class="col-md-12">
        <section class="panel panel-featured panel-featured-info">
            <header class="panel-heading">
                <h2 class="panel-title">Prescribed drugs</h2>
            </header>
            
            <div class="panel-body">
                <table class='table table-striped table-condensed'>
                     <tr>
                        <th></th>
                        <th>Visit Date.</th>
                        <th>Doctor:</th>
                        <th>Prescription:</th>
                       
                    </tr>
                   <?php 


                   $patient_result = $this->pharmacy_model->get_all_patient_visits($patient_id);
                   // var_dump($patient_result); die();
                   $x = 0;
                   $result = '';
                   if($patient_result->num_rows() > 0)
                   {
                   		foreach ($patient_result->result() as $key => $value) {
                   			# code...
                   			$visit_date = $value->visit_date;
                   			$visit_id = $value->visit_id;
                   			$personnel_fname = $value->personnel_fname;
                   			$personnel_onames = $value->personnel_onames;

                   			$doctor = $personnel_onames.' '.$personnel_fname;
                        	$visit_date = date('l d F Y',strtotime($visit_date));

                   			// get all prescriptions done for that day
                   			$prescriptions_rs = $this->pharmacy_model->get_all_visit_prescriptions($visit_id);
                   			$num_rows =count($prescriptions_rs);
		                    $s=0;
		                    $visit_drug = '';
		                    if($num_rows > 0)
		                    {
			                    foreach ($prescriptions_rs as $key_rs):
			                        //var_dump($key_rs->prescription_substitution);
			                        $service_charge_id =$key_rs->product_id;
			                        $checker_id = $key_rs->checker_id;								
			                        $frequncy = $key_rs->drug_times_name;
			                        $id = $key_rs->prescription_id;
			                        $date1 = $key_rs->prescription_startdate;
			                        $date2 = $key_rs->prescription_finishdate;
			                        $sub = $key_rs->prescription_substitution;
			                        $duration = $key_rs->drug_duration_name;
			                        $sub = $key_rs->prescription_substitution;
			                        $duration = $key_rs->drug_duration_name;
			                        $consumption = $key_rs->drug_consumption_name;
			                        $quantity = $key_rs->prescription_quantity;
			                        $medicine = $key_rs->product_name;
			                        $charge = $key_rs->product_charge;
			                        $visit_charge_id = $key_rs->visit_charge_id;
			                        $number_of_days = $key_rs->number_of_days;
			                        $units_given = $key_rs->units_given;
									$prescription_id = $key_rs->prescription_id;
			                        $date = $key_rs->date;
			                        $time = $key_rs->time;
			                        $date_given = date('l d F Y',strtotime($date));


									$visit_drug .="<strong>Drug :</strong>".ucfirst(strtolower($medicine))." <strong> Units given: </strong> ".$units_given."<br>";
			                    endforeach;
			                }
			                $x++;
			                $result.='<tr>
			                			<td>'.$x.'</td>
			                			<td>'.$visit_date.'</td>
			                			<td>'.$doctor.'</td>
			                			<td>'.$visit_drug.'<td>
			                		  </tr>';

                   		}
                   }
                   echo $result;
                  ?>


              </table>
           </div>
       </section>
	</div>
</div>
