<?php
$drug_result = '';
if($query->num_rows() > 0)
{
	$count = $page;
	$drug_result .='
			<table class="table table-hover table-bordered table-striped table-responsive col-md-12" id="customers">
				<thead>
					<tr>
						<th>#</th>
						<th>Visit Date</th>
						<th>Patient No</th>
						<th>Patient Name</th>
						<th>PRN No</th>
						<th>Description</th>
						<th>QTY</th>
						<th>Amount</th>
						<th>Pharm</th>
						<th>Time</th>
					</tr>
				</thead>
				<tbody>
			';
	foreach($query->result() as $visit_drug_result)
	{
		// $visit_id = $visit_drug_result->visit_id;
		$service_charge = $visit_drug_result->service_charge_name;
		$service_amount = $visit_drug_result->service_charge_amount;
		$patient_surname = $visit_drug_result->patient_surname;
		$patient_othernames = $visit_drug_result->patient_othernames;
		$time = $visit_drug_result->time;
		$visit_charge_units = $visit_drug_result->visit_charge_units;
		$patient_type = $visit_drug_result->patient_type;
		$visit_charge_amount = $visit_drug_result->visit_charge_amount;
		$patient_number = $visit_drug_result->patient_number;
		$personnel_fname = $visit_drug_result->personnel_fname;
		$product_code = $visit_drug_result->product_code;
		$visit_date = date('jS M Y',strtotime($visit_drug_result->visit_date));
		$count++;
		
		
		$drug_result .='
					<tr>
						<td>'.$count.'</td>
						<td>'.$visit_date.'</td>
						<td>'.$patient_number.'</td>
						<td>'.$patient_surname.' '.$patient_othernames.'</td>
						<td>'.$product_code.'</td>
						<td>'.$service_charge.'</td>
						<td>'.$visit_charge_units.'</td>
						<td>'.number_format(($visit_charge_units * $visit_charge_amount),2).'</td>
						<td>'.$personnel_fname.'</td>
						<td>'.$time.'</td>
					</tr>';
	}
	$drug_result.='
				</tbody>
			</table>';
}
else
{
	$drug_result.= 'No drugs have been dispensed';
}
// echo $this->load->view('administration/reports/graphs/drug_sales', '', TRUE);
?>

<div class="row">
    <div class="col-md-12">
        <section class="panel panel-featured panel-featured-info">
            <header class="panel-heading">
            	 <h2 class="panel-title"><?php echo $title;?></h2>
            </header>
            <!-- Widget content -->
            <div class="panel-body">
                <div class="row">
                	<div class="col-md-4 col-md-offset-8">
                    	<a class="btn btn-sm btn-warning" id="open_search" onclick="open_search_box()" pull-right><i class="fa fa-search"></i> Open Search</a>
                    	<a class="btn btn-sm btn-info" id="close_search" style="display:none;" onclick="close_search()"><i class="fa fa-search-minus"></i> Close Search</a>
                        <?php
                        $search = $this->session->userdata('all_drugs_search');
						if(!empty($search))
						{
						?>
                    	<a class="btn btn-sm btn-danger" href="<?php echo site_url().'administration/reports/clear_drugs_search';?>"><i class="fa fa-search"></i> Clear Search</a>
                        <?php }?>
                    	<a class="btn btn-sm btn-success" target="_blank" href="<?php echo site_url().'administration/reports/export_drugs';?>">Download</a>
                    </div>
                </div>
                <div class="row">
                	<div class="col-md-12">
                        <div id="search_section" style="display:none;">
        
                            <?php echo $this->load->view("administration/reports/search/drugs", '', TRUE);?>
                        </div>
                    </div>
                </div>
                <div class="row">
                	<div class="col-md-12">
            			<?php echo $drug_result;?>
                    </div>
                </div>
            </div>
            <!--<a href="#" onClick ="$('#customers').tableExport({type:'excel',escape:'false'});">EXCEL DOWNLOADS</a>-->
            <div class="widget-foot">
                                
				<?php if(isset($links)){echo $links;}?>
            
                <div class="clearfix"></div> 
            
            </div>
        </section>
    </div>
</div>

<script type="text/javascript">

	function open_search_box()
	{
		var myTarget2 = document.getElementById("search_section");
		var button = document.getElementById("open_search");
		var button2 = document.getElementById("close_search");

		myTarget2.style.display = '';
		button.style.display = 'none';
		button2.style.display = '';
	}
	
	function close_search()
	{
		var myTarget2 = document.getElementById("search_section");
		var button = document.getElementById("open_search");
		var button2 = document.getElementById("close_search");

		myTarget2.style.display = 'none';
		button.style.display = '';
		button2.style.display = 'none';
	}
</script>