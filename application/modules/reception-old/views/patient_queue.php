<?php
$all_wards = '';
if($wards->num_rows() > 0)
{
	foreach($wards->result() as $row):
		$ward_name = $row->ward_name;
		$ward_id = $row->ward_id;
		
		if($ward_id == set_value('ward_id'))
		{
			$all_wards .= "<option value='".$ward_id."' selected='selected'>".$ward_name."</option>";
		}
		
		else
		{
			$all_wards .= "<option value='".$ward_id."'>".$ward_name."</option>";
		}
	endforeach;
}
$all_rooms = '';
if($rooms->num_rows() > 0)
{
	foreach($rooms->result() as $row):
		$room_name = $row->room_name;
		$room_id = $row->room_id;
		
		if($room_id == set_value('room_id'))
		{
			$all_rooms .= "<option value='".$room_id."' selected='selected'>".$room_name."</option>";
		}
		
		else
		{
			$all_rooms .= "<option value='".$room_id."'>".$room_name."</option>";
		}
	endforeach;
}
$all_doctors = '';
if($doctors->num_rows() > 0){
	foreach($doctors->result() as $row):
		$fname = $row->personnel_fname;
		$onames = $row->personnel_onames;
		$personnel_id = $row->personnel_id;
		
		if($personnel_id == set_value('personnel_id'))
		{
			$all_doctors .= "<option value='".$personnel_id."' selected='selected'>".$onames." ".$fname."</option>";
		}
		
		else
		{
			$all_doctors .= "<option value='".$personnel_id."'>".$onames." ".$fname."</option>";
		}
	endforeach;
}
?>
<!-- search -->
<?php echo $this->load->view('search/search_patients', '', TRUE);?>
<!-- end search -->
 
 <section class="panel panel-primary">
    <header class="panel-heading">
        <h2 class="panel-title"><?php echo $title;?> for <?php echo date('jS M Y',strtotime(date('Y-m-d')));?></h2>

        <div class="pull-right">
	          <a href="<?php echo site_url();?>patients" class="btn btn-info btn-sm pull-right " style="margin-top:-25px"><i class="fa fa-arrow-right"></i> Patients List</a>
	    </div>
    </header>
      <div class="panel-body">
          <div class="padd">
          
<?php
		$search = $this->session->userdata('general_queue_search');
		
		if(!empty($search))
		{
			echo '<a href="'.site_url().'reception/close_general_queue_search/'.$page_name.'" class="btn btn-warning">Close Search</a>';
		}
		$result = '';
		$dependant_id =0;
		//if users exist display them
		if ($query->num_rows() > 0)
		{
			$count = $page;
				
				if($page_name == 'nurse')
				{
					$actions = 5;
				}
				
				else if($page_name == 'doctor')
				{
					$actions = 4;
				}
				
				else if($page_name == 'laboratory')
				{
					$actions = 4;
				}
				
				else if($page_name == 'xray')
				{
					$actions = 4;
				}
				
				else if($page_name == 'ultrasound')
				{
					$actions = 4;
				}
				
				else if($page_name == 'pharmacy')
				{
					$actions = 3;
				}
				
				else if($page_name == 'accounts')
				{
					$actions = 5;
				}
				else if($page_name == 'administration')
				{
					$actions = 2;
				}
				
				else
				{
					$actions = 5;
				}
			
			$result .= 
				'
					<table class="table table-bordered ">
					  <thead>
						<tr>
						  <th>#</th>
						  <th>Visit Date</th>
						  <th>Patient Number</th>
						  <th>Patient Names</th>
						  <th>Visit Type</th>
						  <th>Coming From</th>
						  <th>Going To</th>
						  <th>Wait Time</th>
						  <th>Doctor</th>
						  <th colspan="'.$actions.'">Actions</th>
						</tr>
					  </thead>
					  <tbody>
				';
			
			$personnel_query = $this->personnel_model->get_all_personnel();
			
			foreach ($query->result() as $row)
			{
				$visit_date = date('jS M Y',strtotime($row->visit_date));
				$visit_time = date('H:i a',strtotime($row->visit_time));
				if($row->visit_time_out != '0000-00-00 00:00:00')
				{
					$visit_time_out = date('H:i a',strtotime($row->visit_time_out));
				}
				else
				{
					$visit_time_out = '-';
				}


				if($row->visit_time_out != '0000-00-00 00:00:00')
				{
					$visit_time_out = date('H:i a',strtotime($row->visit_time_out));
					$seconds = strtotime($row->visit_time_out) - strtotime($row->visit_time);//$row->waiting_time;
					$days    = floor($seconds / 86400);
					$hours   = floor(($seconds - ($days * 86400)) / 3600);
					$minutes = floor(($seconds - ($days * 86400) - ($hours * 3600))/60);
					$seconds = floor(($seconds - ($days * 86400) - ($hours * 3600) - ($minutes*60)));
					
					//$total_time = date('H:i',(strtotime($row->visit_time_out) - strtotime($row->visit_time)));//date('H:i',$row->waiting_time);
					$total_time = $days.' '.$hours.':'.$minutes.':'.$seconds;
				}
				else
				{

					$visit_time_out = date('Y-m-d H:i:s');
					// var_dump($visit_time_out); die();
					$seconds = strtotime($visit_time_out) - strtotime($row->visit_time);//$row->waiting_time;
					$days    = floor($seconds / 86400);
					$hours   = floor(($seconds - ($days * 86400)) / 3600);
					$minutes = floor(($seconds - ($days * 86400) - ($hours * 3600))/60);
					$seconds = floor(($seconds - ($days * 86400) - ($hours * 3600) - ($minutes*60)));
					
					//$total_time = date('H:i',(strtotime($row->visit_time_out) - strtotime($row->visit_time)));//date('H:i',$row->waiting_time);
					$total_time = $days.' '.$hours.':'.$minutes.':'.$seconds;



					$visit_time_out = '-';
					// $total_time = '-';
				}
					
				$visit_created = date('H:i a',strtotime($row->visit_date));
				$visit_id = $row->visit_id;
				$patient_id = $row->patient_id;
				$personnel_id = $row->personnel_id;
				$dependant_id = $row->dependant_id;
				$patient_number = $row->patient_number;
				$strath_no = $row->strath_no;
				$visit_type_id = $row->visit_type_id;
				$visit_type = $row->visit_type;
				$accounts = "";//$row->accounts;
				$visit_table_visit_type = $visit_type;
				$patient_table_visit_type = $visit_type_id;
				$coming_from = $this->reception_model->coming_from($visit_id);
				$sent_to = $this->reception_model->going_to($visit_id);

				$visit_type_name = $row->visit_type_name;
				$patient_othernames = $row->patient_othernames;
				$patient_surname = $row->patient_surname;
				$patient_date_of_birth = $row->patient_date_of_birth;
				$ward_id = 1;//$row->ward_id;

				$ward_rooms = $this->nurse_model->get_ward_rooms($ward_id);
				$rooms = $this->reception_model->get_rooms();
				// $beds = $this->reception_model->get_beds();
				


				$all_rooms = '';

				if($rooms->num_rows() > 0)
				{
				    foreach($rooms->result() as $res)
				    {
				        $room_id = $res->room_id;
				        $room_name = $res->room_name;
				        
				        if($room_id == set_value('room_id'))
				        {
				           $all_rooms .='<option value="'.$room_id.'" selected>'.$room_name.'</option>';
				        }
						else
						{
				            $all_rooms .= '<option value="'.$room_id.'">'.$room_name.'</option>';
						}
				    }
				}

				//$all_beds ='';
				//if($beds->num_rows() > 0)
				//{
				  //  foreach($all_beds->result() as $res)
				   // {
				    //    $bed_id = $res->bed_id;
				    //    $bed_name = $res->bed_name;
				        
				     //   if($bed_id == set_value('bed_id'))
				     //   {
				          //  $all_beds .= '<option value="'.$bed_id.'" selected>'.$bed_name.'</option>';
				      //  }
						//else
						//{
				       //     $all_beds .= '<option value="'.$bed_id.'">'.$bed_name.'</option>';
						//}
				    //}
				//}


				//cash paying patient sent to department but has to pass through the accounts
				if($coming_from == "Laboratory")
				{
					$balanced = 'info';
				}
				else
				{
					$balanced = 'default';
				}
				
				//creators and editors
				if($personnel_query->num_rows() > 0)
				{
					$personnel_result = $personnel_query->result();
					
					foreach($personnel_result as $adm)
					{
						$personnel_id2 = $adm->personnel_id;
						
						if($personnel_id == $personnel_id2)
						{
							$doctor = $adm->personnel_onames;
							break;
						}
						
						else
						{
							$doctor = '-';
						}
					}
				}
				
				else
				{
					$doctor = '-';
				}
				$v_data = array('visit_id'=>$visit_id);
				$count++;
				

				$personnel_id = $this->session->userdata('personnel_id');
				$is_nurse = $this->reception_model->check_if_admin($personnel_id,24);

				$personnel_id = $this->session->userdata('personnel_id');
				$is_doctor = $this->reception_model->check_if_admin($personnel_id,12);

				if($is_nurse || $is_doctor)
				{
					$department_id = 2;
					$buttons = '
					<input type="hidden" name="department_id" id="department_id" value="'.$department_id.'" >

					<td><a href="'.site_url().'nurse/patient_card/'.$visit_id.'/a/0" class="btn btn-sm btn-info">Patient Card</a></td>
					<td><a href="'.site_url().'patient-uploads/'.$patient_id.'" class="btn btn-sm btn-primary">Uploads</a></td>
					<td>
						<button type="button" class="btn btn-sm btn-warning" data-toggle="modal" data-target="#create_inpatient'.$visit_id.'">Inpatient</button>
						
						<div class="modal fade" id="create_inpatient'.$visit_id.'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
							<div class="modal-dialog" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
										<h4 class="modal-title" id="myModalLabel">Change to inpatient</h4>
									</div>
									<div class="modal-body">
										'.form_open('reception/change_patient_visit/'.$visit_id.'/'.$visit_type_id, array("class" => "form-horizontal")).'
										<div class="form-group">
											<label class="col-md-4 control-label">Ward: </label>
											
											<div class="col-md-8">
												<select name="ward_id" id="ward_id" class="form-control" >
													<option value="">----Select a ward----</option>
													'.$all_wards.'
												</select>
											</div>
										</div>

										

										<div class="form-group">
											<label class="col-md-4 control-label">Doctor: </label>
											
											<div class="col-md-8">
												 <select name="personnel_id" id="personnel_id" class="form-control custom-select">
													<option value="">----Select a Doctor----</option>
													'.$all_doctors.'
												</select>
											</div>
										</div>
										<div class="form-group">
											<label class="col-lg-4 control-label">Admission date: </label>
											
											<div class="col-lg-8">
												<div class="input-group">
													<span class="input-group-addon">
														<i class="fa fa-calendar"></i>
													</span>
													<input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="visit_date" placeholder="Admission Date" value="'.date('Y-m-d').'">
												</div>
											</div>
										</div>
										
										<div class="row">
											<div class="col-md-8 col-md-offset-4">
												<div class="center-align">
													<button type="submit" class="btn btn-primary">Create inpatient</button>
												</div>
											</div>
										</div>
										'.form_close().'
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
					</td>
					

					';
				}
				

			    $personnel_id = $this->session->userdata('personnel_id');
				$is_doctor = $this->reception_model->check_if_admin($personnel_id,43);

				if($is_doctor)
				{
					$department_id = 45;
					$buttons = '
					<input type="hidden" name="department_id" id="department_id" value="'.$department_id.'" >
					<td>
						<a  class="btn btn-sm btn-danger" id="open_visit'.$visit_id.'" onclick="get_visit_trail('.$visit_id.');">Visit Trail</a>
						<a  class="btn btn-sm btn-danger" id="close_visit'.$visit_id.'" style="display:none;" onclick="close_visit_trail('.$visit_id.');">Close Trail</a></td>
					</td>

					<td><a href="'.site_url().'nurse/patient_card/'.$visit_id.'/a/0" class="btn btn-sm btn-info">Patient Card</a></td>
					<td><a href="'.site_url().'accounts/payments1/'.$patient_id.'" class="btn btn-sm btn-primary" >Payments</a></td>
					<td><a href="'.site_url().'care-units/'.$visit_id.'" class="btn btn-sm btn-primary">ICU/HDU/Theatre</a></td>
					<td><a href="'.site_url().'patient-uploads/'.$patient_id.'" class="btn btn-sm btn-primary">Uploads</a></td>
					<td>
						<button type="button" class="btn btn-sm btn-warning" data-toggle="modal" data-target="#create_inpatient'.$visit_id.'">Inpatient</button>
						
						<div class="modal fade" id="create_inpatient'.$visit_id.'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
							<div class="modal-dialog" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
										<h4 class="modal-title" id="myModalLabel">Change to inpatient</h4>
									</div>
									<div class="modal-body">
										'.form_open('reception/change_patient_visit/'.$visit_id.'/'.$visit_type_id, array("class" => "form-horizontal")).'
										<div class="form-group">
											<label class="col-md-4 control-label">Ward: </label>
											
											<div class="col-md-8">
												<select name="ward_id" id="ward_id" class="form-control" >
													<option value="">----Select a ward----</option>
													'.$all_wards.'
												</select>
											</div>
										</div>

										

										<div class="form-group">
											<label class="col-md-4 control-label">Doctor: </label>
											
											<div class="col-md-8">
												 <select name="personnel_id" id="personnel_id" class="form-control custom-select">
													<option value="">----Select a Doctor----</option>
													'.$all_doctors.'
												</select>
											</div>
										</div>
										<div class="form-group">
											<label class="col-lg-4 control-label">Admission date: </label>
											
											<div class="col-lg-8">
												<div class="input-group">
													<span class="input-group-addon">
														<i class="fa fa-calendar"></i>
													</span>
													<input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="visit_date" placeholder="Admission Date" value="'.date('Y-m-d').'">
												</div>
											</div>
										</div>
										
										<div class="row">
											<div class="col-md-8 col-md-offset-4">
												<div class="center-align">
													<button type="submit" class="btn btn-primary">Create inpatient</button>
												</div>
											</div>
										</div>
										'.form_close().'
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
					</td>
					

					';
				}
				$personnel_id = $this->session->userdata('personnel_id');
				$is_lab = $this->reception_model->check_if_admin($personnel_id,25);

				
				if($is_lab)
				{
					$department_id = 30;
					$buttons = '
					<input type="hidden" name="department_id" id="department_id" value="'.$department_id.'" >
					<td>
						<a  class="btn btn-sm btn-danger" id="open_visit'.$visit_id.'" onclick="get_visit_trail('.$visit_id.');">Visit Trail</a>
						<a  class="btn btn-sm btn-danger" id="close_visit'.$visit_id.'" style="display:none;" onclick="close_visit_trail('.$visit_id.');">Close Trail</a></td>
					</td>

					<td><a href="'.site_url().'laboratory/test/'.$visit_id.'" class="btn btn-sm btn-info">Tests</a></td>
					<td><a href="'.site_url().'patient-uploads/'.$patient_id.'" class="btn btn-sm btn-primary">Uploads</a></td>
					';
				}

				$personnel_id = $this->session->userdata('personnel_id');
				$is_records = $this->reception_model->check_if_admin($personnel_id,35);

				if($is_records)
				{

					$department_id = 0;
					$buttons = '  
					 <td>
									<button type="button" class="btn btn-sm btn-warning" data-toggle="modal" data-target="#create_inpatient'.$patient_id.'">Discharged</button>

					<input type="hidden" name="department_id" id="department_id" value="'.$department_id.'" >					
                    <td><a href="'.site_url().'nurse/inpatient_card/'.$visit_id.'/a/0" class="btn btn-sm btn-info">Patient Card</a></td>
                    <td><a href="'.site_url().'patient-uploads/'.$patient_id.'" class="btn btn-sm btn-primary">Uploads</a></td>
					<td>
                        <button type="button" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#create_inpatient'.$patient_id.'">Discharge</button>

								
									<div class="modal fade" id="create_inpatient'.$patient_id.'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
										<div class="modal-dialog" role="document">
											<div class="modal-content">
												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
													<h4 class="modal-title" id="myModalLabel">Change Patient Status to Discharged</h4>
												</div>
												<div class="modal-body">
													'.form_open('reception/change_discharged_status/'.$patient_id, array("class" => "form-horizontal")).'
													<div class="form-group">
														<label class="col-lg-4 control-label">Discharged date: </label>
														<input type="hidden" class="form-control" name="redirect_url" placeholder="" autocomplete="off" value="'.$this->uri->uri_string().'">
														<div class="col-lg-8">
															<div class="input-group">
																<span class="input-group-addon">
																	<i class="fa fa-calendar"></i>
																</span>

																<input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="discharged_date" placeholder="RIP Date" value="'.date('Y-m-d').'">

																<input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="discharged_date" placeholder="Discharged Date" value="'.date('Y-m-d').'">

															</div>
														</div>
													</div>
													
													<div class="row">
														<div class="col-md-12">
															<div class="center-align">
																<button type="submit" class="btn btn-primary">Update Status</button>
															</div>
														</div>
													</div>
													'.form_close().'
												</div>
												<div class="modal-footer">
													<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
												</div>
											</div>
										</div>
									</div>
						</td>
									<td><a href="'.site_url().'nurse/patient_card/'.$visit_id.'/a/0" class="btn btn-sm btn-info">Patient Card</a>
									</td>
							
						
					
				

					';
				}
				
				$personnel_id = $this->session->userdata('personnel_id');
				$is_phamacist = $this->reception_model->check_if_admin($personnel_id,27);

				if($is_phamacist)
				{
					$department_id = 5;
					$buttons = '
					<input type="hidden" name="department_id" id="department_id" value="'.$department_id.'" >
					<td>
						<a  class="btn btn-sm btn-danger" id="open_visit'.$visit_id.'" onclick="get_visit_trail('.$visit_id.');">Visit Trail</a>
						<a  class="btn btn-sm btn-danger" id="close_visit'.$visit_id.'" style="display:none;" onclick="close_visit_trail('.$visit_id.');">Close Trail</a></td>
					</td>


					<td><a href="'.site_url().'pharmacy/prescription1/'.$visit_id.'/1" class="btn btn-sm btn-info">Prescription</a></td>

					<td><a href="'.site_url().'patient-bill/'.$patient_id.'" class="btn btn-sm btn-primary" >Bill</a></td>';
				}

				$personnel_id = $this->session->userdata('personnel_id');
				$is_accountant = $this->reception_model->check_if_admin($personnel_id,28);
				
				if($is_accountant)
				{
					$department_id = 6;
					$buttons = '

					<td><a href="'.site_url().'accounts/payments/'.$patient_id.'" class="btn btn-sm btn-primary" >Payments</a></td>
					<td><a href="'.site_url().'reception/end_visit/'.$visit_id.'/1" class="btn btn-sm btn-danger" onclick="return confirm(\'End this visit?\');">End Visit</a></td>
					';
				}

				$personnel_id = $this->session->userdata('personnel_id');
				$is_accountant = $this->reception_model->check_if_admin($personnel_id,42);
				
				if($is_accountant)
				{
					$department_id = 44;
					$buttons = '
					<td>
					<input type="hidden" name="department_id" id="department_id" value="'.$department_id.'" >
						<a  class="btn btn-sm btn-success" id="open_visit'.$visit_id.'" onclick="get_visit_trail('.$visit_id.');">Visit Trail</a>
						<a  class="btn btn-sm btn-success" id="close_visit'.$visit_id.'" style="display:none;" onclick="close_visit_trail('.$visit_id.');">Close Trail</a></td>
					</td>

					<td><a href="'.site_url().'accounts/print_receipt_new/'.$visit_id.'" target="_blank" class="btn btn-sm btn-info">Receipt</a></td>
					<td><a href="'.site_url().'accounts/print_invoice_new/'.$visit_id.'" target="_blank" class="btn btn-sm btn-success">Invoice </a></td>
					<td><a href="'.site_url().'accounts/payments/'.$patient_id.'" class="btn btn-sm btn-primary" >Payments</a></td>
					<td><a href="'.site_url().'reception/end_visit/'.$visit_id.'/1" class="btn btn-sm btn-danger" onclick="return confirm(\'End this visit?\');">End Visit</a></td>
					';
				}

				
				$is_admin = $this->reception_model->check_if_admin($personnel_id,1);

				if($is_admin OR $personnel_id == 0)
				{
					// var_dump($personnel_id); die();
					$department_id = 2;

					$buttons = '

					<input type="hidden" name="department_id" id="department_id" value="'.$department_id.'" >
					<td><a href="'.site_url().'nurse/patient_card/'.$visit_id.'/a/0" class="btn btn-sm btn-info"> Card</a></td>
					<td><a href="'.site_url().'pharmacy/prescription1/'.$visit_id.'/1" class="btn btn-sm btn-info">Prescription</a></td>
					<td><a href="'.site_url().'laboratory/test/'.$visit_id.'" class="btn btn-sm btn-info">Tests</a></td>	
					<td><a href="'.site_url().'care-units/'.$visit_id.'" class="btn btn-sm btn-primary">ICU/HDU</a></td>
					<td><a href="'.site_url().'accounts/payments/'.$patient_id.'" class="btn btn-sm btn-primary" >Payments</a></td>
					
					';
					
				}
				

				$is_receptionist = $this->reception_model->check_if_admin($personnel_id,10);

				if($is_receptionist)
				{
					// var_dump($personnel_id); die();
					$department_id = 0;

					$buttons = '
					<input type="hidden" name="department_id" id="department_id" value="'.$department_id.'" >
					
					<td>
						<button type="button" class="btn btn-sm btn-warning" data-toggle="modal" data-target="#create_inpatient'.$visit_id.'">Inpatient</button>
						
						<div class="modal fade" id="create_inpatient'.$visit_id.'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
							<div class="modal-dialog" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
										<h4 class="modal-title" id="myModalLabel">Change to inpatient</h4>
									</div>
									<div class="modal-body">
										'.form_open('reception/change_patient_visit/'.$visit_id.'/'.$visit_type_id, array("class" => "form-horizontal")).'
										<div class="form-group">
											<label class="col-md-4 control-label">Ward: </label>
											
											<div class="col-md-8">
												<select name="ward_id" id="ward_id" class="form-control" >
													<option value="">----Select a ward----</option>
													'.$all_wards.'
												</select>
											</div>
										</div>
										<div class="form-group">
											<label class="col-md-4 control-label">Room: </label>
											
											<div class="col-md-8">
												<select name="room_id" id="room_id" class="form-control" >
													<option value="">----Select a Room----</option>
													'.$all_rooms.'
												</select>
											</div>
										</div>
                                     
                                     <div class="form-group">
											<label class="col-md-4 control-label">Doctor: </label>
											
											<div class="col-md-8">
												 <select name="personnel_id" id="personnel_id" class="form-control custom-select">
													<option value="">----Select a Doctor----</option>
													'.$all_doctors.'
												</select>
											</div>
										</div>
                                
										<div class="form-group">
											<label class="col-lg-4 control-label">Admission date: </label>
											
											<div class="col-lg-8">
												<div class="input-group">
													<span class="input-group-addon">
														<i class="fa fa-calendar"></i>
													</span>
													<input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="visit_date" placeholder="Admission Date" value="'.date('Y-m-d').'">
												</div>
											</div>
										</div>
										
										<div class="row">
											<div class="col-md-8 col-md-offset-4">
												<div class="center-align">
													<button type="submit" class="btn btn-primary">Create inpatient</button>
												</div>
											</div>
										</div>
										'.form_close().'
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
					</td>
					<td><a href="'.site_url().'reception/edit_visit/'.$visit_id.'" class="btn btn-sm btn-primary"> Edit </a></td>
					<td><a href="'.site_url().'patient-uploads/'.$patient_id.'" class="btn btn-sm btn-primary">Uploads</a></td>
				';
					
				}
				
								
					
				$result .= 
					'
						<tr >
							<td>'.$count.'</td>
							<td class="'.$balanced.'">'.$visit_date.'</td>
							<td class="'.$balanced.'">'.$patient_number.'</td>
							<td class="'.$balanced.'">'.$patient_surname.' '.$patient_othernames.'</td>
							<td>'.$visit_type_name.'</td>
							<td>'.$coming_from.'</td>
							<td>'.$sent_to.'</td>
							<td>'.$total_time.'</td>
						    <td>'.$doctor.'</td>
							'.$buttons.'
						</tr> 
					';
					if($page_name == 'accounts')
					{
						$pink = 15;
					}
					if($page_name == 'administration')
					{
						$pink = 15;
					}
					else if($page_name == 'laboratory')
					{
						$pink = 12;
					}
					else
					{
						$pink = 12;
					}
					$v_data['patient_type'] = $visit_type_id;
			
			}
			
			$result .= 
			'
						  </tbody>
						</table>
			';
		}
		
		else
		{
			$result .= "There are no patients";
		}
		
?>
		<?php
		$error = $this->session->userdata('error_message');
		$success = $this->session->userdata('success_message');
		
		if(!empty($error))
		{
			echo '<div class="alert alert-danger">'.$error.'</div>';
			$this->session->unset_userdata('error_message');
		}
		
		if(!empty($success))
		{
			echo '<div class="alert alert-success">'.$success.'</div>';
			$this->session->unset_userdata('success_message');
		}
		echo $result;
		?>
          </div>
          
          <div class="widget-foot">
                                
				<?php if(isset($links)){echo $links;}?>
            
                <div class="clearfix"></div> 
            
            </div>
        </div>
        <!-- Widget ends -->
       

  </section>

  <audio id="sound1" src="<?php echo base_url();?>sound/beep.mp3"></audio>
  <script type="text/javascript">
  	$(document).ready(function(){
  	   $("#personnel_id").customselect();
       $("#bed_id").customselect();
       $("#room_id").customselect();
       var department_id = document.getElementById("department_id").value;
       // alert(department_id);
		// setInterval(function(){check_new_patients(department_id)},10000);

	 });

  
   	function check_new_patients(module)
		{	
		 var XMLHttpRequestObject = false;
        
		    if (window.XMLHttpRequest) {
		    
		        XMLHttpRequestObject = new XMLHttpRequest();
		    } 
		        
		    else if (window.ActiveXObject) {
		        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
		    }
		    
		    var config_url = $('#config_url').val();
		    var url = config_url+"nurse/check_queues/"+module;
		    // alert(url);
		    if(XMLHttpRequestObject) {
		                
		        XMLHttpRequestObject.open("GET", url);
		                
		        XMLHttpRequestObject.onreadystatechange = function(){
		            
		            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
		            	
	         			var one = XMLHttpRequestObject.responseText;
	         			if(one == 1)
	         			{
	         				 var audio1 = document.getElementById("sound1");
						 	 if (audio1.paused !== true){
							    audio1.pause();
							 }
							 else
							 {
								audio1.play();
							 }
	         			}
	         			else
	         			{

	         			}
			         	
	         
		            }
		        }
		                
		        XMLHttpRequestObject.send(null);
		    }
		}

	function get_visit_trail(visit_id){

		var myTarget2 = document.getElementById("visit_trail"+visit_id);
		var button = document.getElementById("open_visit"+visit_id);
		var button2 = document.getElementById("close_visit"+visit_id);

		myTarget2.style.display = '';
		button.style.display = 'none';
		button2.style.display = '';
	}
	function close_visit_trail(visit_id){

		var myTarget2 = document.getElementById("visit_trail"+visit_id);
		var button = document.getElementById("open_visit"+visit_id);
		var button2 = document.getElementById("close_visit"+visit_id);

		myTarget2.style.display = 'none';
		button.style.display = '';
		button2.style.display = 'none';
	}
  </script>

  <script type="text/javascript">
	
	$(document).on("change","select#ward_id",function(e)
	{
		var ward_id = $(this).val();
		
		var url = "<?php echo site_url();?>nurse/get_ward_rooms/"+ward_id;
		// alert(url);
		//get rooms
		$.get( url , function( data ) 
		{
			$( "#room_id" ).html( data );
			
			$.get( "<?php echo site_url();?>nurse/get_room_beds/0", function( data ) 
			{
				$( "#bed_id" ).html( data );
			});
		});
	});
	
	$(document).on("change","select#room_id",function(e)
	{
		var room_id = $(this).val();
		
		//get beds
		$.get( "<?php echo site_url();?>nurse/get_room_beds/"+room_id, function( data ) 
		{
			$( "#bed_id" ).html( data );
		});
	});
</script>

 