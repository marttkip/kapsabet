<?php
//patient data
$row = $patient->row();
$patient_surname = $row->patient_surname;
$patient_othernames = $row->patient_othernames;
$title_id = $row->title_id;
$patient_date_of_birth = $row->patient_date_of_birth;
$gender_id = $row->gender_id;
$county_id = $row->county_id;
$religion_id = $row->religion_id;
$civil_status_id = $row->civil_status_id;
$patient_email = $row->patient_email;
$patient_address = $row->patient_address;
$patient_postalcode = $row->patient_postalcode;
$patient_town = $row->patient_town;
$patient_phone1 = $row->patient_phone1;
$patient_phone2 = $row->patient_phone2;
$patient_nationality = $row->patient_nationality;
$patient_location = $row->patient_location;
$patient_village = $row->patient_village;
$patient_refferal = $row->patient_refferal;
$patient_town = $row->patient_town;
$revisit_id = $row->revisit_id;
$patient_age = $row->patient_age;
$patient_district = $row->patient_district;
$patient_occupation = $row->patient_occupation;
$patient_kin_sname = $row->patient_kin_sname;
$patient_kin_othernames = $row->patient_kin_othernames;
$relationship_id = $row->relationship_id;
$patient_national_id = $row->patient_national_id;
$insurance_company_id = $row->insurance_company_id;
$next_of_kin_contact = $row->patient_kin_phonenumber1;
$current_patient_number = $row->current_patient_number;
$ethnic_id = $row->ethnic_id;
$contibutor_nhif = $row->contibutor_nhif;
$contibutor_contact= $row->contibutor_contact;
$current_patient_number = $row->current_patient_number;
$patient_employeer = $row->patient_employeer;
$patient_employeer_address = $row->patient_employeer_address;
$patient_employeer_phone = $row->patient_employeer_phone;

//echo $gender_id;
//repopulate data if validation errors occur
$validation_error = validation_errors();
                
if(!empty($validation_error))
{
    $patient_surname = set_value('patient_surname');
    $patient_othernames = set_value('patient_othernames');
    $title_id = set_value('title_id');
    $patient_date_of_birth = set_value('patient_date_of_birth');
    $religion_id = set_value('religion_id');
    $gender_id = set_value('gender_id');
    $county_id = set_value('county_id');
    $religion_id = set_value('religion_id');
    $civil_status_id = set_value('civil_status_id');
    $patient_email = set_value('patient_email');
    $patient_address = set_value('patient_address');
    $patient_postalcode = set_value('patient_postalcode');
    $patient_town = set_value('patient_town');
    $patient_phone1 = set_value('patient_phone1');
    $patient_phone2 = set_value('patient_phone2');
    $patient_location = set_value('patient_location');
    $patient_village = set_value('patient_village');
    $patient_district = set_value('patient_district');
    $patient_occupation = set_value('patient_occupation');
    $patient_nationality = set_value('patient_nationality');
    $patient_age = set_value('patient_age');
    $revisit_id = set_value('revisit_id');
    $patient_kin_sname = set_value('patient_kin_sname');
    $patient_kin_othernames = set_value('patient_kin_othernames');
    $relationship_id = set_value('relationship_id');
    $insurance_company_id = set_value('insurance_company_id');
    $patient_national_id = set_value('patient_national_id');
    $next_of_kin_contact = set_value('next_of_kin_contact');
    $current_patient_number = set_value('current_patient_number');
    $ethnic_id= set_value('ethnic_id');
    $contibutor_contact = set_value('contibutor_contact');
    $contibutor_nhif = set_value('contibutor_nhif');
     $patient_employeer = set_value('patient_employeer');
      $patient_employeer_address = set_value('patient_employeer_address');
       $patient_employeer_phone = set_value('patient_employeer_phone');
 
}
?>
 <section class="panel">
    <div class="row">
        <div class="col-md-12">

          <!-- Widget -->
         <header class="panel-heading">
              <h4 class="pull-left"><i class="icon-reorder"></i>Edit <?php echo $patient_surname.' '.$patient_othernames;?></h4>
              <div class="widget-icons pull-right">
                     <a href="<?php echo site_url();?>patients" class="btn btn-info btn-sm pull-right"> <i class="fa fa-arrow-left"></i> Patients List</a>
              </div>
              <div class="clearfix"></div>
        </header>             

            <!-- Widget content -->
        <div class="panel-body">
              <div class="padd">
              <div class="center-align">
                <?php
                    $error = $this->session->userdata('error_message');
                    $success = $this->session->userdata('success_message');
                    
                    if(!empty($error))
                    {
                        echo '<div class="alert alert-danger">'.$error.'</div>';
                        $this->session->unset_userdata('error_message');
                    }
                    
                    if(!empty($validation_error))
                    {
                        echo '<div class="alert alert-danger">'.$validation_error.'</div>';
                    }
                    
                    if(!empty($success))
                    {
                        echo '<div class="alert alert-success">'.$success.'</div>';
                        $this->session->unset_userdata('success_message');
                    }
                ?>
              </div>
                <?php echo form_open($this->uri->uri_string(), array("class" => "form-horizontal"));?>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group" style="display: none">
                                <label class="col-md-4 control-label">Title: </label>
                                
                                <div class="col-md-8">
                                    <select class="form-control" name="title_id">
                                        <?php
                                            if($titles->num_rows() > 0)
                                            {
                                                $title = $titles->result();
                                                
                                                foreach($title as $res)
                                                {
                                                    $db_title_id = $res->title_id;
                                                    $title_name = $res->title_name;
                                                    
                                                    if($db_title_id == $title_id)
                                                    {
                                                        echo '<option value="'.$db_title_id.'" selected>'.$title_name.'</option>';
                                                    }
                                                    
                                                    else
                                                    {
                                                        echo '<option value="'.$db_title_id.'">'.$title_name.'</option>';
                                                    }
                                                }
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>
                         
                          
                            <div class="form-group">
                                <label class="col-md-4 control-label">Surname: </label>
                                
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="patient_surname" placeholder="Surname" value="<?php echo $patient_surname;?>">
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="col-md-4 control-label">Other Names: </label>
                                
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="patient_othernames" placeholder="Other Names" value="<?php echo $patient_othernames;?>">
                                </div>
                            </div>
                            
                             <!-- <div class="form-group">
                                <label class="col-md-4 control-label">Patients Date of birth: </label>
                                
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="patient_date_of_birth" placeholder="patients dob" value="<?php echo $patient_date_of_birth;?>">
                                </div>
                            </div> -->
                             <div class="form-group">
                                <label class="col-md-4 control-label">Gender: </label>
                                
                                <div class="col-md-8">
                                    <select class="form-control" name="gender_id">
                                        <?php
                                            if($genders->num_rows() > 0)
                                            {
                                                $gender = $genders->result();
                                                
                                                foreach($gender as $res)
                                                {
                                                    $db_gender_id = $res->gender_id;
                                                    $gender_name = $res->gender_name;
                                                    
                                                    if($db_gender_id == $gender_id)
                                                    {
                                                        echo '<option value="'.$db_gender_id.'" selected>'.$gender_name.'</option>';
                                                    }
                                                    
                                                    else
                                                    {
                                                        echo '<option value="'.$db_gender_id.'">'.$gender_name.'</option>';
                                                    }
                                                }
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label">National ID: </label>
                                
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="patient_national_id" placeholder="National ID" value="<?php echo $patient_national_id;?>">
                                </div>
                            </div>
                            

                          <div class="form-group">
                                 <label class="col-md-4 control-label">Date of Birth : </label>
                
                                     <div class="col-md-8">
                                         <div class="input-group">
                                        <span class="input-group-addon">
                                          <i class="fa fa-calendar"></i>
                                         </span>
                                    <input data-format="yyyy-MM-dd" id="date_of_birth" type="text" data-plugin-datepicker class="form-control" name="patient_dob" placeholder="Date of Birth"  value="<?php echo $patient_date_of_birth;?>"> <a onclick="calculate_age()" class="btn btn-success">Get Age</a>
                                     </div>
                                  </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Year: </label>
                                
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="patient_year"  placeholder=" Year" >
                                </div>
                            </div>

                             <div class="form-group" style="display: none">
                                <label class="col-md-4 control-label">Age: </label>
                                
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="patient_age" placeholder="age" value="<?php echo $patient_age;?>">
                                </div>
                            </div>
                            
                           
                        <div class="form-group" style="display: none">
                              <label class="col-lg-4 control-label">Religion: </label>
            
                         <div class="col-lg-8">
                             <select class="form-control" name="religion_id">
                            <?php
                                if($religions->num_rows() > 0)
                                {
                                    $religion = $religions->result();
                                    
                                    foreach($religion as $res)
                                    {
                                        $db_religion_id = $res->religion_id;
                                        $religion_name = $res->religion_name;
                                        
                                        if($db_religion_id == $religion_id)
                                        {
                                            echo '<option value="'.$db_religion_id.'" selected>'.$religion_name.'</option>';
                                        }
                                        
                                        else
                                        {
                                            echo '<option value="'.$db_religion_id.'">'.$religion_name.'</option>';
                                        }
                                    }
                                }
                            ?>
                        </select>
                    </div>
                   </div>
                   <div class="form-group" style="display: none">
                              <label class="col-lg-4 control-label">Civil Status : </label>
            
                         <div class="col-lg-8">
                             <select class="form-control" name="civil_status_id">
                            <?php
                                if($civil_statuses->num_rows() > 0)
                                {
                                    $status = $civil_statuses->result();
                                    
                             foreach($status as $res)
                                   {
                                $db_status_id = $res->civil_status_id;
                                $status_name = $res->civil_status_name;
                                
                                if($db_status_id == $civil_status_id)
                                {
                                    echo '<option value="'.$db_status_id.'" selected>'.$status_name.'</option>';
                                }
                                
                                else
                                {
                                    echo '<option value="'.$db_status_id.'">'.$status_name.'</option>';
                                }
                               }
                        }
                    ?>
                              </select>
                      </div>
                </div>
                            
                            
                           
                            <div class="form-group" style="display: none">
                                <label class="col-md-4 control-label">Email Address: </label>
                                
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="patient_email" placeholder="Email Address" value="<?php echo $patient_email;?>">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label">Postal Address: </label>
                                
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="patient_postalcode" placeholder="Postal Address" value="<?php echo $patient_email;?>">
                                </div>
                            </div>
                            
                            
                  <div class="form-group" style="display: none">
                                <label class="col-md-4 control-label">Nationality: </label>
                                
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="patient_nationality" placeholder="Nationality" value="<?php echo $patient_nationality;?>">
                                </div>
                            </div>
                             <div class="form-group" style="display: none">
                                <label class="col-md-4 control-label">Refferal Facility: </label>
                                
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="patient_refferal" placeholder="Refferal Facility" value="<?php echo $patient_refferal;?>">
                                </div>
                            </div>
                             <div class="form-group">
                                <label class="col-md-4 control-label">Primary Phone: </label>
                                
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="patient_phone1" placeholder="Primary Phone" value="<?php echo $patient_phone1;?>">
                                </div>
                            </div>
                             <div class="form-group">
                                <label class="col-md-4 control-label">Town: </label>
                                
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="patient_town" placeholder="Town" value="<?php echo $patient_town;?>">
                                </div>
                            </div>
                             <div class="form-group">
                                <label class="col-md-4 control-label">Village: </label>
                                
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="patient_village" placeholder="Village" value="<?php echo $patient_village;?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Location: </label>
                                
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="patient_location" placeholder="Location" value="<?php echo $patient_location;?>">
                                </div>
                            </div>
                           
                            
                        </div>
                        
                        <div class="col-md-6">
                            
                           
                            
                           
                            <div class="form-group">
                                <label class="col-md-4 control-label">Sub-county </label>
                                
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="patient_district" placeholder="District" value="<?php echo $patient_district;?>">
                                </div>
                            </div>
                             <div class="form-group">
                                <label class="col-md-4 control-label">County : </label>
                                
                                <div class="col-md-8">
                                    <select class="form-control" name="county_id">
                                        <?php
                                            if($countys->num_rows() > 0)
                                            {
                                                $county = $countys->result();
                                                
                                                foreach($county as $res)
                                                {
                                                    $db_county_id = $res->county_id;
                                                    $county_name = $res->county_name;
                                                    
                                                    if($db_county_id == $county_id)
                                                    {
                                                        echo '<option value="'.$db_county_id.'" selected>'.$county_name.'</option>';
                                                    }
                                                    
                                                    else
                                                    {
                                                        echo '<option value="'.$db_county_id.'">'.$county_name.'</option>';
                                                    }
                                                }
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>
                              <div class="form-group" style="display: none">
                                <label class="col-md-4 control-label">Ethnic : </label>
                                
                                <div class="col-md-8">
                                    <select class="form-control" name="ethnic_id">
                                        <?php
                                            if($ethnics->num_rows() > 0)
                                            {
                                                $ethnic = $ethnics->result();
                                                
                                                foreach($ethnic as $res)
                                                {
                                                    $db_ethnic_id = $res->ethnic_id;
                                                    $ethnic_name = $res->ethnic_name;
                                                    
                                                    if($db_ethnic_id == $ethnic_id)
                                                    {
                                                        echo '<option value="'.$db_ethnic_id.'" selected>'.$ethnic_name.'</option>';
                                                    }
                                                    
                                                    else
                                                    {
                                                        echo '<option value="'.$db_ethnic_id.'">'.$ethnic_name.'</option>';
                                                    }
                                                }
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="col-md-4 control-label">Occupation: </label>
                                
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="patient_occupation" placeholder="Occupation" value="<?php echo $patient_occupation;?>">
                                </div>
                            </div>
                             <div class="form-group">
                                <label class="col-md-4 control-label">Employeer: </label>
                                
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="patient_employeer" placeholder="Employeer" value="<?php echo $patient_employeer;?>">
                                </div>
                            </div>
                             <div class="form-group">
                                <label class="col-md-4 control-label">Employer Address: </label>
                                
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="patient_employeer_address" placeholder="Employeer Address" value="<?php echo $patient_employeer_address;?>">
                                </div>
                            </div>
                             <div class="form-group">
                                <label class="col-md-4 control-label">Employeer Phone no: </label>
                                
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="patient_employeer_phone" placeholder="Employeer Phone" value="<?php echo $patient_employeer_phone;?>">
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="col-md-4 control-label">Next of Kin Name: </label>
                                
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="patient_kin_sname" placeholder="Name" value="<?php echo $patient_kin_sname;?>">
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="col-md-4 control-label">Next of Kin Contact: </label>
                                
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="next_of_kin_contact" placeholder="" value="<?php echo $next_of_kin_contact;?>">
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="col-md-4 control-label">Relationship To Kin: </label>
                                
                                <div class="col-md-8">
                                    <select class="form-control" name="relationship_id">
                                        <?php
                                            if($relationships->num_rows() > 0)
                                            {
                                                $relationship = $relationships->result();
                                                
                                                foreach($relationship as $res)
                                                {
                                                    $db_relationship_id = $res->relationship_id;
                                                    $relationship_name = $res->relationship_name;
                                                    
                                                    if($db_relationship_id == $relationship_id)
                                                    {
                                                        echo '<option value="'.$db_relationship_id.'" selected>'.$relationship_name.'</option>';
                                                    }
                                                    
                                                    else
                                                    {
                                                        echo '<option value="'.$db_relationship_id.'">'.$relationship_name.'</option>';
                                                    }
                                                }
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label">Dependants Contibutor (NHIF/AON etc): </label>
                                
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="contibutor_nhif" placeholder="" value="<?php echo $contibutor_nhif;?>">
                                </div>
                            </div>
                             <div class="form-group">
                                <label class="col-md-4 control-label">Dependants Contibutor Contact: </label>
                                
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="contibutor_contact" placeholder="" value="<?php echo $contibutor_contact;?>">
                                </div>
                            </div>
                         <!--    <div class="form-group">
                                <label class="col-md-4 control-label">Insurance : </label>
                                
                                <div class="col-md-8">
                                    <select class="form-control" name="insurance_company_id">
                                         <option value="0">Select an insurance Company</option>
                                        <?php
                                            if($insurance->num_rows() > 0)
                                            {
                                                $insurance = $insurance->result();
                                                
                                                foreach($insurance as $res)
                                                {
                                                    $insurance_company_id1 = $res->insurance_company_id;
                                                    $insurance_company_name = $res->insurance_company_name;
                                                    
                                                    if($insurance_company_id1 == $insurance_company_id)
                                                    {
                                                        echo '<option value="'.$insurance_company_id1.'" selected>'.$insurance_company_name.'</option>';
                                                    }
                                                    
                                                    else
                                                    {
                                                        echo '<option value="'.$insurance_company_id1.'">'.$insurance_company_name.'</option>';
                                                    }
                                                }
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div> -->
                            
                        </div>
                    </div>
                <br/>
                <div class="center-align">
                    <button class="btn btn-info btn-sm" type="submit">Edit Patient</button>
                </div>
    <?php echo form_close();?>
              </div>
            </div>
            <!-- Widget ends -->

          </div>
        </div>
</section>
<script type="text/javascript">
    function calculate_age()
    {
        var dateString =  $('#date_of_birth').val();

        var dateString = new Date(dateString);
        var month = dateString.getMonth() + 1;
       

       if(month < 10)
       {
            month  = "0"+month;
       }

       var day = dateString.getDate();
       

       if(day < 10)
       {
            day  = "0"+day;
       }

        var dateString = day + '/' + month + '/' +  dateString.getFullYear();
        var now = new Date();
          var today = new Date(now.getYear(),now.getMonth(),now.getDate());

          var yearNow = now.getYear();
          var monthNow = now.getMonth();
          var dateNow = now.getDate();

          var dob = new Date(dateString.substring(6,10),
                             dateString.substring(0,2)-1,                   
                             dateString.substring(3,5)                  
                             );

          var yearDob = dob.getYear();
          var monthDob = dob.getMonth();
          var dateDob = dob.getDate();
          var age = {};
          var ageString = "";
          var yearString = "";
          var monthString = "";
          var dayString = "";


          yearAge = yearNow - yearDob;

          if (monthNow >= monthDob)
            var monthAge = monthNow - monthDob;
          else {
            yearAge--;
            var monthAge = 12 + monthNow -monthDob;
          }

          if (dateNow >= dateDob)
            var dateAge = dateNow - dateDob;
          else {
            monthAge--;
            var dateAge = 31 + dateNow - dateDob;

            if (monthAge < 0) {
              monthAge = 11;
              yearAge--;
            }
          }

          age = {
              years: yearAge,
              months: monthAge,
              days: dateAge
              };

          if ( age.years > 1 ) yearString = " years";
          else yearString = " year";
          if ( age.months> 1 ) monthString = " months";
          else monthString = " month";
          if ( age.days > 1 ) dayString = " days";
          else dayString = " day";



          if ( (age.years > 0) && (age.months > 0) && (age.days > 0) )
            ageString = age.years + yearString + ", " + age.months + monthString + ", and " + age.days + dayString + " old.";
          else if ( (age.years == 0) && (age.months == 0) && (age.days > 0) )
            ageString = "Only " + age.days + dayString + " old!";
          else if ( (age.years > 0) && (age.months == 0) && (age.days == 0) )
            ageString = age.years + yearString + " old. Happy Birthday!!";
          else if ( (age.years > 0) && (age.months > 0) && (age.days == 0) )
            ageString = age.years + yearString + " and " + age.months + monthString + " old.";
          else if ( (age.years == 0) && (age.months > 0) && (age.days > 0) )
            ageString = age.months + monthString + " and " + age.days + dayString + " old.";
          else if ( (age.years > 0) && (age.months == 0) && (age.days > 0) )
            ageString = age.years + yearString + " and " + age.days + dayString + " old.";
          else if ( (age.years == 0) && (age.months > 0) && (age.days == 0) )
            ageString = age.months + monthString + " old.";
          else ageString = "Oops! Could not calculate age!";

        document.getElementById('age_value').value = ageString;
    }
</script>