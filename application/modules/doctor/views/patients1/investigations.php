<?php

 $data['visit_id'] = $visit_id;
 $data['lab_test'] = 100;
 ?>
<div id="test_results">
  <div class="row">
  <div class="col-md-12">
        <section class="panel panel-featured panel-featured-info">
            <header class="panel-heading">
                <h2 class="panel-title">Lab Tests</h2>
            </header>
            <div class="panel-body">
                <div class="col-lg-8 col-md-8 col-sm-8">
                  <div class="form-group">
                    <select id='lab_test_id' name='lab_test_id' class='form-control custom-select ' >
                      <option value=''>None - Please Select a Lab test</option>
                      <?php echo $lab_tests;?>
                    </select>
                  </div>
                
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4">
                  <div class="form-group">
                      <button type='submit' class="btn btn-sm btn-success"  onclick="parse_lab_test(<?php echo $visit_id;?>);"> Add Lab Test</button>
                  </div>
                </div>
                 <!-- visit Procedures from java script -->
                
                <!-- end of visit procedures -->
            </div>
            <div id="lab_table"></div>
            <?php echo $this->load->view("laboratory/tests/visit_tests", $data, TRUE); ?>
         </section>
    </div>
</div>

</div>
<div id="xray_results">
  <div class="row">
  <div class="col-md-12">
        <section class="panel panel-featured panel-featured-info">
            <header class="panel-heading">
                <h2 class="panel-title">XRAY</h2>
            </header>
            <div class="panel-body">
                <div class="col-lg-8 col-md-8 col-sm-8">
                  <div class="form-group">
                    <select id='xray_id' name='xray_id' class='form-control custom-select ' >
                      <option value=''>None - Please Select an XRAY</option>
                      <?php echo $xrays;?>
                    </select>
                  </div>
                
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4">
                  <div class="form-group">
                      <button type='submit' class="btn btn-sm btn-success"  onclick="parse_xray(<?php echo $visit_id;?>);"> Add an XRAY</button>
                  </div>
                </div>
                
            </div>
            <div id="xray_table"></div>
            <?php echo $this->load->view("radiology/tests/test2", $data, TRUE); ?>
         </section>
    </div>
</div>
	
</div>
<!--<div id="ultrasound_results">
  <div class="row">
  <div class="col-md-12">
        <section class="panel panel-featured panel-featured-info">
            <header class="panel-heading">
                <h2 class="panel-title">ULTRASOUND</h2>
            </header>
            <div class="panel-body">
                <div class="col-lg-8 col-md-8 col-sm-8">
                  <div class="form-group">
                    <select id='ultrasound_id' name='ultrasound_id' class='form-control custom-select ' >
                      <option value=''>None - Please Select an Ulra sound</option>
                      <?php echo $ultrasound;?>
                    </select>
                  </div>
                
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4">
                  <div class="form-group">
                      <button type='submit' class="btn btn-sm btn-success"  onclick="parse_ultrasound(<?php echo $visit_id;?>);"> Add an Ultrasound</button>
                  </div>
                </div>
                 <! visit Procedures from java script 
                
                <!end of visit procedures 
            </div>
             <div id="ultrasound_table"></div>
             <?php //echo $this->load->view("radiology/tests_ultrasound/test2", $data, TRUE); ?>
         </section>
    </div>
</div>
	
</div>-->




<script type="text/javascript">
   $(function() {
       $("#lab_test_id").customselect();
       // $("#diseases_id").customselect();
       $("#xray_id").customselect();
       // $("#ultrasound_id").customselect();
   });

    $(document).ready(function(){
      get_lab_table(<?php echo $visit_id;?>);
      get_xray_table(<?php echo $visit_id;?>);
   });

      function parse_lab_test(visit_id)
   {
     var lab_test_id = document.getElementById("lab_test_id").value;
      lab(lab_test_id, visit_id);
     
   }
    function get_lab_table(visit_id){
         var XMLHttpRequestObject = false;
             
         if (window.XMLHttpRequest) {
         
             XMLHttpRequestObject = new XMLHttpRequest();
         } 
             
         else if (window.ActiveXObject) {
             XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
         }
         var url = "<?php echo site_url();?>laboratory/test_lab/"+visit_id;
     
         if(XMLHttpRequestObject) {
                     
             XMLHttpRequestObject.open("GET", url);
                     
             XMLHttpRequestObject.onreadystatechange = function(){
                 
                 if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
                     
                     document.getElementById("lab_table").innerHTML = XMLHttpRequestObject.responseText;
                 }
             }
             
             XMLHttpRequestObject.send(null);
         }
     }
   
    function lab(id, visit_id){
     
     var XMLHttpRequestObject = false;
         
     if (window.XMLHttpRequest) {
     
         XMLHttpRequestObject = new XMLHttpRequest();
     } 
         
     else if (window.ActiveXObject) {
         XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
     }
     var url = "<?php echo site_url();?>laboratory/test_lab/"+visit_id+"/"+id;
     // window.alert(url);
     if(XMLHttpRequestObject) {
                 
         XMLHttpRequestObject.open("GET", url);
                 
         XMLHttpRequestObject.onreadystatechange = function(){
             
             if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
                 
                document.getElementById("lab_table").innerHTML = XMLHttpRequestObject.responseText;
                //get_lab_table(visit_id);
             }
         }
         
         XMLHttpRequestObject.send(null);
     }
   }
   function parse_xray(visit_id)
   {
   var xray_id = document.getElementById("xray_id").value;
   xray(xray_id, visit_id);
   
   }
   
   function xray(id, visit_id){
     
     var XMLHttpRequestObject = false;
         
     if (window.XMLHttpRequest) {
     
         XMLHttpRequestObject = new XMLHttpRequest();
     } 
         
     else if (window.ActiveXObject) {
         XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
     }
     var url = "<?php echo site_url();?>radiology/xray/test_xray/"+visit_id+"/"+id;
     // window.alert(url);
     if(XMLHttpRequestObject) {
                 
         XMLHttpRequestObject.open("GET", url);
                 
         XMLHttpRequestObject.onreadystatechange = function(){
             
             if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
                 
                document.getElementById("xray_table").innerHTML = XMLHttpRequestObject.responseText;
                //get_xray_table(visit_id);
             }
         }
         
         XMLHttpRequestObject.send(null);
     }
   }
   
   function get_xray_table(visit_id){
     var XMLHttpRequestObject = false;
         
     if (window.XMLHttpRequest) {
     
         XMLHttpRequestObject = new XMLHttpRequest();
     } 
         
     else if (window.ActiveXObject) {
         XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
     }
     var url = "<?php echo site_url();?>radiology/xray/test_xray/"+visit_id;
     
     if(XMLHttpRequestObject) {
                 
         XMLHttpRequestObject.open("GET", url);
                 
         XMLHttpRequestObject.onreadystatechange = function(){
             
             if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
                 
                 document.getElementById("xray_table").innerHTML = XMLHttpRequestObject.responseText;
             }
         }
         
         XMLHttpRequestObject.send(null);
     }
   }
</script>