
<div class="row">
  <div class="col-md-12">
    <section class="panel panel-featured panel-featured-info">
      <header class="panel-heading">
        <h2 class="panel-title">Diagnosis</h2>
      </header>

      <div class="panel-body">
                <div class="col-lg-8 col-md-8 col-sm-8">
                  <div class="form-group">
                    <select id='diseases_id' name='diseases_id' class='form-control custom-select ' >
                      <option value=''>None - Please Select a diagnosis</option>
                      <?php echo $diseases;?>
                    </select>
                  </div>
                
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4">
                  <div class="form-group">
                      <button type='submit' class="btn btn-sm btn-success"  onclick="pass_diagnosis(<?php echo $visit_id;?>);"> Identify the disease</button>
                  </div>
                </div>
      </div>
      <!-- visit Procedures from java script -->
     
      <!-- end of visit procedures -->
      <div id="patient_diagnosis"></div>

            
    </section>
    </div>
</div>
<div class="row">
  <div class="col-md-12">
        <section class="panel panel-featured panel-featured-info">
            <header class="panel-heading">
                <h2 class="panel-title">Prescription</h2>
            </header>
            <div class="panel-body">
                <div class="col-lg-8 col-md-8 col-sm-8">
                  <div class="form-group">
                    <select id='drug_id' name='drug_id' class='form-control custom-select ' onchange="get_drug_to_prescribe(<?php echo $visit_id;?>);">
                      <option value=''>None - Please Select an drug</option>
                      <?php echo $drugs;?>
                    </select>
                  </div>
                
                </div>
                
                <!-- end of visit procedures -->
            </div>
             <div id="prescription_view"></div>
             <div id="visit_prescription"></div>

             <?php // echo $this->load->view("pharmacy/display_prescription", $data, TRUE); ?>
              
         </section>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
          <section class="panel panel-featured panel-featured-info">
            <header class="panel-heading">
                <h2 class="panel-title">Plan</h2>
            </header>
            <div class="panel-body">
              <div class="padd">
                <div id="visit_plan_detail"></div>

                <table align='center' class='table table-striped table-hover table-condensed'>
                  <tr>
                    <th>Plan Name</th>
                    <th>Description</th>
                    <th></th>
                  </tr>
                  <tr>
                    <td valign='top'>
                      <select id='plan_id' class='form-control'>
                        <?php
                        $plan_rs = $this->nurse_model->get_plan_details();
                        $num_plans = count($plan_rs);
                        if($num_plans > 0){
                          foreach ($plan_rs as $key_item):
                        
                          $plan_name = $key_item->plan_name;
                          $plan_id = $key_item->plan_id;
                          echo "<option value='".$plan_id."'>".$plan_name."</option>";
                          endforeach;
                        }
                        ?>
                    </select>
                    </td>
                        <td><textarea id='plan_description_value' class='cleditor' ></textarea></td>
                        <td><input type='button' class='btn btn-sm btn-success' value='Save' onclick='save_visit_plan_detail(<?php echo $visit_id?>)' /></td>
                    </tr>
                </table>
                <!-- end of vitals data -->
              </div>
            </div>
          </section>
    </div>
</div>

<script type="text/javascript">
   $(function() {
       $("#drug_id").customselect();
       $("#diseases_id").customselect();
   });

   $(document).ready(function(){
      get_disease(<?php echo $visit_id?>);
      display_prescription(<?php echo $visit_id?>,0);
      get_visit_plan(<?php echo $visit_id;?>);      
      display_inpatient_prescription(<?php echo $visit_id;?>,0);
   });

   function save_plan(visit_id)
   {
    var plan = tinymce.get('visit_plan').getContent();
    var visit_id = '<?php echo $visit_id;?>';
    var config_url = $('#config_url').val();
    var notes_type_id = 6;
    var data_url = config_url+"nurse/save_notes/"+visit_id+'/'+notes_type_id;
    
    var notes_date = '<?php echo date('Y-m-d');?>';
    var notes_time = '<?php echo date('H:i');?>';
    $.ajax({
      type:'POST',
      url: data_url,
      data:{notes: plan, date: notes_date, time: notes_time, notes_type_id: notes_type_id},
      dataType: 'json',
      success:function(data){
        if(data.result == 'success')
        {
          $('#plan_section').html(data.message);
          tinymce.get('visit_plan').setContent('');
          $('#add_plan').modal('hide');
          //initiate WYSIWYG editor
          tinymce.init({
            selector: ".cleditor",
            height : "300"
          });
          alert("You have successfully added the objective findings");
        }
        else
        {
          alert("Unable to add the objective findings");
        }
      //obj.innerHTML = XMLHttpRequestObject.responseText;
      },
      error: function(xhr, status, error) {
        //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
        alert(error);
      }
   
    });
   }
   
   function get_disease(visit_id){
     
     var XMLHttpRequestObject = false;
       
     if (window.XMLHttpRequest) {
     
       XMLHttpRequestObject = new XMLHttpRequest();
     } 
       
     else if (window.ActiveXObject) {
       XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
     }
     var config_url = $('#config_url').val();
     var url = config_url+"nurse/get_diagnosis/"+visit_id;
   
         
     if(XMLHttpRequestObject) {
       var obj = document.getElementById("visit_diagnosis_original");
       //var obj2 = document.getElementById("visit_diagnosis");
           
       XMLHttpRequestObject.open("GET", url);
           
       XMLHttpRequestObject.onreadystatechange = function(){
         
         if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
           obj.innerHTML = XMLHttpRequestObject.responseText;
           //obj2.innerHTML = XMLHttpRequestObject.responseText;
         }
       }
       
       XMLHttpRequestObject.send(null);
     }
   }
   
   $(document).on("click","a.delete_diagnosis",function() 
   {
       var diagnosis_id = $(this).attr('href');
       var visit_id = $(this).attr('id');
    var config_url = $('#config_url').val();
    var url = config_url+"nurse/delete_diagnosis/"+diagnosis_id;
    
    $.get(url, function( data ) {
      get_disease(visit_id);
    });
    
    return false;
   });

   function display_prescription(visit_id, page){
     
     var XMLHttpRequestObject = false;
       
     if (window.XMLHttpRequest) {
     
       XMLHttpRequestObject = new XMLHttpRequest();
     } 
       
     else if (window.ActiveXObject) {
       XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
     }
      var config_url = $('#config_url').val();
     var url = config_url+"pharmacy/display_prescription/"+visit_id;
     
     if(page == 1){
       var obj = window.opener.document.getElementById("prescription");
     }
     
     else{
       var obj = document.getElementById("visit_prescription");
     }
     if(XMLHttpRequestObject) {
           
       XMLHttpRequestObject.open("GET", url);
           
       XMLHttpRequestObject.onreadystatechange = function(){
         
         if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
           obj.innerHTML = XMLHttpRequestObject.responseText;
           
           if(page == 1){
             window.close(this);
           
           }
           //plan(visit_id);
         }
       }
       
       XMLHttpRequestObject.send(null);
     }
   }

   function get_visit_plan(visit_id){
    
      var XMLHttpRequestObject = false;
          
      if (window.XMLHttpRequest) {
      
          XMLHttpRequestObject = new XMLHttpRequest();
      } 
          
      else if (window.ActiveXObject) {
          XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
      }
      var config_url = document.getElementById("config_url").value;
      var url = config_url+"nurse/load_plan_details/"+visit_id;
      // alert(url);
      if(XMLHttpRequestObject) {
          
          var obj = document.getElementById("visit_plan_detail");
                  
          XMLHttpRequestObject.open("GET", url);
                  
          XMLHttpRequestObject.onreadystatechange = function(){
              
              if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

                  obj.innerHTML = XMLHttpRequestObject.responseText;
              }
          }
                  
          XMLHttpRequestObject.send(null);
      }
  }

  function delete_plan_detail(id, visit_id){
    //alert(id);
    var XMLHttpRequestObject = false;
        
    if (window.XMLHttpRequest) {
    
        XMLHttpRequestObject = new XMLHttpRequest();
    } 
        
    else if (window.ActiveXObject) {
        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
    }
      var config_url = document.getElementById("config_url").value;
    var url = config_url+"nurse/delete_plan_detail/"+id;
    
    if(XMLHttpRequestObject) {
                
        XMLHttpRequestObject.open("GET", url);
                
        XMLHttpRequestObject.onreadystatechange = function(){
            
            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

                get_surgeries(visit_id);
            }
        }
                
        XMLHttpRequestObject.send(null);
    }
}

function save_visit_plan_detail(visit_id){
   // var description =  document.getElementById("plan_description_value").value;
    var description = tinymce.get('plan_description_value').getContent();
   var plan_id = document.getElementById("plan_id").value;
   var config_url = document.getElementById("config_url").value;
  var url = config_url+"nurse/plan_description/"+plan_id+"/"+visit_id;   
  
   $.ajax({
   type:'POST',
   url: url,
   data:{description: description, plan_id: plan_id, visit_id: visit_id},
   dataType: 'text',
   success:function(data){
        tinymce.get('plan_description_value').setContent('');
          //tinyMCE.activeEditor.setContent('');
          //initiate WYSIWYG editor
          tinymce.init({
            selector: ".cleditor",
            height : "200"
          });   
   },
   error: function(xhr, status, error) {
   alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
   
   }
   });
   get_visit_plan(visit_id);
   return false;

}

function display_inpatient_prescription(visit_id,module){
   
     var XMLHttpRequestObject = false;
         
     if (window.XMLHttpRequest) {
     
         XMLHttpRequestObject = new XMLHttpRequest();
     } 
         
     else if (window.ActiveXObject) {
         XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
     }
     
     var config_url = document.getElementById("config_url").value;
     var url = config_url+"pharmacy/display_inpatient_prescription/"+visit_id+"/"+module;
     
     if(XMLHttpRequestObject) {
                 
         XMLHttpRequestObject.open("GET", url);
                 
         XMLHttpRequestObject.onreadystatechange = function(){
             
             if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
   
                 document.getElementById("visit_prescription").innerHTML=XMLHttpRequestObject.responseText;
             }
         }
                 
         XMLHttpRequestObject.send(null);
     }
   }
   function get_drug_to_prescribe(visit_id)
   {
   var XMLHttpRequestObject = false;
         
     if (window.XMLHttpRequest) {
     
         XMLHttpRequestObject = new XMLHttpRequest();
     } 
         
     else if (window.ActiveXObject) {
         XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
     }
     var drug_id = document.getElementById("drug_id").value;
   
     var url = "<?php echo site_url();?>pharmacy/inpatient_prescription/"+visit_id+"/"+drug_id+"/2";
   
      if(XMLHttpRequestObject) {
                 
         XMLHttpRequestObject.open("GET", url);
                 
         XMLHttpRequestObject.onreadystatechange = function(){
             
             if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
               var prescription_view = document.getElementById("prescription_view");
              
               document.getElementById("prescription_view").innerHTML=XMLHttpRequestObject.responseText;
                prescription_view.style.display = 'block';
             }
         }
                 
         XMLHttpRequestObject.send(null);
     }
   
   
   }
   
   function pass_diagnosis(visit_id)
   {
   var diseases_id = document.getElementById("diseases_id").value;
   save_disease(diseases_id, visit_id);
   
   }
   
   function save_disease(val, visit_id){
   
   var XMLHttpRequestObject = false;
     
   if (window.XMLHttpRequest) {
   
     XMLHttpRequestObject = new XMLHttpRequest();
   } 
     
   else if (window.ActiveXObject) {
     XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
   } 
   var config_url = $('#config_url').val();
   var url = config_url+"nurse/save_diagnosis/"+val+"/"+visit_id;
   if(XMLHttpRequestObject) {
         
     XMLHttpRequestObject.open("GET", url);
         
     XMLHttpRequestObject.onreadystatechange = function(){
       
       if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
         get_disease(visit_id);
       }
     }
     
     XMLHttpRequestObject.send(null);
   }
   }


   function pass_prescription()
   {

   var quantity = document.getElementById("quantity_value").value;
   var x = document.getElementById("x_value").value;
   var dose_value = document.getElementById("dose_value").value;
   var duration = 1;//document.getElementById("duration_value").value;
   var consumption = document.getElementById("consumption_value").value;
   var number_of_days = document.getElementById("number_of_days_value").value;
   var service_charge_id = document.getElementById("drug_id").value;
   var visit_id = document.getElementById("visit_id").value;
   var input_total_units = document.getElementById("input-total-value").value;
   var module = document.getElementById("module").value;
   var passed_value = document.getElementById("passed_value").value;
   var type_of_drug = document.getElementById("type_of_drug").value;
   
   var url = "<?php echo base_url();?>pharmacy/prescribe_prescription";
   
  
   $.ajax({
   type:'POST',
   url: url,
   data:{quantity: quantity, x: x, duration: duration,consumption: consumption, service_charge_id : service_charge_id, visit_id : visit_id, number_of_days: number_of_days,module: module,passed_value:passed_value,input_total_units:input_total_units,dose_value: dose_value,type_of_drug: type_of_drug},
   dataType: 'text',
   success:function(data){
   
   var prescription_view = document.getElementById("prescription_view");
   prescription_view.style.display = 'none';
   display_inpatient_prescription(visit_id,0);
   
   },
   error: function(xhr, status, error) {
   alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
   
   }
   });
   
   return false;
   }


  function check_frequency_type()
   {
   
     var x = document.getElementById("x_value").value;
     var type_of_drug = document.getElementById("type_of_drug").value;
       
     var number_of_days = document.getElementById("number_of_days_value").value;
     var quantity = document.getElementById("quantity_value").value;
     var dose_value = document.getElementById("dose_value").value;

    var service_charge_id = document.getElementById("drug_id").value;
    var visit_id = document.getElementById("visit_id").value;
    var consumption = document.getElementById("consumption_value").value;

     if(x == "" || x == 0)
     {

       // alert("Please select the frequency of the medicine");
       x = 1;
     }
     
     if(number_of_days == "" || number_of_days == 0)
     {
        number_of_days = 1;
     }
     
         
      var url = "<?php echo base_url();?>pharmacy/get_values";
       $.ajax({
       type:'POST',
       url: url,
       data:{quantity: quantity, x: x, service_charge_id : service_charge_id, visit_id : visit_id, number_of_days: number_of_days,dose_value:dose_value,consumption: consumption,type_of_drug: type_of_drug},
       dataType: 'text',
       success:function(data){
          var data = jQuery.parseJSON(data);

          var amount = data.amount;
          var frequency = data.frequency;
           var item = data.item;

          // {"message":"success","amount":"35","frequency":"5"}

          if(type_of_drug == 3)
          {
           
            var total_units = number_of_days * frequency * quantity;
            var total_amount =  number_of_days * frequency * amount * quantity;
          }
          else
          {

             var total_units =   quantity;
             var total_amount =   amount * quantity;
          }
        

        // document.getElementById("total_units").innerHTML = "<h2>"+ amount +" units</h2>";
         $( "#total_units" ).html("<h2>"+ total_units +" units</h2>");
         $( "#total_amount" ).html("<h3>Ksh. "+ total_amount +"</h3>");
         $( "#item_description" ).html("<p> "+ item +"</p>");

         document.getElementById("input-total-value").value = total_units;
         // document.getElementById("total_amount").innerHTML = "<h3>Ksh. "+ frequency +" units</h3>";


       
       },
       error: function(xhr, status, error) {
       alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
       
       }
       });
   
        
     
  }

  function button_update_prescription(visit_id,visit_charge_id,prescription_id,module)
   {
   var quantity = $('#quantity'+prescription_id).val();
   var x = $('#x'+prescription_id).val();
   var duration = $('#duration'+prescription_id).val();
   var consumption = $('#consumption'+prescription_id).val();
   var url = "<?php echo base_url();?>pharmacy/update_inpatient_prescription/"+visit_id+"/"+visit_charge_id+"/"+prescription_id+"/"+module;
   
   
   $.ajax({
   type:'POST',
   url: url,
   data:{quantity: quantity, x: x, duration: duration,consumption: consumption},
   dataType: 'text',
   success:function(data){
   
   
   },
   error: function(xhr, status, error) {
   alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
   
   }
   });
   display_inpatient_prescription(visit_id,0);
   return false;
   }

   function delete_prescription(prescription_id, visit_id,visit_charge_id,module)
   {
   var res = confirm('Are you sure you want to delete this prescription ?');
   
   if(res)
   {
     var XMLHttpRequestObject = false;
     
     if (window.XMLHttpRequest) {
       XMLHttpRequestObject = new XMLHttpRequest();
     } 
     
     else if (window.ActiveXObject) {
       XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
     }
     var url = config_url+"pharmacy/delete_inpatient_prescription/"+prescription_id+"/"+visit_id+"/"+visit_charge_id+"/"+module;
     
     if(XMLHttpRequestObject) {
       
       XMLHttpRequestObject.open("GET", url);
       
       XMLHttpRequestObject.onreadystatechange = function(){
         
         if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
           
            display_inpatient_prescription(visit_id,0);
          
         }
       }
       XMLHttpRequestObject.send(null);
     }
   }
   }
</script>