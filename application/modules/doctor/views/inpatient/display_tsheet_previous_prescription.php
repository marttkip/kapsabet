<?php 

// get previous days prescriptions

$date_today = date('Y-m-d');

$rs3 = $this->pharmacy_model->select_previous_tsheet_days($visit_id,$date_today);
// var_dump($rs3); die();
$table_data2 = '';
if($rs3->num_rows() > 0)
{
	foreach ($rs3->result() as $key => $value) {
		# code...
		$visit_date = $value->date_created;
		$rs3 = $this->pharmacy_model->select_tsheet_drugs($visit_id,$visit_date);
		// var_dump($date_today); die();
		$num_rows3 =count($rs3);
		$s=0;
		$table_data2 .= '

						<table class="table table-bordered ">
						  <thead>
							<tr>
							  <th>#</th>
							  <th>Date</th>
							  <th>Drug Name</th>
							  <th>Route</th>
							  <th>Dose</th>
							  <th>Frequency</th>
							  <th>Duration</th>
							  <th>Prescribed By</th>
							  <th>6am</th>
							  <th>12pm/2pm</th>
							  <th>6pm</th>
							  <th>10pm/12am</th>
							</tr>
						  </thead>
						  <tbody>';
		if($num_rows3 > 0){
			foreach ($rs3 as $key_rs3):
					$frequncy = $key_rs3->drug_times_name;
		            $frequency_id = $key_rs3->drug_times_id;
		            $prescription_id = $key_rs3->prescription_id;
		            $date1 = $key_rs3->prescription_startdate;
		            $date2 = $key_rs3->prescription_finishdate;
		            $sub = $key_rs3->prescription_substitution;
		            $duration = $key_rs3->drug_duration_name;
		            $duration_id = $key_rs3->drug_duration_id;
		            $sub = $key_rs3->prescription_substitution;
		            $date_created = $key_rs3->date_created;
		            $consumption = $key_rs3->drug_consumption_name;
		            $consumption_id = $key_rs3->drug_consumption_id;
		            $quantity = $key_rs3->prescription_quantity;
		            $medicine = $key_rs3->product_name;
		            $product_deleted = $key_rs3->product_deleted;
		            $charge = $service_charge_amount = $key_rs3->product_charge;
		            $product_unitprice = $key_rs3->product_unitprice;
		            $product_id = $key_rs3->actual_product_id;
		            $visit_charge_id = $key_rs3->visit_charge_id;
		            $number_of_days = $key_rs3->number_of_days;
		            $units_given = $key_rs3->units_given;
		            $charged = $key_rs3->charged;
		            $prescription_id = $key_rs3->prescription_id;
		            $date = $key_rs3->date;
		            $time = $key_rs3->time;
		            $sum_units = $key_rs3->visit_charge_units;
		            $charge = $key_rs3->visit_charge_amount;
		            $visit_charge_comment = $key_rs3->visit_charge_comment;
		            $morning = $key_rs3->morning;
		            $midday = $key_rs3->midday;
		            $evening = $key_rs3->evening;
					$night = $key_rs3->night;
			        $completed_status = $key_rs3->completed_status;

			            if($morning == 1)
			            {
			            	$morning_view = 'checked';
			            	$morning_value = 0;
			            }
			            else
			            {
			            	$morning_view = '';
			            	$morning_value = 1;
			            }

			            if($midday == 1)
			            {
			            	$midday_view = 'checked';
			            	$midday_value = 0;
			            }
			            else
			            {
			            	$midday_view = '';
			            	$midday_value = 1;
			            }

			            if($evening == 1)
			            {
			            	$evening_view = 'checked';
			            	$evening_value = 0;
			            }
			            else
			            {
			            	$evening_view = '';
			            	$evening_value = 1;
			            }

			             if($night == 1)
			            {
			                  $night_view = 'checked';
			                  $night_value = 0;
			            }
			            else
			            {
			                  $night_view = '';
			                  $night_value = 1;
			            }
		            $s++;
		            $table_data2 .= '<tr>
							  			<td>'.$s.'</td>
							  			<td>'.$date_created.'</td>
							  			<td>'.$medicine.'</td>
							  			<td>'.$consumption.'</td>
							  			<td>'.$quantity.'</td>
							  			<td>'.$frequncy.'</td>
							  			<td>'.$duration.'</td>
							  			<td>1</td>  			
										<td><input type="checkbox"  class="form-control" style="height: 15px !important;margin: 0px !important;"  value="'.$morning_value.'" '.$morning_view.' readonly></td>
										<td><input type="checkbox"  class="form-control" style="height: 15px !important;margin: 0px !important;" value="'.$midday_value.'" '.$midday_view.' readonly></td>
										<td><input type="checkbox"  class="form-control" style="height: 15px !important;margin: 0px !important;"  value="'.$evening_value.'" '.$evening_view.' readonly></td>
										<td><input type="checkbox"  class="form-control" style="height: 15px !important;margin: 0px !important;"  value="'.$night_value.'" '.$night_view.' readonly></td>	
							  		</tr>';



			endforeach;

		}

		$table_data2 .='</tbody>
		</table>';
	}
}





?>

<div class="row">
	<div class="col-md-12">	
	 	<section class="panel panel-featured panel-featured-info">
            <header class="panel-heading">
                <h2 class="panel-title">Previous Tsheet records</h2>
            </header>
            <div class="panel-body">	
				<?php echo $table_data2?>	
			</div>
		</section>	  
	</div>
</div>