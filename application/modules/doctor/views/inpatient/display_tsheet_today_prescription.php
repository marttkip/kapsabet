<?php

// get all prescribed drugs 
$date_today = date('Y-m-d');
$rs = $this->pharmacy_model->select_prescription_tsheet($visit_id,$date_today);
// var_dump($rs); die();
$num_rows =count($rs);
$s=0;
$consumables = '';
if($num_rows > 0){
	foreach ($rs as $key_rs):
			$frequncy = $key_rs->drug_times_name;
            $frequency_id = $key_rs->drug_times_id;
            $prescription_id = $key_rs->prescription_id;
            $date1 = $key_rs->prescription_startdate;
            $date2 = $key_rs->prescription_finishdate;
            $sub = $key_rs->prescription_substitution;
            $duration = $key_rs->drug_duration_name;
            $duration_id = $key_rs->drug_duration_id;
            $sub = $key_rs->prescription_substitution;
            $consumption = $key_rs->drug_consumption_name;
            $consumption_id = $key_rs->drug_consumption_id;
            $quantity = $key_rs->prescription_quantity;
            $medicine = $key_rs->product_name;
            $product_deleted = $key_rs->product_deleted;
            $charge = $service_charge_amount = $key_rs->product_charge;
		$consumables .="<option value='".$prescription_id."'>".$medicine."</option>";
	endforeach;

}


// endforeach;





?>
<div class="row">
	<div class="col-md-12">
		<div class="col-lg-3">
		</div>
		<div class="col-lg-6 col-md-6 col-sm-6">
		      <div class="form-group">
		        <select id='prescription_items' name='prescription_items' class='form-control custom-select ' >
		          <option value=''>None - Please Select a prescribed drug</option>
		          <?php echo $consumables;?>
		        </select>
		      </div>
		    
		    </div>
		    <div class="col-lg-3 col-md-3 col-sm-3">
		      <div class="form-group">
		          <button type='submit' class="btn btn-sm btn-success"  onclick="pass_days_tsheet(<?php echo $visit_id;?>);"> Add to prescription</button>
		      </div>
	     </div>
		
	</div>
</div>
<br>
<?php
$date_today = date('Y-m-d');

$rs2 = $this->pharmacy_model->select_tsheet_drugs($visit_id,$date_today);
// var_dump($date_today); die();
$num_rows2 =count($rs2);
$s=0;
$table_data = '';
if($num_rows2 > 0){
	foreach ($rs2 as $key_rs2):
			$frequncy = $key_rs2->drug_times_name;
            $frequency_id = $key_rs2->drug_times_id;
            $prescription_id = $key_rs2->prescription_id;
            $date1 = $key_rs2->prescription_startdate;
            $date2 = $key_rs2->prescription_finishdate;
            $sub = $key_rs2->prescription_substitution;
            $duration = $key_rs2->drug_duration_name;
            $duration_id = $key_rs2->drug_duration_id;
            $sub = $key_rs2->prescription_substitution;
            $date_created = $key_rs2->date_created;
            $consumption = $key_rs2->drug_consumption_name;
            $consumption_id = $key_rs2->drug_consumption_id;
            $quantity = $key_rs2->prescription_quantity;
            $medicine = $key_rs2->product_name;
            $product_deleted = $key_rs2->product_deleted;
            $charge = $service_charge_amount = $key_rs2->product_charge;
            $product_unitprice = $key_rs2->product_unitprice;
            $product_id = $key_rs2->actual_product_id;
            $visit_charge_id = $key_rs2->visit_charge_id;
            $number_of_days = $key_rs2->number_of_days;
            $units_given = $key_rs2->units_given;
            $charged = $key_rs2->charged;
            $prescription_id = $key_rs2->prescription_id;
            $date = $key_rs2->date;
            $time = $key_rs2->time;
            $sum_units = $key_rs2->visit_charge_units;
            $charge = $key_rs2->visit_charge_amount;
            $visit_charge_comment = $key_rs2->visit_charge_comment;
            $morning = $key_rs2->morning;
            $midday = $key_rs2->midday;
            $evening = $key_rs2->evening;
            $night = $key_rs2->night;
            $completed_status = $key_rs2->completed_status;

            if($morning == 1)
            {
            	$morning_view = 'checked';
            	$morning_value = 0;
            }
            else
            {
            	$morning_view = '';
            	$morning_value = 1;
            }

            if($midday == 1)
            {
            	$midday_view = 'checked';
            	$midday_value = 0;
            }
            else
            {
            	$midday_view = '';
            	$midday_value = 1;
            }

            if($evening == 1)
            {
            	$evening_view = 'checked';
            	$evening_value = 0;
            }
            else
            {
            	$evening_view = '';
            	$evening_value = 1;
            }

             if($night == 1)
            {
                  $night_view = 'checked';
                  $night_value = 0;
            }
            else
            {
                  $night_view = '';
                  $night_value = 1;
            }

            if($completed_status == 1)
            {
            	$highlight = 'danger';
            }
            else
            {
            	$highlight = 'defualt';
            }
            $s++;
            $table_data .= '<tr>
					  			<td>'.$s.'</td>
					  			<td class="'.$highlight.'">'.$date_created.'</td>
					  			<td class="'.$highlight.'">'.$medicine.'</td>
					  			<td >'.$consumption.'</td>
					  			<td>'.$quantity.'</td>
					  			<td>'.$frequncy.'</td>
					  			<td>'.$duration.'</td>
					  			<td>1</td>  			
								<td><input type="checkbox" id="morning'.$prescription_id.'" class="form-control" style="height:15px !important;margin: 0px !important;"  value="'.$morning_value.'" '.$morning_view.' onclick="update_morning_value('.$prescription_id.','.$visit_id.')"></td>
								<td><input type="checkbox" id="midday'.$prescription_id.'" class="form-control" style="height:15px !important;margin: 0px !important;" value="'.$midday_value.'" '.$midday_view.' onclick="update_midday_value('.$prescription_id.','.$visit_id.')"></td>
								<td><input type="checkbox" id="evening'.$prescription_id.'" class="form-control" style="height:15px !important;margin: 0px !important;"  value="'.$evening_value.'" '.$evening_view.' onclick="update_evening_value('.$prescription_id.','.$visit_id.')"></td>
                                                <td><input type="checkbox" id="night'.$prescription_id.'" class="form-control" style="height:15px !important;margin: 0px !important;"  value="'.$night_value.'" '.$night_view.' onclick="update_night_value('.$prescription_id.','.$visit_id.')"></td>
								<td> <a class="btn btn-danger btn-xs"  onclick="delete_tsheet_prescription('.$prescription_id.','.$visit_id.')" onclick="return confirm(\'Do you want to mark '.$medicine.' as dose completed ?\');"><i class="fa fa-trash"></i></a></td>	
					  		</tr>';



	endforeach;

}
?>
<div class="row">
	<div class="col-md-12">
		<table class="table table-bordered ">
		  <thead>
			<tr>
			  <th>#</th>
			  <th>Date</th>
			  <th>Drug Name</th>
			  <th>Route</th>
			  <th>Dose</th>
			  <th>Frequency</th>
			  <th>Duration</th>
			  <th>Prescribed By</th>
			   <th>6am</th>
                    <th>12pm/2pm</th>
                    <th>6pm</th>
                    <th>10pm/12am</th>
			  <th>Action</th>
			</tr>
		  </thead>
		  <tbody>
		  		<?php echo $table_data?>
		  </tbody>
		</table>
	</div>
</div>



