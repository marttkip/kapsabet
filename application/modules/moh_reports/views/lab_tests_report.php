<?php echo $this->load->view('search/lab_tests_search', '', TRUE);?>
<?php
$search_title = $this->session->userdata('laboratory_title_search');
if(!empty($search_title))
{
	$title_ext = $search_title;
}
else
{
	$title_ext = 'Tests Report for '.date('Y-m-d');
}

?>
<div class="row">
    <div class="col-md-12">

        <section class="panel panel-featured panel-featured-info">
            <header class="panel-heading">
            	 <h2 class="panel-title"><?php echo $title;?></h2>
            	 <a href="<?php echo site_url();?>moh-reports/print-lab-tests-report" target="_blank" class="btn btn-sm btn-warning pull-right" style="margin-top:-25px;"> <i class="fa fa-print"></i> Print List</a>
            	  <a href="<?php echo site_url();?>moh-reports/export-lab-tests-report" target="_blank" class="btn btn-sm btn-success pull-right" style="margin-top:-25px;margin-right: 5px;"> <i class="fa fa-print"></i> Export List</a>
            </header>

          <!-- Widget content -->
                <div class="panel-body">
          <h5 class="center-align"><?php echo $title_ext;?></h5>
          <br>
<?php
		$result = '';
		$search = $this->session->userdata('laboratory_test_report_search');
		if(!empty($search))
		{
			echo '<a href="'.site_url().'close-lab-tests-search" class="btn btn-sm btn-warning">Close Search</a>';
		}

		//if users exist display them
		if ($query->num_rows() > 0)
		{
			$count = $page;

			$result .=
				'
					<table class="table table-hover table-bordered table-striped table-responsive col-md-12">
					  <thead>
						<tr>
						  <th>#</th>
						  <th>Visit Date</th>
						  <th>Patient</th>
						  <th>Patient Number</th>
						  <th>Patient Age</th>
						  <th>Patient Gender </th>
						  <th>Procedure Name</th>
              <th>Result</th>
						  </tr>
					  </thead>
					  <tbody>

				';



			$personnel_query = $this->personnel_model->get_all_personnel();

			foreach ($query->result() as $row)
			{
				$total_invoiced = 0;
				$visit_date = date('jS M Y',strtotime($row->visit_date));
				$visit_time = date('H:i a',strtotime($row->visit_time));
				if($row->visit_time_out != '0000-00-00 00:00:00')
				{
					$visit_time_out = date('H:i a',strtotime($row->visit_time_out));
				}
				else
				{
					$visit_time_out = '-';
				}

				$visit_id = $row->visit_id;
				$patient_id = $row->patient_id;
				$visit_date_days = $row->visit_date;
				$patient_number = $row->patient_number;
				$strath_no = $row->strath_no;
				$personnel_id = $row->personnel_id;
				$dependant_id = $row->dependant_id;
				$strath_no = $row->strath_no;
				$visit_type_id = $row->visit_type_id;
				$gender_id = $row->gender_id;
				$patient_othernames = $row->patient_othernames;
				$patient_surname = $row->patient_surname;
				$patient_date_of_birth = $row->patient_date_of_birth;
				$last_visit = $row->last_visit;
        $service_charge_name = $row->service_charge_name;
				$service_charge_name = $row->service_charge_name;
        $lab_positive_status = $row->lab_positive_status;
        $lab_test_id = $row->lab_test_id;
				$names = $patient_surname.' '.$patient_othernames;


				if($gender_id == 1)
				{
					$gender = 'Male';
				}
				else
				{
					$gender = 'Female';
				}

				// this is to check for any credit note or debit notes
				$age = $this->moh_reports_model->calculate_age($patient_date_of_birth);
				// end of the debit and credit notes


				//creators and editors
				if($personnel_query->num_rows() > 0)
				{
					$personnel_result = $personnel_query->result();

					foreach($personnel_result as $adm)
					{
						$personnel_id2 = $adm->personnel_id;

						if($personnel_id == $personnel_id2)
						{
							$doctor = $adm->personnel_onames.' '.$adm->personnel_fname;
							break;
						}

						else
						{
							$doctor = '-';
						}
					}
				}

				else
				{
					$doctor = '-';
				}
        if($lab_positive_status)
				{
					$result_status = $this->moh_reports_model->get_tests_result_done($lab_test_id,NULL,$visit_id);
          if($result_status == 1)
          {
            $result_status = 'NEGATIVE';
          }
          else if($result_status == 2)
          {
            $result_status = 'POSITIVE';
          }
          else {
            $result_status = 'NOT CAPTURED';
          }
				}
				else
				{

					$result_status = '-';
				}
				$count++;


					$result .=
						'
							<tr>
								<td>'.$count.'</td>
								<td>'.$visit_date.'</td>
								<td>'.$names.'</td>
								<td>'.$patient_number.'</td>
								<td>'.$age.'</td>
								<td>'.$gender.'</td>
                <td>'.$service_charge_name.'</td>
                <td>'.$result_status.'</td>

						';
			}

			$result .=
			'
						  </tbody>
						</table>
			';
		}

		else
		{
			$result .= "There are no visit procedures done";
		}

		echo $result;
?>
          </div>

          <div class="widget-foot">

				<?php if(isset($links)){echo $links;}?>

                <div class="clearfix"></div>

            </div>

		</section>
    </div>
  </div>
