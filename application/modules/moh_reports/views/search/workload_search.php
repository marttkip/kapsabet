       
        <section class="panel panel-featured panel-featured-info">
            <header class="panel-heading">
            	<h2 class="panel-title">Search</h2>
            </header>             

          <!-- Widget content -->
                <div class="panel-body">
			<?php
            echo form_open("search-workload", array("class" => "form-horizontal"));
            ?>
            <div class="row">
            	
                <div class="col-md-6">
                    
                    <div class="form-group">
                        <label class="col-lg-4 control-label">Year: </label>
                        
                        <div class="col-lg-8">
                        	<div class="input-group">
                        		<select class="form-control" name="year">
                        			<option value="">---Select Year---</option>
                        			<option value="2016">2016</option>
                        			<option value="2017">2017</option>
                                    <option value="2018">2018</option>
                                    <option value="2019">2019</option>
                        		</select>
                                
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    
                    <div class="form-group">
                        <label class="col-lg-4 control-label"> Month: </label>
                        
                        <div class="col-lg-8">
                        	<div class="input-group">
                                <select class="form-control" name="month">
                                	<option value="">---Select Month---</option>
                        			<option value="01">Jan</option>
                        			<option value="02">Feb</option>
                        			<option value="03">Mar</option>
                        			<option value="04">Apr</option>
                        			<option value="05">May</option>
                        			<option value="06">Jun</option>
                        			<option value="07">Jul</option>
                        			<option value="08">Aug</option>
                        			<option value="09">Sept</option>
                        			<option value="10">Oct</option>
                        			<option value="11">Nov</option>
                        			<option value="12">Dec</option>
                        		</select>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
            <br>
            <div class="row">
            	<div class="col-md-12">
            		<div class="form-group">
                        <div class="center-align">
                            <button type="submit" class="btn btn-info">Search Report</button>
                        </div>
                </div>
            		
            	</div>
            	
            </div>
            
            
            <?php
            echo form_close();
            ?>
          </div>
		</section>