<section class="panel panel-featured panel-featured-info">
    <header class="panel-heading">
      <h2 class="panel-title pull-right">Search Procedures</h2>
      <h2 class="panel-title">Search</h2>
    </header>

  <!-- Widget content -->
        <div class="panel-body">
<?php
    echo form_open("search-lab-tests-report", array("class" => "form-horizontal"));
    ?>
      <div class="row">
          <div class="col-md-4">
              <div class="form-group">
                  <label class="col-md-4 control-label">Name: </label>

                  <div class="col-md-8">
                      <input type="text" class="form-control" name="surname" placeholder="Name">
                  </div>
              </div>
              <div class="form-group">
                  <label class="col-md-4 control-label">Test Name: </label>

                  <div class="col-md-8">
                      <input type="text" class="form-control" name="lab_test_name" placeholder="Test">
                  </div>
              </div>

          </div>
          <div class="col-md-4">
            <div class="form-group">
                <label class="col-md-4 control-label">Age Range: </label>

                <div class="col-md-8">
                    <select class="form-control" name="age_group" >
                      <option value="">----- select an option -----</option>
                      <option value="1">Over 5 Years</option>
                      <option value="2">Under 5 Years</option>
                    </select>
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-4 control-label">Gender : </label>

                <div class="col-md-8">
                    <select class="form-control" name="gender_id" >
                      <option value="">----- select an option -----</option>
                      <option value="1">Male</option>
                      <option value="2">Female</option>
                    </select>
                </div>
            </div>
          </div>
          <div class="col-md-4">

            <div class="form-group">
                  <label class="col-lg-4 control-label">Visit Date From: </label>

                  <div class="col-lg-8">
                    <div class="input-group">
                          <span class="input-group-addon">
                              <i class="fa fa-calendar"></i>
                          </span>
                          <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="visit_date_from" placeholder="Visit Date From">
                      </div>
                  </div>
              </div>

              <div class="form-group">
                  <label class="col-lg-4 control-label">Visit Date To: </label>

                  <div class="col-lg-8">
                    <div class="input-group">
                          <span class="input-group-addon">
                              <i class="fa fa-calendar"></i>
                          </span>
                          <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="visit_date_to" placeholder="Visit Date To">
                      </div>
                  </div>
              </div>

          </div>
        </div>
        <br>
        <div class="row">
            <div class="form-group">
                <div class="col-md-12">
                  <div class="center-align">
                      <button type="submit" class="btn btn-info">Search</button>
            </div>
                </div>
            </div>
        </div>


    <?php
    echo form_close();
    ?>
  </div>
</section>
