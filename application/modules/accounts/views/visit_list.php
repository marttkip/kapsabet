<table class="table  table-bordered col-md-12">			
	<tbody>
		<?php
		// var_dump()

		// $count_visit = $visit_list->num_rows();
		$num_pages = $total_rows/$per_page;

		if($num_pages < 1)
		{
			$num_pages = 0;
		}
		$num_pages = round($num_pages);

		if($page==0)
		{
			$counted = 0;
		}
		else if($page > 0)
		{
			$counted = $per_page*$page;
		}

		if($visit_list->num_rows() > 0)
		{
			echo "<thead>
						<tr>
							<th>Date</th>
							<th>Number</th>
							<th>Type</th>
							<th>Bal</th>
						</tr>
				</thead>";
			foreach ($visit_list->result() as $key => $value) {
				# code...
				$visit_idd = $value->visit_id;
				$inpatient_visit = $value->inpatient;
				$visit_date_date = $value->visit_date;
				$invoice_number = $value->invoice_number;
				$close_card = $value->close_card;

				$payments_value = $this->accounts_model->total_payments($visit_idd);

				$invoice_total = $this->accounts_model->total_invoice($visit_idd);

				$balance = $this->accounts_model->balance($payments_value,$invoice_total);
				if($balance < 0)
				{
					$balance = 0;
				}
				else
				{
					$balance = $balance;
				}
				if($inpatient_visit == 1)
				{
					$visit_type = 'A';
				}
				else
				{
					$visit_type = 'C';
				}

				if($close_card == 0)
				{
					$color = 'default';
				}
				else if($close_card == 1)
				{
					$color = 'success';

				}
				else if($close_card == 2)
				{
					$color = 'danger';
				}
				$counted++;
				echo "<tr onclick='get_visit_detail(".$visit_idd.")' class='".$color."'>
						<td>".$visit_date_date."</td><td>".$invoice_number."</td><td>".$visit_type."</td><td>".number_format($balance,2)."</td>
					</tr>";
			}
		}
		?>
	</tbody>
</table>
<div class="row">
	<div class="col-md-12" style="padding-right: 25px;">
		<div class="pull-right">
			<?php
				$link ='<ul style="list-style:none;">';
				// echo $page;
				if($num_pages > $page)
				{
					// echo "now ".$num_pages." ".$page;
					$last_page = $num_pages -1;

					if($page > 0 AND $page < $last_page)
					{
						// echo $page;
						$page++;
						// echo "now".$page;
						$previous = $page -2;
						$link .='<li onclick="get_next_page('.$previous.','.$patient_id.')" class="pull-left" style="margin-right:20px;" > <i class="fa fa-angle-left"></i> Back</li>  <li onclick="get_next_page('.$page.','.$patient_id.')" class="pull-right"> Next <i class="fa fa-angle-right"></i> </li>';
					}else if($page == $last_page)
					{
						$page++;

						$previous = $page -2;
						// echo "equal".$num_pages." ".$page;
						$link .='<li onclick="get_next_page('.$previous.','.$patient_id.')" class="pull-left"> <i class="fa fa-angle-left"></i> Back</li>';
					}
					else
					{
						$page++;
						$link .='<li onclick="get_next_page('.$page.','.$patient_id.')" class="pull-right"> Next <i class="fa fa-angle-right"></i> </li>';
					}
					// var_dump($link); die();
				}
				$link .='</ul>';
				echo $link;
				
			?>
		</div>
	</div>
</div>
