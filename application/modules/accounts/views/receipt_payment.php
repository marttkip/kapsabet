
 <section class="panel ">
	<header class="panel-heading">
		<div class="panel-title">
		<strong>Name:</strong> <?php echo $patient_surname.' '.$patient_othernames;?>. <strong> Patient Visit: </strong><?php echo $visit_type_name;?>. 
		

		</div>
		<div class="pull-right">
			<?php 
			if($close_page == 0)
			{
				?>
				<a href="<?php echo site_url();?>queues/outpatient-queue" class="btn btn-info btn-sm pull-right " style="margin-top:-25px"><i class="fa fa-arrow-left"></i> Back to Outpatients</a>
				<?php
			}
			else
			{
				?>
				<a href="<?php echo site_url();?>queues/walkins" class="btn btn-info btn-sm pull-right " style="margin-top:-25px"><i class="fa fa-arrow-left"></i> Back to Walkins</a>
				<?php
			}
			?>
			 
		</div>
	</header>
	
	<!-- Widget content -->
	
	<div class="panel-body">
		<div class="row">
			<div class="col-md-12">
			<?php
				$error = $this->session->userdata('error_message');
				$success = $this->session->userdata('success_message');
				
				if(!empty($error))
				{
				  echo '<div class="alert alert-danger">'.$error.'</div>';
				  $this->session->unset_userdata('error_message');
				}
				
				if(!empty($success))
				{
				  echo '<div class="alert alert-success">'.$success.'</div>';
				  $this->session->unset_userdata('success_message');
				}
			 ?>
			</div>
		</div>
		
		
        <!--<div class="row">
        	<div class="col-sm-3 col-sm-offset-3">
            	<a href="<?php echo site_url().'doctor/print_prescription'.$visit_id;?>" class="btn btn-warning">Print prescription</a>
            </div>
            
        	<div class="col-sm-3">
            	<a href="<?php echo site_url().'doctor/print_lab_tests'.$visit_id;?>" class="btn btn-danger">Print lab tests</a>
            </div>
        </div>-->

        
      
        
		<div class="row">
			<div class="col-md-12">
				<div class="col-md-7">
					<section class="panel panel-featured panel-featured-info">
						<header class="panel-heading">
							
							<h2 class="panel-title">Invoices Charges</h2>
							 <a href="<?php echo site_url();?>accounts/print_invoice_new/<?php echo $visit_id?>" target="_blank" class="btn btn-sm btn-warning pull-right" style="margin-top:-25px;"> <i class="fa fa-print"></i> Print Invoice</a>
						</header>
						<!--  <div class="row">
						 	<br>
                            	<?php //echo form_open("accounts/bill_patient/".$visit_id, array("class" => "form-horizontal"));?>
					            	<div class="col-md-10 ">
					                    <div class="col-md-12" style="margin-bottom: 10px">
						                  <div class="form-group">
						                  <label class="col-md-2 control-label">Service: </label>
						                  	<div class="col-md-10">
							                    <select id='service_id_item' name='service_charge_id' class='form-control custom-select ' >
							                      <option value=''>None - Please Select a service</option>
							                       <?php echo $services_list;?>
							                    </select>

							                    <input type="hidden" name="visit_id_checked" id="visit_id_checked">
						                    </div>
						                  </div>
						                </div>
						                <br>
						                <input type="hidden" name="provider_id" value="0">
						               
						                <input data-format="yyyy-MM-dd" type="hidden" data-plugin-datepicker class="form-control" name="visit_date_date" id="visit_date_date" placeholder="Admission Date" value="<?php //echo date('Y-m-d');?>">
						            </div>
						            <div class="col-md-10" >
						            	<div class="center-align">
											<button type="submit" class='btn btn-info btn-sm' type='submit' >Add to Bill</button>
										</div>
						            </div>
						         <?php// echo form_close();?>
						    </div> -->
						<div class="panel-body">
                            <?php

								echo "
								<table align='center' class='table table-striped table-hover table-condensed'>
									<tr>
										<th>#</th>
										<th >Services/Items</th>
										<th>Units</th>
										<th >Unit Cost (Ksh)</th>
										<th > Total</th>
									</tr>		
								"; 

								$total= 0; 
								$count = 0;
								$total_bill =0;
								
								// var_dump($invoice_items->num_rows()); die();
								$sub_total= 0; 
								$personnel_query = $this->personnel_model->retrieve_personnel();
									
								if($charge_sheet_query->num_rows() >0){
									
									$visit_date_day = '';
									foreach ($charge_sheet_query->result() as $value => $key1):
										$v_procedure_id = $key1->visit_charge_id;
										$procedure_id = $key1->service_charge_id;
										$service_name = $key1->service_name;
										$date = $key1->date;
										$time = $key1->time;
										$visit_charge_timestamp = $key1->visit_charge_timestamp;
										$visit_charge_amount = $key1->visit_charge_amount;
										$units = $key1->visit_charge_units;
										$procedure_name = $key1->service_charge_name;
										$service_id = $key1->service_id;
										$provider_id = $key1->provider_id;
									
										$sub_total= $sub_total +($units * $visit_charge_amount);
										$visit_date = date('l d F Y',strtotime($date));
										$visit_time = date('H:i A',strtotime($visit_charge_timestamp));
										
										if($visit_date_day != $visit_date)
										{
											
											$visit_date_day = $visit_date;
										}
										else
										{
											$visit_date_day = '';
										}

										// echo 'asdadsa'.$visit_date_day;

										if($personnel_query->num_rows() > 0)
										{
											$personnel_result = $personnel_query->result();
											
											foreach($personnel_result as $adm)
											{
												$personnel_id = $adm->personnel_id;
												

												if($personnel_id == $provider_id)
												{
													$provider_id = '[ Dr. '.$adm->personnel_fname.' '.$adm->personnel_lname.']';


													$procedure_name = $procedure_name.$provider_id;
												}
												
												
												
											}
										}
										
										else
										{
											$provider_id = '';
										}





										$counted++;
										echo"
												<tr> 
													<td>".$counted."</td>
													<td >".$procedure_name."</td>
													<td >".$units."</td>
													<td >".number_format($visit_charge_amount,2)."</td>
													<td >".number_format($visit_charge_amount*$units,2)."</td>
												
												</tr>	
										";

										$visit_date_day = $visit_date;
										$total_bill += $visit_charge_amount*$units;
										endforeach;
										

								}	
								echo"
												<tr> 
													<td colspan='4'>Total Bill</td>
													<td >".floor($total_bill)."</td>
												</tr>	
										";	
					echo"
					 </table>
					";
					?>
						
						</div>
					</section>
				</div>
				
				<div class="col-md-5">
							<section class="panel panel-featured panel-featured-info">
								<header class="panel-heading">
									<h2 class="panel-title">Receipts</h2>
								</header>
								
								<div class="panel-body">
                                		<?php echo form_open("accounts/make_payment_charge/".$visit_id.'/'.$close_page, array("class" => "form-horizontal"));?>
										<div class="form-group">
											<div class="col-lg-6">
                                            	<div class="radio">
                                                    <label>
                                                        <input id="optionsRadios2" type="radio" name="type_payment" value="1" checked="checked" onclick="getservices(1)"> 
                                                        Normal
                                                    </label>
                                                </div>
											</div>
											<div class="col-lg-6">
                                            	<div class="radio">
                                                    <label>
                                                        <input id="optionsRadios2" type="radio" name="type_payment" value="2" onclick="getservices(2)"> 
                                                        Waiver
                                                    </label>
                                                </div>
											</div>
											<!-- <div class="col-lg-4">
                                            	<div class="radio">
                                                    <label>
                                                        <input id="optionsRadios2" type="radio" name="type_payment" value="3" onclick="getservices(3)"> 
                                                        Credit Note
                                                    </label>
                                                </div>
											</div> -->
										</div>
                                        <input type="hidden" name="service_id" value="0">
										<div id="service_div2" class="form-group" style="display:none;">
											<!-- <label class="col-lg-4 control-label">Service: </label>
										  
											<div class="col-lg-8">
												
                                            	<select name="service_id" class="form-control" >
                                                	<option value="">All services</option>
                                            	<?php
												if(count($item_invoiced_rs) > 0)
												{
													$s=0;
													foreach ($item_invoiced_rs as $key_items):
														$s++;
														$service_id = $key_items->service_id;
														$service_name = $key_items->service_name;
														?>
                                                        <option value="<?php echo $service_id;?>"><?php echo $service_name;?></option>
														<?php
													endforeach;
												}
													
												//display DN & CN services
												if(count($payments_rs) > 0)
												{
													foreach ($payments_rs as $key_items):
														$payment_type = $key_items->payment_type;
														
														if(($payment_type == 2) || ($payment_type == 3))
														{
															$payment_service_id = $key_items->payment_service_id;
															
															if($payment_service_id > 0)
															{
																$service_associate = $this->accounts_model->get_service_detail($payment_service_id);
																?>
																<option value="<?php echo $payment_service_id;?>"><?php echo $service_associate;?></option>
																<?php
															}
														}
														
													endforeach;
												}
												?>
                                                </select>
											</div> -->
										</div>
                                        
                                    	<div id="service_div" class="form-group" style="display:none;">
                                            <!-- <label class="col-lg-4 control-label"> Services: </label>
                                            
                                            <div class="col-lg-8">
                                                <select class="form-control" name="payment_service_id" >
                                                	<option value="">--Select a service--</option>
													<?php
                                                    $service_rs = $this->accounts_model->get_all_service();
                                                    $service_num_rows = count($service_rs);
                                                    if($service_num_rows > 0)
                                                    {
														foreach($service_rs as $service_res)
														{
															$service_id = $service_res->service_id;
															$service_name = $service_res->service_name;
															

																echo '<option value="'.$service_id.'">'.$service_name.'</option>';
															
															
														}
                                                    }
                                                    ?>
                                                </select>
                                            </div> -->
                                        </div>
                                         <input type="hidden" class="form-control" name="redirect_url" placeholder="" autocomplete="off" value="<?php echo $this->uri->uri_string()?>">
										<div class="form-group">
											<label class="col-lg-2 control-label">Amount: </label>
										  
											<div class="col-lg-10">
												<input type="text" class="form-control" name="amount_paid" placeholder="" autocomplete="off">
											</div>
										</div>
										
										<div class="form-group" id="payment_method">
											<label class="col-lg-2 control-label">Payment Method: </label>
											  
											<div class="col-lg-10">
												<select class="form-control" name="payment_method" onchange="check_payment_type(this.value)">
                                                	<?php
													  $method_rs = $this->accounts_model->get_payment_methods();
													  $num_rows = count($method_rs);
													 if($num_rows > 0)
													  {
														
														foreach($method_rs as $res)
														{
														  $payment_method_id = $res->payment_method_id;
														  $payment_method = $res->payment_method;
														  
															echo '<option value="'.$payment_method_id.'">'.$payment_method.'</option>';
														  
														}
													  }
												  ?>
												</select>
											  </div>
										</div>
										<div id="mpesa_div" class="form-group" style="display:none;" >
											<label class="col-lg-2 control-label"> Mpesa TX Code: </label>

											<div class="col-lg-8">
												<input type="text" class="form-control" name="mpesa_code" placeholder="">
											</div>
										</div>
									  
										<div id="insuarance_div" class="form-group" style="display:none;" >
											<label class="col-lg-2 control-label"> Insurance Number: </label>
											<div class="col-lg-10">
												<input type="text" class="form-control" name="insuarance_number" placeholder="">
											</div>
										</div>
									  
										<div id="cheque_div" class="form-group" style="display:none;" >
											<label class="col-lg-2 control-label"> Cheque Number: </label>
										  
											<div class="col-lg-10">
												<input type="text" class="form-control" name="cheque_number" placeholder="">
											</div>
										</div>
									  
										<div id="username_div" class="form-group" style="display:none;" >
											<label class="col-lg-2 control-label"> Username: </label>
										  
											<div class="col-lg-10">
												<input type="text" class="form-control" name="username" placeholder="">
											</div>
										</div>
									  
										<div id="password_div" class="form-group" style="display:none;" >
											<div class="form-group">
												<label class="col-lg-2 control-label"> Password: </label>										  
												<div class="col-lg-10">
													<input type="password" class="form-control" name="password" placeholder="">
												</div>
											</div>
											<div class="form-group">
							                    <label class="col-md-2 control-label">Description: </label>
							                    
							                    <div class="col-md-10">
							                        <textarea class="form-control" name="waiver_description"></textarea>
							                    </div>
							                </div>

										</div>
										

										<div class="center-align">
											<button class="btn btn-info btn-sm" type="submit">Add Payment Information</button>
										</div>
										<?php echo form_close();?>
										<br>
									<table class="table table-hover table-bordered col-md-12">
										<thead>
											<tr>
												<th>#</th>
												<th>Time</th>
												<th>Method</th>
												<th>Amount</th>
												<th colspan="2"></th>
											</tr>
										</thead>
										<tbody>
											<?php
											$payments_rs = $this->accounts_model->payments($visit_id);
											$total_payments = 0;
											$total_amount = ($total + $debit_note_amount) - $credit_note_amount;
											$total_waiver = 0;
											if(count($payments_rs) > 0)
											{
												$x=0;

												foreach ($payments_rs as $key_items):
													$x++;
													$payment_method = $key_items->payment_method;

													$time = $key_items->time;
													$payment_type = $key_items->payment_type;
													$payment_id = $key_items->payment_id;
													$payment_status = $key_items->payment_status;
													$payment_service_id = $key_items->payment_service_id;
													$service_name = '';

													if($payment_type == 2 && $payment_status == 1)
													{
														$waiver_amount = $key_items->amount_paid;
														$total_waiver += $waiver_amount;
													}
													
													if($payment_type == 1 && $payment_status == 1)
													{
														$amount_paid = $key_items->amount_paid;
														$amount_paidd = number_format($amount_paid,2);
														
														if(count($item_invoiced_rs) > 0)
														{
															foreach ($item_invoiced_rs as $key_items):
															
																$service_id = $key_items->service_id;
																
																if($service_id == $payment_service_id)
																{
																	$service_name = $key_items->service_name;
																	break;
																}
															endforeach;
														}
													
														//display DN & CN services
														if((count($payments_rs) > 0) && ($service_name == ''))
														{
															foreach ($payments_rs as $key_items):
																$payment_type = $key_items->payment_type;
																
																if(($payment_type == 2) || ($payment_type == 3))
																{
																	$payment_service_id2 = $key_items->payment_service_id;
																	
																	if($payment_service_id2 == $payment_service_id)
																	{
																		$service_name = $this->accounts_model->get_service_detail($payment_service_id);
																		break;
																	}
																}
																
															endforeach;
														}
														?>
														<tr>
															<td><?php echo $x;?></td>
															<td><?php echo $time;?></td>
															<td><?php echo $payment_method;?></td>
															<td><?php echo $amount_paidd;?></td>
															<td><a href="<?php echo site_url().'accounts/print_single_receipt/'.$payment_id;?>" class="btn btn-small btn-warning" target="_blank"><i class="fa fa-print"></i></a></td>
															

																<td>
	                                                            	<button type="button" class="btn btn-small btn-default" data-toggle="modal" data-target="#refund_payment<?php echo $payment_id;?>"><i class="fa fa-times"></i></button>
																	<!-- Modal -->
																	<div class="modal fade" id="refund_payment<?php echo $payment_id;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
																	    <div class="modal-dialog" role="document">
																	        <div class="modal-content">
																	            <div class="modal-header">
																	            	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
																	            	<h4 class="modal-title" id="myModalLabel">Cancel payment</h4>
																	            </div>
																	            <div class="modal-body">
																	            	<?php echo form_open("accounts/cancel_payment/".$payment_id.'/'.$visit_id, array("class" => "form-horizontal"));?>
																	                <div class="form-group">
																	                    <label class="col-md-4 control-label">Action: </label>
																	                    
																	                    <div class="col-md-8">
																	                        <select class="form-control" name="cancel_action_id">
																	                        	<option value="">-- Select action --</option>
																	                            <?php
																	                                if($cancel_actions->num_rows() > 0)
																	                                {
																	                                    foreach($cancel_actions->result() as $res)
																	                                    {
																	                                        $cancel_action_id = $res->cancel_action_id;
																	                                        $cancel_action_name = $res->cancel_action_name;
																	                                        
																	                                        echo '<option value="'.$cancel_action_id.'">'.$cancel_action_name.'</option>';
																	                                    }
																	                                }
																	                            ?>
																	                        </select>
																	                    </div>
																	                </div>
																	                <input type="hidden" class="form-control" name="redirect_url" placeholder="" autocomplete="off" value="<?php echo $this->uri->uri_string()?>">
																	                
																	                <div class="form-group">
																	                    <label class="col-md-4 control-label">Description: </label>
																	                    
																	                    <div class="col-md-8">
																	                        <textarea class="form-control" name="cancel_description"></textarea>
																	                    </div>
																	                </div>
																	                
																	                <div class="row">
																	                	<div class="col-md-8 col-md-offset-4">
																	                    	<div class="center-align">
																	                        	<button type="submit" class="btn btn-primary">Save action</button>
																	                        </div>
																	                    </div>
																	                </div>
																	                <?php echo form_close();?>
																	            </div>
																	            <div class="modal-footer">
																	                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
																	            </div>
																	        </div>
																	    </div>
																	</div>

	                                                            </td>
	                                                      
														</tr>
														<?php
														$total_payments =  $total_payments + $amount_paid;
													}
												endforeach;

												?>
												<tr>
													<td colspan="3"><strong>Total : </strong></td>
													<td><strong> <?php echo number_format($total_payments,2);?></strong></td>
												</tr>
												<?php
											}
											
											else
											{
												?>
												<tr>
													<td colspan="4"> No payments made yet</td>
												</tr>
												<?php
											}
											?>
										</tbody>
									</table>
								</div>
							</section>
						</div>
				<!-- END OF THE SPAN 7 -->
			</div>
		</div>
		<div class="row">
	    	<div class="col-md-12 center-align">
	    		<h4><strong> WAIVER. <?php echo number_format($total_waiver,2)?></strong></h4>
	    		 <h2><strong>BAL. <?php echo number_format(($total_bill- $total_payments)-$total_waiver,2)?></strong></h2>
	        </div>
	    </div>
		<div class="row">
	    	<div class="col-md-12 center-align">
	    		<?php
	    		$coming_from = $this->reception_model->coming_from_id($visit_id);
	    		// var_dump($coming_from); die();
	    		?>
	    		 <a href="<?php echo site_url();?>accounts/send_back_to_department/<?php echo $visit_id?>/<?php echo $coming_from?>" class="btn btn-warning btn-sm  " onclick="return confirm('Do you want to send back to department for editing ?')" ><i class="fa fa-arrow-left"></i> Send  to Department</a>
	    		 <?php
	    		 if($close_page == 5)
	    		 {


	    		 ?>
	    		 <a href="<?php echo site_url();?>accounts/end_visit/<?php echo $visit_id?>" class="btn btn-danger btn-sm  " onclick="return confirm('Do you want to end this visit ?')" ><i class="fa fa-folder-closed"></i> End Visit</a>
	    		 <?php
	    			}
	    		 ?>
	        </div>
	    </div>

	
	</div>
</section>
  <!-- END OF ROW -->
<script type="text/javascript">

 
   $(function() {
       $("#service_id_item").customselect();
       $("#provider_id_item").customselect();
       $("#parent_service_id").customselect();

   });
   $(document).ready(function(){
   		// display_patient_bill(<?php echo $visit_id;?>);
   });
     
  function getservices(id){

        var myTarget1 = document.getElementById("service_div");
        var myTarget2 = document.getElementById("username_div");
        var myTarget3 = document.getElementById("password_div");
        var myTarget4 = document.getElementById("service_div2");
        var myTarget5 = document.getElementById("payment_method");
		
        if(id == 1)
        {
          myTarget1.style.display = 'none';
          myTarget2.style.display = 'none';
          myTarget3.style.display = 'none';
          myTarget4.style.display = 'block';
          myTarget5.style.display = 'block';
        }
        else
        {
          myTarget1.style.display = 'block';
          myTarget2.style.display = 'block';
          myTarget3.style.display = 'block';
          myTarget4.style.display = 'none';
          myTarget5.style.display = 'none';
        }
        
  }
  function check_payment_type(payment_type_id){
   
    var myTarget1 = document.getElementById("cheque_div");

    var myTarget2 = document.getElementById("mpesa_div");

    var myTarget3 = document.getElementById("insuarance_div");

    if(payment_type_id == 1)
    {
      // this is a check
     
      myTarget1.style.display = 'block';
      myTarget2.style.display = 'none';
      myTarget3.style.display = 'none';
    }
    else if(payment_type_id == 2)
    {
      myTarget1.style.display = 'none';
      myTarget2.style.display = 'none';
      myTarget3.style.display = 'none';
    }
    else if(payment_type_id == 3)
    {
      myTarget1.style.display = 'none';
      myTarget2.style.display = 'none';
      myTarget3.style.display = 'block';
    }
    else if(payment_type_id == 4)
    {
      myTarget1.style.display = 'none';
      myTarget2.style.display = 'none';
      myTarget3.style.display = 'none';
    }
    else if(payment_type_id == 5)
    {
      myTarget1.style.display = 'none';
      myTarget2.style.display = 'block';
      myTarget3.style.display = 'none';
    }
    else
    {
      myTarget2.style.display = 'none';
      myTarget3.style.display = 'block';  
    }

  }

   function display_patient_bill(visit_id){

      var XMLHttpRequestObject = false;
          
      if (window.XMLHttpRequest) {
      
          XMLHttpRequestObject = new XMLHttpRequest();
      } 
          
      else if (window.ActiveXObject) {
          XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
      }
      
      var config_url = document.getElementById("config_url").value;
      var url = config_url+"accounts/view_patient_bill/"+visit_id;
      // alert(url);
      if(XMLHttpRequestObject) {
                  
          XMLHttpRequestObject.open("GET", url);
                  
          XMLHttpRequestObject.onreadystatechange = function(){
              
              if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

                  document.getElementById("patient_bill").innerHTML=XMLHttpRequestObject.responseText;
              }
          }
                  
          XMLHttpRequestObject.send(null);
      }
  }

	//Calculate procedure total
	function calculatetotal(amount, id, procedure_id, v_id){
	       
	    var units = document.getElementById('units'+id).value;  
	    var billed_amount = document.getElementById('billed_amount'+id).value;  

	    grand_total(id, units, billed_amount, v_id);
	}
	function grand_total(procedure_id, units, amount, v_id){
	    var XMLHttpRequestObject = false;
	        
	    if (window.XMLHttpRequest) {
	    
	        XMLHttpRequestObject = new XMLHttpRequest();
	    } 
	        
	    else if (window.ActiveXObject) {
	        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
	    }
	    var config_url = document.getElementById("config_url").value;
	    var url = config_url+"accounts/update_service_total/"+procedure_id+"/"+units+"/"+amount;
	    // alert(url);
	    if(XMLHttpRequestObject) {
	                
	        XMLHttpRequestObject.open("GET", url);
	                
	        XMLHttpRequestObject.onreadystatechange = function(){
	            
	            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) 
				{
	    			display_patient_bill(v_id);
	            }
	        }
	                
	        XMLHttpRequestObject.send(null);
	    }
	}
	function delete_service(id, visit_id){
	    var XMLHttpRequestObject = false;
	        
	    if (window.XMLHttpRequest) {
	    
	        XMLHttpRequestObject = new XMLHttpRequest();
	    } 
	        
	    else if (window.ActiveXObject) {
	        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
	    }
	     var config_url = document.getElementById("config_url").value;
	    var url = config_url+"accounts/delete_service_billed/"+id;
	    
	    if(XMLHttpRequestObject) {
	                
	        XMLHttpRequestObject.open("GET", url);
	                
	        XMLHttpRequestObject.onreadystatechange = function(){
	            
	            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

	                display_patient_bill(visit_id);
	            }
	        }
	                
	        XMLHttpRequestObject.send(null);
	    }
	}
	function save_service_items(visit_id)
	{
		var provider_id = $('#provider_id'+visit_id).val();
		var service_id = $('#service_id'+visit_id).val();
		var visit_date = $('#visit_date_date'+visit_id).val();
		var url = "<?php echo base_url();?>accounts/add_patient_bill/"+visit_id;
		
		$.ajax({
		type:'POST',
		url: url,
		data:{provider_id: provider_id, service_charge_id: service_id, visit_date: visit_date},
		dataType: 'text',
		success:function(data){
			alert("You have successfully billed");
			display_patient_bill(visit_id);
		},
		error: function(xhr, status, error) {
		alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
			display_patient_bill(visit_id);
		}
		});
		return false;
	}

 
</script>
