   
 <section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">Search <?php echo $title;?></h2>
    </header>             

          <!-- Widget content -->
                <div class="panel-body">
          
			<?php echo form_open("hospital_administration/search_unallocated_invoices", array("class" => "form-horizontal"));?>
            <div class="row center-align">
                <div class="col-md-8">
                    <div class="form-group">
                        <label class="col-lg-4 control-label">Invoice Number: </label>
                        
                        <div class="col-lg-8">
                          	 <input type="text" class="form-control" name="invoice_number" placeholder="Invoice Number">
                        </div>
                    </div>
                </div>
                
                <div class="col-md-3">
                	<div class="center-align">
                        <button type="submit" class="btn btn-info"><i class="fa fa-search"></i> Search</button>
                    </div>
                </div>
            </div>
            
            
            <?php
            echo form_close();
            ?>
            </div>
        
		</section>