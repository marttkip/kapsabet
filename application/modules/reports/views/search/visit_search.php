        <?php
            $reach = '';
            $where = 'diseases.diseases_id > 0 AND diseases.diseases_name != "Suspected Malaria"';
            $table = 'diseases';
            $this->db->where($where);
            $diseases_list = $this->db->get($table);

            foreach ($diseases_list->result() as $row)
            {
                $diseases_name = $row->diseases_name;
                $diseases_id = $row->diseases_id;
                $diseases_code = $row->diseases_code;
                $reach .='<option value="'.$diseases_id.'">'.$diseases_name.' '.$diseases_code.' </option>';
            }
       ?>       
        <section class="panel panel-featured panel-featured-info">
            <header class="panel-heading">
            	<h2 class="panel-title pull-right">Visit Search:</h2>
            	<h2 class="panel-title">Search</h2>
            </header>             

          <!-- Widget content -->
                <div class="panel-body">
			<?php
            echo form_open("reports/search_visit_reports", array("class" => "form-horizontal"));
            ?>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-md-4 control-label">Range: </label>
                        
                        <div class="col-md-8">
                            <select class="form-control" name="mobidity_type">
                                <option value="">---Select Age---</option>
                                <option value="1"> > 5 Years</option>
                                <option value="0"> < 5 Years </option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label">Disease List: </label>
                        
                        <div class="col-md-8">
                            <select id='diseases_id' name='diseases_id' class='form-control custom-select ' >
                                <option value="">---Select type of disease---</option>
                                <?php echo $reach?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    
                    <div class="form-group">
                        <label class="col-lg-4 control-label">Visit Date From: </label>
                        
                        <div class="col-lg-8">
                        	<div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </span>
                                <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="visit_date_from" placeholder="Visit Date From">
                            </div>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-lg-4 control-label">Visit Date To: </label>
                        
                        <div class="col-lg-8">
                        	<div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </span>
                                <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="visit_date_to" placeholder="Visit Date To">
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
            <br>
            <div class="row">
            	<div class="col-md-12">
            		<div class="form-group">
                        <div class="center-align">
                            <button type="submit" class="btn btn-info">Search Report</button>
                        </div>
                </div>
            		
            	</div>
            	
            </div>
            
            
            <?php
            echo form_close();
            ?>
          </div>
		</section>


        <script text="javascript">
$(function() {
    $("#diseases_id").customselect();
});
</script>