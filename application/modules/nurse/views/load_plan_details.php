<?php
$patient_id = $this->nurse_model->get_patient_id($visit_id);

$surgery_rs = $this->nurse_model->get_visit_pan_detail($visit_id);
$num_surgeries = count($surgery_rs);



echo
"
			<table align='center' class='table table-striped table-hover table-condensed'>
		<tr>
			<th>Plan Name</th>
			<th>Description</th>
			<th></th>
		</tr>
		";
		if($num_surgeries > 0){
			foreach ($surgery_rs as $key):
		
			$plan_name = $key->plan_name;
			$description = $key->plan_description;
			$id = $key->patient_plan_id;
			echo
			"
					<tr>
						<td>".$plan_name."</td>
						<td>".$description."</td>
						<td>
						<a class='btn btn-danger'   onclick='delete_plan_detail(".$id.", ".$visit_id.")'><i class='fa fa-trash'></i></a>
						</td>
					</tr>
			";
			endforeach;
		}
echo
"
	
 </table>
";
?>