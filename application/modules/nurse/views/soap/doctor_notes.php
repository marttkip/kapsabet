<?php

$v_data['signature_location'] = base_url().'assets/signatures/';
$v_data['query'] = $this->nurse_model->get_notes(2, $visit_id);

if(!isset($mobile_personnel_id))
{
	$mobile_personnel_id = NULL;
}
$v_data['mobile_personnel_id'] = $mobile_personnel_id;

$notes = $this->load->view('nurse/patients/notes', $v_data, TRUE);

echo '<div id="doctor_notes_section">'.$notes.'</div>';

echo form_open('nurse/save_doctor_notes/'.$visit_id, array('class' => 'form-horizontal', 'id' => 'doctor_form', 'visit_id' => $visit_id));
$patient_id = $this->nurse_model->get_patient_id($visit_id);


$get_medical_rs = $this->nurse_model->get_doctor_notes($patient_id);
$num_rows = count($get_medical_rs);
//echo $num_rows;

if($num_rows > 0){
	foreach ($get_medical_rs as $key2) :
		$doctor_notes = $key2->doctor_notes;
	endforeach;

echo
'	<div class="row">
		<div class="col-sm-6" >
			<div class="form-group">
				<label class="control-label">Date</label>
				<div id="datetimepicker1" class="input-append">
                    <input data-format="yyyy-MM-dd" class="picker" id="doctor_notes_date" type="text" name="date" placeholder="Date" value="'.date('Y-m-d').'">
                    <span class="add-on">
                        &nbsp;<i data-time-icon="icon-time" data-date-icon="icon-calendar" style="cursor:pointer;">
                        </i>
                    </span>
                </div>
			</div>
		</div>
		
		<div class="col-sm-6" >
			<div class="form-group">
				<label class="control-label">Time</label>
				<div id="datetimepicker3" class="input-append">
				   <input data-format="hh:mm" class="picker" id="doctor_notes_time" name="time"  type="text" class="form-control" value="'.date('H:i').'">
				   <span class="add-on" style="cursor:pointer;">
					 &nbsp;<i data-time-icon="icon-time" data-date-icon="icon-calendar">
					 </i>
				   </span>
				</div>
			</div>
		</div>
	</div>';

echo
'
	<div class="row">
		<div class="col-md-12">
			 <textarea id="doctor_notes_item" rows="10" cols="100" class="cleditor" ></textarea>
		</div>
	</div>
	<br>
	<div class="row">
	    <div class="col-md-12">
	        <div class="form-group">
	            <div class="col-lg-12">
	                <div class="center-align">
	                      <a hred="#" class="btn btn-sm btn-primary" onclick="save_doctor_notes('.$visit_id.')">Update Continuation Notes</a>
	                  </div>
	            </div>
	        </div>
	    </div>
	</div>
';
}

else{
echo

'
<input data-format="yyyy-MM-dd" class="picker" id="doctor_notes_date" type="hidden" name="date" placeholder="Date" value="'.date('Y-m-d').'">
<input data-format="hh:mm" class="picker" id="doctor_notes_time" name="time"  type="hidden" class="form-control" value="'.date('H:i').'">
	<div class="row">
		<div class="col-md-12" style="height:500px;">
			 <textarea id="doctor_notes_item'.$visit_id.'" rows="10" cols="100" class="cleditor" ></textarea>
		</div>
	</div>
	<br>
	<div class="row">
	    <div class="col-md-12">
	        <div class="form-group">
	            <div class="col-lg-12">
	                <div class="center-align">
	                	<button  class="btn btn-sm btn-success"  type="submit"> Save Continuation Notes</button>
	                     
	                  </div>
	            </div>
	        </div>
	    </div>
	</div>
';
}
echo form_close();
	
?>


<script type="text/javascript">
	
	$(document).on("submit", "form#doctor_form", function (e) 
	{
		e.preventDefault();
		// alert("salkdkajshdkjashda");
		console.debug(tinymce.activeEditor.getContent());
	   	var doctor_notes = tinymce.get('doctor_notes_item'+<?php echo $visit_id;?>).getContent();
		var visit_id = '<?php echo $visit_id;?>';
		var config_url = $('#config_url').val();
        var data_url = config_url+"nurse/save_patient_doctor_notes/"+visit_id;
        // window.alert(doctor_notes);
         //var doctor_notes = $('#doctor_notes_item').val();//document.getElementById("vital"+vital_id).value;
		 var doctor_notes_date = $('#doctor_notes_date').val();
		 var doctor_notes_time = $('#doctor_notes_time').val();
		 // alert(doctor_notes_date);

        $.ajax({
			type:'POST',
			url: data_url,
			data:{notes: doctor_notes, date: doctor_notes_date, time: doctor_notes_time},
			dataType: 'json',
			success:function(data){
				if(data.result == 'success')
				{
					$('#doctor_notes_section').html(data.message);
					$('#soap_doctor_notes_section').html(data.message);
					alert("You have successfully updated the doctors' notes");
				}
				else
				{
					alert("Unable to update  doctors' notes");
				}
			//obj.innerHTML = XMLHttpRequestObject.responseText;
			},
			error: function(xhr, status, error) {
				//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
				alert(error);
			}

        });
		return false;
	})
	
	$(document).on("submit", "form#edit_doctor_notes", function (e) 
	{
		e.preventDefault();
		$('#notes_response'+notes_id).html('');
		var notes_id = $(this).attr('notes_id');
		var notes_div_name = 'doctor_notes'+notes_id;
	   	var doctor_notes = tinymce.get(notes_div_name).getContent();
		var doctor_notes_date = $('#date'+notes_id).val();
		var doctor_notes_time = $('#time'+notes_id).val();
		var notes_type_id = $('#notes_type_id'+notes_id).val();
		var visit_id = '<?php echo $visit_id;?>';
		var config_url = $('#config_url').val();
        var data_url = config_url+"nurse/update_doctor_notes/"+notes_id+'/'+notes_type_id+'/'+visit_id;
		// alert(data_url);
        $.ajax({
			type:'POST',
			url: data_url,
			data:{notes: doctor_notes, date: doctor_notes_date, time: doctor_notes_time},
			dataType: 'json',
			success:function(data){
				if(data.result == 'success')
				{
					$('#notes_response'+notes_id).html('<div class="alert alert-success center-align">Update successfull</div>');
					//nurse notes
					if(notes_type_id == '1')
					{
						$('#doctor_notes_section').html(data.message);
						$('#soap_doctor_notes_section').html(data.message);
					}
					
					//symptoms
					else if(notes_type_id == '3')
					{
						$('#symptoms_section').html(data.message);
					}
					
					//objective findings
					else if(notes_type_id == '4')
					{
						$('#objective_findings_section').html(data.message);
					}
					
					//assessment
					else if(notes_type_id == '5')
					{
						$('#assessment_section').html(data.message);
					}
					
					//plan
					else if(notes_type_id == '6')
					{
						$('#plan_section').html(data.message);
					}
					$('#edit_notes'+notes_id).modal('hide');
					//initiate WYSIWYG editor
					tinymce.init({
						selector: ".cleditor",
						height : "300"
					});
					//alert("You have successfully updated your notes");
				}
				else
				{
					alert("Unable to update the notes");
				}
			},
			error: function(xhr, status, error) {
				//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
				alert(error);
			}

        });
		return false;
	})
	
	$(document).on("click", "a.delete_doctor_notes", function (e) 
	{
		e.preventDefault();
		var visit_id = $(this).attr('visit_id');
		var notes_id = $(this).attr('notes_id');
		var notes_type_id = $(this).attr('notes_type_id');
		
        var data_url = config_url+"nurse/delete_doctor_notes/"+notes_id+'/'+notes_type_id+'/'+visit_id;
		
		$.ajax({
			type:'POST',
			url: data_url,
			data:{},
			dataType: 'json',
			success:function(data){
				if(data.result == 'success')
				{
					//nurse notes
					if(notes_type_id == '1')
					{
						$('#doctor_notes_section').html(data.message);
						$('#soap_doctor_notes_section').html(data.message);
					}
					
					//symptoms
					else if(notes_type_id == '3')
					{
						$('#symptoms_section').html(data.message);
					}
					
					//objective findings
					else if(notes_type_id == '4')
					{
						$('#objective_findings_section').html(data.message);
					}
					
					//assessment
					else if(notes_type_id == '5')
					{
						$('#assessment_section').html(data.message);
					}
					
					//plan
					else if(notes_type_id == '6')
					{
						$('#plan_section').html(data.message);
					}
					//initiate WYSIWYG editor
					tinymce.init({
						selector: ".cleditor",
						height : "300"
					});
					alert("You have successfully deleted your notes");
				}
				else
				{
					alert("Unable to delete the notes");
				}
			//obj.innerHTML = XMLHttpRequestObject.responseText;
			},
			error: function(xhr, status, error) {
				//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
				alert(error);
			}

        });
		return false;
	})
</script>