<?php

$income_rs = $this->company_financial_model->get_income_value('Revenue');

// var_dump($income_rs); die();
$income_result = '';
$total_income = 0;
if($income_rs->num_rows() > 0)
{
	foreach ($income_rs->result() as $key => $value) {
		# code...
		$total_amount = $value->total_amount;
		$transactionName = $value->parent_service;
		$total_income += $total_amount;
		$income_result .='<tr>
							<td class="text-left">'.strtoupper($transactionName).'</td>
							<td class="text-right">'.number_format($total_amount,2).'</td>
							</tr>';
	}
	$income_result .='<tr>
							<td class="text-left"><b>Total Income</b></td>
							<td class="text-right"><b>'.number_format($total_income,2).'</b></td>
							</tr>';
}




// $operation_rs = $this->company_financial_model->get_cog_value('Expense');





$cog_result = '';
$total_cog = 0;
$start_date = $this->company_financial_model->get_inventory_start_date();
$closing_stock =  $this->company_financial_model->get_opening_stock();
$total_purchases = $this->company_financial_model->get_product_purchases($start_date);
$total_other_purchases = $this->company_financial_model->get_product_other_purchases($start_date);
$total_return_outwards = $this->company_financial_model->get_product_return_outwards($start_date);
$total_sales = $this->company_financial_model->get_product_sales();
$total_other_deductions = $this->company_financial_model->get_total_other_deductions();
$current_stock = (($total_purchases+$closing_stock+$total_other_purchases) - ($total_sales + $total_return_outwards + $total_other_deductions));
$total_cog = $total_purchases+$closing_stock-$current_stock;
// if($cog_rs->num_rows() > 0)
// {
// 	foreach ($cog_rs->result() as $key => $value) {
// 		# code...
// 		$total_amount = $value->total_amount;
// 		$transactionName = $value->accountName;
// 		$total_cog += $total_amount;
// 		$cog_result .='<tr>
// 							<td class="text-left">'.$transactionName.'</td>
// 							<td class="text-right">'.number_format($total_amount,2).'</td>
// 							</tr>';
// 	}
//
// 	$cog_result .='<tr>
// 							<td class="text-left"><b>Total COG</b></td>
// 							<td class="text-right"><b>'.number_format($total_cog,2).'</b></td>
// 							</tr>';
// }

$operation_rs = $this->company_financial_model->get_operational_cost_value('Expense');

$operation_result = '';
$total_operational_amount = 0;
if($operation_rs->num_rows() > 0)
{
	foreach ($operation_rs->result() as $key => $value) {
		# code...
		$total_amount = $value->total_amount;
		$transactionName = $value->accountName;
		$total_operational_amount += $total_amount;
		$operation_result .='<tr>
							<td class="text-left">'.strtoupper($transactionName).'</td>
							<td class="text-right">'.number_format($total_amount,2).'</td>
							</tr>';
	}
	
}

$salary = $this->company_financial_model->get_salary_expenses();
$nssf = $this->company_financial_model->get_statutories(1);
$nhif = $this->company_financial_model->get_statutories(2);
$paye_amount = $this->company_financial_model->get_statutories(3);
$relief = $this->company_financial_model->get_statutories(4);


// 791
$paye = $paye_amount - $relief;

// $salary -= $nssf + $nhif + $paye_amount;
$salary -= $relief;
$total_operational_amount += $salary+$nssf+$nhif+$paye_amount;

$operation_result .='<tr>
							<td class="text-left"><b>Total Operation Cost</b></td>
							<td class="text-right"><b>'.number_format($total_operational_amount,2).'</b></td>
							</tr>';


$statement = $this->session->userdata('income_statement_title_search');

if(empty($statement))
{
	$checked = $statement;
}
else {
	$checked = date('M j, Y', strtotime(date('Y-01-01'))).' to ' .date('M j, Y', strtotime(date('Y-m-d')));
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title><?php echo $contacts['company_name'];?> | P & L</title>
        <!-- For mobile content -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- IE Support -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap/css/bootstrap.css" media="all"/>
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/stylesheets/theme-custom.css" media="all"/>
        <style type="text/css">
			.receipt_spacing{letter-spacing:0px; font-size: 12px;}
			.center-align{margin:0 auto; text-align:center;}

			.receipt_bottom_border{border-bottom: #888888 medium solid;}
			.row .col-md-12 table {
				border:solid #000 !important;
				border-width:1px 0 0 1px !important;
				font-size:10px;
			}
			.row .col-md-12 th, .row .col-md-12 td {
				border:solid #000 !important;
				border-width:0 1px 1px 0 !important;
			}
			.table thead > tr > th, .table tbody > tr > th, .table tfoot > tr > th, .table thead > tr > td, .table tbody > tr > td, .table tfoot > tr > td
			{
				 padding: 2px;
			}

			.row .col-md-12 .title-item{float:left;width: 130px; font-weight:bold; text-align:right; padding-right: 20px;}
			.title-img{float:left; padding-left:30px;}
			img.logo{max-height:70px; margin:0 auto;}
		</style>
    </head>
    <body class="receipt_spacing">
    	<div class="row">
        	<div class="col-xs-12">
            	<img src="<?php echo base_url().'assets/logo/'.$contacts['logo'];?>" alt="<?php echo $contacts['company_name'];?>" class="img-responsive logo"/>
            </div>
        </div>
    	<div class="row">
        	<div class="col-md-12 center-align receipt_bottom_border">
            	<strong>
                	<?php echo $contacts['company_name'];?><br/>
                    P.O. Box <?php echo $contacts['address'];?> <?php echo $contacts['post_code'];?>, <?php echo $contacts['city'];?><br/>
                    E-mail: <?php echo $contacts['email'];?>. Tel : <?php echo $contacts['phone'];?><br/>
                    <?php echo $contacts['location'];?>, <?php echo $contacts['building'];?>, <?php echo $contacts['floor'];?><br/>
                </strong>
            </div>
        </div>

      <div class="row receipt_bottom_border" >
        	<div class="col-md-12 center-align" style="padding: 5px;">
            	<strong>PROFIT AND LOSS STATEMENT</strong><br>

            	<?php
            	$search_title = $this->session->userdata('income_statement_title_search');

      				 if(empty($search_title))
      				 {
      				 	$search_title = "";
      				 }
      				 else
      				 {
      				 	$search_title =$search_title;
      				 }
				 echo $search_title;
            	?>

            </div>
        </div>

    	<div class="row">
        	<div style="margin: auto;max-width: 500px;">
						<div class="col-md-12">
						<div class="row">
							<h5> <strong>INCOME</strong></h5>
						</div>
						<table class="table">
							<thead>
								<th style="width: 60%"> ITEM </th>
								<th style="width: 40%">AMOUNT</th>
							</thead>

							<tbody>
								<?php echo $income_result;?>
							</tbody>
						</table>
						</div>
						<div class="col-md-12">
							<div class="row">

							<h5> <strong>COST OF GOOD SOLD</strong></h5>
							</div>
							<table class="table">
								<thead>
									<th style="width: 60%"> ITEM </th>
									<th style="width: 40%">AMOUNT</th>
								</thead>
								<tbody>
									<tr>
										<td>OPENING STOCK</td>
										<td class="text-right"> <?php echo number_format($closing_stock,2);?></td>
									</tr>
									<tr>
										<td>PURCHASES</td>
										<td class="text-right">  <?php echo number_format($total_purchases,2);?></td>
									</tr>
									<tr>
										<td>OTHER ADDITIONS</td>
										<td class="text-right">  <?php echo number_format($total_other_purchases,2);?></td>
									</tr>
									<tr>
										<td>RETURN OUTWARDS</td>
										<td class="text-right">  ( <?php echo number_format($total_return_outwards,2);?> )</td>
									</tr>
									<tr>
										<td>OTHER DEDUCTIONS</td>
										<td class="text-right"> ( <?php echo number_format($total_other_deductions,2);?> )</td>
									</tr>
									<tr>
										<td>CURRENT STOCK</td>
										<td class="text-right"><?php echo number_format($current_stock,2);?></td>
									</tr>
									<tr>
												<td >TOTAL GOODS SOLD</td>
												<td class="text-right"> ( <?php echo number_format($total_purchases+$closing_stock-$current_stock,2);?> )</td>
									</tr>
								</tbody>
							</table>


						</div>
						<div class="col-md-12">
								<table class="table">
								<tbody>
									<tr>
										<td style="width: 60%"><strong>GROSS PROFIT : (INCOME - TGS )</strong></td>
										<td style="width: 40%" class="text-right"><strong style="border-top: 2px solid #000">Ksh. <?php echo number_format($total_income - $total_cog,2);?></strong></td>
									</tr>
								</tbody>
							</table>
						</div>

						<div class="col-md-12">
							<div class="row">
								<h5> <strong>EXPENSES</strong></h5>
							</div>
							<table class="table">
								<thead>
									<th style="width: 60%"> ITEM </th>
									<th style="width: 40%">AMOUNT</th>
								</thead>
								<tbody>
									
									<tr>
										<td class="text-left">SALARY AND SALARY ADVANCE</td>
										<td class="text-right"> <?php echo number_format($salary,2);?> </td>
									</tr>
									<tr>
										<td class="text-left">PAYE</td>
										<td class="text-right">  <?php echo number_format($paye,2);?> </td>
									</tr>
									<tr>
										<td class="text-left">NSSF</td>
										<td class="text-right">  <?php echo number_format($nssf,2);?></td>
									</tr>
									<tr>
										<td class="text-left">NHIF</td>
										<td class="text-right">  <?php echo number_format($nhif,2);?> </td>
									</tr>
									<?php echo $operation_result;?>
								</tbody>
							</table>
						</div>
						<div class="col-md-12">
								<table class="table">
								<tbody>
									<tr>
										<td style="width: 60%"><strong>NET PROFIT</strong></td>
										<td style="width: 40%" class="text-right"><strong style="border-top: 2px solid #000">Ksh. <?php echo number_format($total_income - $total_cog - $total_operational_amount,2)?></strong></td>
									</tr>
								</tbody>
							</table>
						</div>
            </div>
        </div>

    	<div class="row" style="font-style:italic; font-size:11px;">
        	<div class="col-sm-12">
                <div class="col-sm-10 pull-left">
                    <strong>Prepared by: </strong>
                </div>
                <div class="col-sm-2 pull-right">
                    <?php echo date('jS M Y H:i a'); ?>
                </div>
            </div>

        </div>
    </body>

</html>
