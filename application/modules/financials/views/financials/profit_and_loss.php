<?php echo $this->load->view('search/search_profit_and_loss','', true);?>
<?php

$income_rs = $this->company_financial_model->get_income_value('Revenue');

// var_dump($income_rs); die();
$income_result = '';
$total_income = 0;
if($income_rs->num_rows() > 0)
{
	foreach ($income_rs->result() as $key => $value) {
		# code...
		$total_amount = $value->total_amount;
		$transactionName = $value->parent_service;
		$service_id = $value->service_id;
		$total_income += $total_amount;
		$income_result .='<tr>
							<td class="text-left">'.strtoupper($transactionName).'</td>
							<td class="text-right">
							<a href="'.site_url().'company-financials/services-bills/'.$service_id.'" >'.number_format($total_amount,2).'</a></td>
							</tr>';
	}
	$income_result .='<tr>
							<td class="text-left"><b>Total Income</b></td>
							<td class="text-right"><b>'.number_format($total_income,2).'</b></td>
							</tr>';
}




// $operation_rs = $this->company_financial_model->get_cog_value('Expense');





$cog_result = '';
$total_cog = 0;
$start_date = $this->company_financial_model->get_inventory_start_date();
$closing_stock =  $this->company_financial_model->get_opening_stock();
$total_purchases = $this->company_financial_model->get_product_purchases($start_date);
$total_other_purchases = $this->company_financial_model->get_product_other_purchases($start_date);
$total_return_outwards = $this->company_financial_model->get_product_return_outwards($start_date);
$total_sales = $this->company_financial_model->get_product_sales();
$total_other_deductions = $this->company_financial_model->get_total_other_deductions();
$current_stock = (($total_purchases+$closing_stock+$total_other_purchases) - ($total_sales + $total_return_outwards + $total_other_deductions));
$total_cog = $total_purchases+$closing_stock-$current_stock;




$operation_rs = $this->company_financial_model->get_operational_cost_value('Expense');

$operation_result = '';
$total_operational_amount = 0;
if($operation_rs->num_rows() > 0)
{
	foreach ($operation_rs->result() as $key => $value) {
		# code...
		$total_amount = $value->total_amount;
		$transactionName = $value->accountName;
		$account_id = $value->accountId;
		$total_operational_amount += $total_amount;
		$operation_result .='<tr>
							<td class="text-left">'.strtoupper($transactionName).'</td>
							<td class="text-right"><a href="'.site_url().'accounting/expense-ledger/'.$account_id.'" >'.number_format($total_amount,2).'</a></td>
							</tr>';
	}
	
}

$salary = $this->company_financial_model->get_salary_expenses();
$nssf = $this->company_financial_model->get_statutories(1);
$nhif = $this->company_financial_model->get_statutories(2);
$paye_amount = $this->company_financial_model->get_statutories(3);
$relief = $this->company_financial_model->get_statutories(4);



$paye = $paye_amount - $relief;

// $salary -= $nssf + $nhif + $paye_amount;

$other_deductions = $salary - ($nssf+$nhif+$paye_amount);
$salary -= $relief;
$total_operational_amount += $salary+$nssf+$nhif+$paye_amount;
$operation_result .='<tr>
							<td class="text-left"><b>Total Operation Cost</b></td>
							<td class="text-right" style="border-top:#3c8dbc solid 2px;"><b>'.number_format($total_operational_amount,2).'</b></td>
							</tr>';


$statement = $this->session->userdata('income_statement_title_search');

// var_dump($statement);die();

if(!empty($statement))
{
	$checked = $statement;
}
else {
	$checked = 'Reporting period: '.date('M j, Y', strtotime(date('Y-01-01'))).' to ' .date('M j, Y', strtotime(date('Y-m-d')));
}
?>

<div class="text-center">
	<h3 class="box-title">Income Statement</h3>
	<h5 class="box-title"> <?php echo $checked?></h5>
	<h6 class="box-title">Created <?php echo date('M j, Y', strtotime(date('Y-m-d')));?></h6>
</div>

<section class="panel">
		<header class="panel-heading">
				<h5 class="pull-left"><i class="icon-reorder"></i>Revenue</h5>
				<div class="clearfix"></div>
		</header>
		<!-- /.box-header -->
		<div class="panel-body">
			<h5 class="box-title" style="background-color:#3c8dbc;color:#fff;padding:5px;">INCOME</h5>
    	<table class="table  table-striped table-condensed">
			<thead>
				<tr>
        			<th class="text-left">Account</th>
					<th class="text-right">Balance</th>
				</tr>
			</thead>
			<tbody>
				<?php echo $income_result;?>
			</tbody>
		</table>


			<h5 class="box-title" style="background-color:#3c8dbc;color:#fff;padding:5px;">COST OF GOODS SOLD</h5>
    	<table class="table  table-striped table-condensed">
			<thead>
				<tr>
        			<th class="text-left">Account</th>
					<th class="text-right">Balance</th>
				</tr>
			</thead>
			<tbody>
				<tr>
							<td >OPENING STOCK</td>
							<td class="text-right"><a href="<?php echo site_url().'view-closing-stock'?>" target="_blank"><?php echo number_format($closing_stock,2);?></a> </td>
				</tr>
				<tr>
							<td >PURCHASES</td>
							<td class="text-right"><a href="<?php echo site_url().'view-purchases'?>" target="_blank"><?php echo number_format($total_purchases,2);?></a> </td>
				</tr>
				<tr>
							<td >OTHER ADDITIONS</td>
							<td class="text-right" ><a href="<?php echo site_url().'view-other-additions'?>" target="_blank"><?php echo number_format($total_other_purchases,2);?></a> </td>
				</tr>
				<tr>
							<td >RETURN OUTWARDS</td>
							<td class="text-right" ><a href="<?php echo site_url().'view-return-outwards'?>" target="_blank"><?php echo number_format($total_return_outwards,2);?></a> </td>
				</tr>
				<tr>
							<td >OTHER DEDUCTIONS</td>
							<td class="text-right" ><a href="<?php echo site_url().'view-other-deductions'?>" target="_blank"><?php echo number_format($total_other_deductions,2);?></a> </td>
				</tr>

				<tr>
							<td >CURRENT STOCK</td>
							<td class="text-right" ><a href="<?php echo site_url().'view-current-stock'?>" target="_blank"><?php echo number_format($current_stock,2);?></a> </td>
				</tr>


				<tr>
							<td ><b>TOTAL GOODS SOLD<b></td>
							<td class="text-right" style="border-top:#3c8dbc solid 1px;">(<?php echo number_format($total_cog,2);?>)</td>
				</tr>
				<tr>
        			<td class="text-left"><strong>GROSS PROFIT</strong> (Total Income - Total Goods Sold)</td>
					<th class="text-right"  style="border-top:#3c8dbc solid 2px;"><?php echo number_format($total_income - $total_cog,2)?></th>
				</tr>
			</tbody>
		</table>

		<h5 class="box-title" style="background-color:#3c8dbc;color:#fff;padding:5px;">OPERATING EXPENSE</h5>
    	<table class="table  table-striped table-condensed">
			<thead>
				<tr>
        			<th class="text-left">Account</th>
					<th class="text-right">Balance</th>
				</tr>
			</thead>
			<tbody>
				
				<tr>
					<td class="text-left">SALARY AND SALARY ADVANCE</td>
					<td class="text-right"> <?php echo number_format($salary,2);?> </td>
				</tr>
				<tr>
					<td class="text-left">PAYE</td>
					<td class="text-right">  <?php echo number_format($paye,2);?> </td>
				</tr>
				<tr>
					<td class="text-left">NSSF</td>
					<td class="text-right">  <?php echo number_format($nssf,2);?></td>
				</tr>
				<tr>
					<td class="text-left">NHIF</td>
					<td class="text-right">  <?php echo number_format($nhif,2);?> </td>
				</tr>
				<tr>
					<td class="text-left">LOANS / WAGES / ADVANCES</td>
					<td class="text-right">  <?php echo number_format($other_deductions,2);?> </td>
				</tr>
				<?php echo $operation_result?>
				<tr>
        			<th class="text-left"><strong>Operating Profit (Loss)</strong></th>
					<th class="text-right" style="border-top:#3c8dbc solid 3px;"><?php echo number_format($total_income - $total_cog - $total_operational_amount,2)?></th>
				</tr>

			</tbody>
		</table>

		<!-- <h5 class="box-title">INTEREST (INCOME), EXPENSE & TAXES</h5> -->
    	<table class="table  table-striped table-condensed">
			<thead>
				<tr>
        			<th class="text-left"></th>
					<th class="text-right"></th>
				</tr>
			</thead>
			<tbody>

				<tr>
        			<th class="text-left"><strong>NET Profit</strong></th>
					<th class="text-right"><?php echo number_format($total_income - $total_cog - $total_operational_amount,2)?></th>
				</tr>
			</tbody>
		</table>
    </div>
</section>
