<?php echo $this->load->view('search/search_account_transactions','', true);?>


<div class="text-center">
	<h3 class="box-title">Account Transactions</h3>
	<h5 class="box-title">Reporting period: January 01, 2019 to February 14, 2019</h5>
	<h6 class="box-title">Created February 14, 2019</h6>
</div>

<div class="box">
    <div class="box-body">
    	<!-- <h3 class="box-title">Revenue</h3> -->
    	<table class="table  table-striped table-condensed">
			<thead>
				<tr>
        			<th class="text-left">Account</th>
        			<th class="text-right">Debit</th>
        			<th class="text-right">Credit</th>
					<th class="text-right">NET Movement</th>
				</tr>
			</thead>
			<tbody>
				<tr>
        			<td class="text-left">Web Development</td>
					<td class="text-right">200,00</td>
					<td class="text-right">200,00</td>
					<td class="text-right">200,00</td>
				</tr>
				<tr>
        			<td class="text-left">Web Development</td>
					<td class="text-right">200,00</td>
					<td class="text-right">200,00</td>
					<td class="text-right">200,00</td>
				</tr>
				<tr>
        			<td class="text-left">Web Development</td>
					<td class="text-right">200,00</td>
					<td class="text-right">200,00</td>
					<td class="text-right">200,00</td>
				</tr>
			</tbody>
		</table>


    </div>
</div>
