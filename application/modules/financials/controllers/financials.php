<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Financials extends MX_Controller 
{	
    var $documents_path;


	function __construct()
	{
		parent:: __construct();
		$this->load->model('auth/auth_model');
		$this->load->model('site/site_model');
		$this->load->model('admin/users_model');
		$this->load->model('hr/personnel_model');
		$this->load->model('customers/customers_model');
		$this->load->model('admin/sections_model');
		$this->load->model('admin/branches_model');
		$this->load->model('admin/admin_model');
		$this->load->model('admin/file_model');
		$this->load->model('vehicles/vehicle_model');
		$this->load->model('drivers/driver_model');
		$this->load->model('financials_model');


		
		$this->load->model('admin/file_model');

		//path to image directory
		$this->documents_path = realpath(APPPATH . '../assets/documents/vehicles');
		
		
		$this->load->library('image_lib');
		
		if(!$this->auth_model->check_login())
		{
			redirect('login');
		}
	}
    
	/*
	*
	*	Default action is to show all the customer
	*
	*/
	public function index($order = 'personnel.personnel_id', $order_method = 'ASC') 
	{

		$where = 'personnel.personnel_id = personnel_job.personnel_id AND personnel_job.personnel_id = personnel.personnel_id';
		$table = 'personnel,personnel_job';
		//pagination
		$segment = 5;
		$this->load->library('pagination');
		$config['base_url'] = site_url().'driver'.$order.'/'.$order_method;
		$config['total_rows'] = $this->users_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active">';
		$config['cur_tag_close'] = '</li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $data["links"] = $this->pagination->create_links();
		$query = $this->driver_model->get_all_drivers($table, $where, $config["per_page"], $page, $order, $order_method);
		
		//change of order method 
		if($order_method == 'DESC')
		{
			$order_method = 'ASC';
		}
		
		else
		{
			$order_method = 'DESC';
		}
		
		$data['title'] = 'Driver Financials';
		$v_data['title'] = $data['title'];
		
		$v_data['order'] = $order;
		$v_data['order_method'] = $order_method;
		$v_data['query'] = $query;
		$v_data['page'] = $page;
		$data['content'] = $this->load->view('financials/financials/all_financials', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}


	/*
	*
	*	Add a new customer
	*
	*/
	public function assign_drivers($personnel_id) 
	{
		
		//form validation rules
		$this->form_validation->set_rules('vehicle_id', 'Vehicle Name', 'required|xss_clean');
		

		//if form has been submitted
		if ($this->form_validation->run())
		{
		
			if($this->driver_model->assign_drivers($personnel_id))
			{
				$this->session->set_userdata('success_message', 'Driver was successfully assigned ');
				
			}
			
			else
			{
				$this->session->set_userdata('error_message', 'Could not assign driver. Please try again');
			}
		}
		
		
		//open the add new Customers
		
		$data['title'] = 'Assign Driver';
		$v_data['vehicles'] = $this->vehicle_model->get_all_vehicles_drivers();
		$v_data['vehicles_history'] = $this->driver_model->get_personnel_vehicles_data($personnel_id);
		$v_data['title'] = $data['title'];
	
		$data['content'] = $this->load->view('drivers/drivers/assign_drivers', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}
	/*
	*
	*	Add a new customer
	*
	*/
	public function add_vehicle() 
	{
		
		//form validation rules
		$this->form_validation->set_rules('vehicle_name', 'Vehicle Name', 'required|xss_clean');
		$this->form_validation->set_rules('vehicle_plate', 'Vehicle Plate', 'required|xss_clean');
		$this->form_validation->set_rules('vehicle_capacity', 'Vehicle Capacity', 'required|xss_clean');
	

		
		//if form has been submitted
		if ($this->form_validation->run())
		{
			
			if($this->vehicle_model->add_vehicle())
			{
				$this->session->set_userdata('success_message', 'Vehicle added successfully');
				redirect('vehicles');
			}
			
			else
			{
				$this->session->set_userdata('error_message', 'Could not add Vehicle. Please try again');
			}
		}
		
		
		//open the add new Customers
		
		$data['title'] = 'Add Vehicles';
		$v_data['title'] = $data['title'];
	
		$data['content'] = $this->load->view('vehicles/vehicles/add_vehicle', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}
    
	/*
	*
	*	Edit an existing vehicle
	*	@param int $vehicle_id
	*
	*/
	public function edit_vehicle($vehicle_id) 
	{
		//form validation rules
		$this->form_validation->set_rules('vehicle_name', 'Vehicle Name', 'required|xss_clean');
		$this->form_validation->set_rules('vehicle_plate', 'Vehicle Plate', 'required|xss_clean');
		$this->form_validation->set_rules('vehicle_capacity', 'Vehicle Capacity', 'required|xss_clean');
		
		//if form has been submitted
		if ($this->form_validation->run())
		{
			//update vehicle
			if($this->vehicle_model->update_vehicle($vehicle_id))
			{
				$this->session->set_userdata('success_message', 'Vehicle updated successfully');
				redirect('vehicles');
			}
			
			else
			{
				$this->session->set_userdata('error_message', 'Could not update vehicle. Please try again');
			}
		}
		
		//open the add new vehicle
		$data['title'] = 'Edit vehicle';
		$v_data['title'] = $data['title'];
		
		//select the vehicle from the database
		$query = $this->vehicle_model->get_vehicle($vehicle_id);
		
		if ($query->num_rows() > 0)
		{
			$v_data['vehicle'] = $query->result();
			
			$data['content'] = $this->load->view('vehicles/vehicles/edit_vehicle', $v_data, true);
		}
		
		else
		{
			$data['content'] = 'Vehicle does not exist';
		}
		
		$this->load->view('admin/templates/general_page', $data);
	}
    

    
	/*
	*
	*	Activate an existing vehicle
	*	@param int $vehicle_id
	*
	*/
	public function activate_vehicle($vehicle_id)
	{
		if($this->vehicle_model->activate_vehicle($vehicle_id))
		{
			$this->session->set_userdata('success_message', 'Vehicle activated successfully');
		}
		
		else
		{
			$this->session->set_userdata('error_message', 'Vehicle could not be activated. Please try again');
		}
		redirect('vehicles');
	}
    
	/*
	*
	*	Deactivate an existing vehicle
	*	@param int $vehicle_id
	*
	*/
	public function deactivate_vehicle($vehicle_id)
	{
		if($this->vehicle_model->deactivate_vehicle($vehicle_id))
		{
			$this->session->set_userdata('success_message', 'Vehicle disabled successfully');
		}
		
		else
		{
			$this->session->set_userdata('error_message', 'Vehicle could not be disabled. Please try again');
		}
		redirect('vehicles');
	}
	public function disburse_mpesa($mpesa_contact_id)
	{
		if (!empty($mpesa_contact_id))
		{
			if($this->financials_model->disburse_mpesa($mpesa_contact_id))
			{
				$this->session->set_userdata('success_message', 'Successfully Disbursed the amount');
				
			}
			
			else
			{
				$this->session->set_userdata('error_message', 'Could not disburse amount. Please try again');
			}
		}

		redirect('financials');
	}

}
?>