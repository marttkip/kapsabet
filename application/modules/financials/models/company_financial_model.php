<?php

class Company_financial_model extends CI_Model
{

	public function get_income_value($transactionCategory)
	{
		//retrieve all users

		$search_status = $this->session->userdata('income_statement_search');
		$search_payments_add = '';
		$search_invoice_add = '';
		if($search_status == 1)
		{
			$date_from = $this->session->userdata('date_from_income_statement');
			$date_to = $this->session->userdata('date_to_income_statement');

			if(!empty($date_from) AND !empty($date_to))
			{
				$search_invoice_add =  ' AND (transaction_date >= \''.$date_from.'\' AND transaction_date <= \''.$date_to.'\') ';
			}
			else if(!empty($date_from))
			{
				$search_invoice_add = ' AND transaction_date = \''.$date_from.'\'';
			}
			else if(!empty($date_to))
			{
				$search_invoice_add = ' AND transaction_date = \''.$date_to.'\'';
			}
		}
		else
		{
			$search_invoice_add = '';

		}

		$this->db->from('v_all_invoice_payments,service');
		$this->db->select('SUM(cr_amount) AS total_amount,service.service_name AS parent_service,service.service_id');
		$this->db->where('service.service_id = v_all_invoice_payments.parent_service  AND  transactionCategory = "'.$transactionCategory.'" '.$search_invoice_add);
		$this->db->group_by('parent_service');
		$query = $this->db->get();

		return $query;
	}

	public function get_operational_cost_value($transactionCategory)
	{

		$search_status = $this->session->userdata('income_statement_search');
		$search_payments_add = '';
		$search_invoice_add = '';
		if($search_status == 1)
		{
			$date_from = $this->session->userdata('date_from_income_statement');
			$date_to = $this->session->userdata('date_to_income_statement');

			if(!empty($date_from) AND !empty($date_to))
			{
				$search_invoice_add =  ' AND (transactionDate >= \''.$date_from.'\' AND transactionDate <= \''.$date_to.'\') ';
			}
			else if(!empty($date_from))
			{
				$search_invoice_add = ' AND transactionDate = \''.$date_from.'\'';
			}
			else if(!empty($date_to))
			{
				$search_invoice_add = ' AND transactionDate = \''.$date_to.'\'';
			}
		}
		else
		{
			$search_invoice_add = '';

		}
		//retrieve all users
		$this->db->from('v_general_ledger');
		$this->db->select('SUM(dr_amount) AS total_amount,accountName,accountId');
		$this->db->where('transactionCategory = "'.$transactionCategory.'" '.$search_invoice_add);
		$this->db->group_by('accountId');
		$query = $this->db->get();

		return $query;
	}


	public function get_cog_value($transactionCategory)
	{
		//retrieve all users
		$this->db->from('v_all_invoice_payments');
		$this->db->select('SUM(dr_amount) AS total_amount,accountName');
		$this->db->where('transactionCategory = "'.$transactionCategory.'" AND (patient_id IS NULL OR patient_id = 0)');
		$this->db->group_by('accountId');
		$query = $this->db->get();

		return $query;
	}


	public function get_payables_aging_report()
	{
		//retrieve all users
		$this->db->from('v_aged_payables');
		$this->db->select('*');
		// $this->db->where('');
		// $this->db->group_by('accountId');
		$query = $this->db->get();

		return $query;
	}


		public function get_payables_aging_report_by_creditor($creditor_id)
		{
			//retrieve all users
			$this->db->from('v_aged_payables');
			$this->db->select('*');
			$this->db->where('recepientId ='.$creditor_id);
			// $this->db->group_by('accountId');
			$query = $this->db->get();

			return $query;
		}

	public function get_receivables_aging_report()
	{
		//retrieve all users
		$this->db->from('v_aged_receivables');
		$this->db->select('*');
		// $this->db->where('');
		// $this->db->group_by('accountId');
		$query = $this->db->get();

		return $query;
	}



	public function get_account_value()
	{

		$search_status = $this->session->userdata('balance_sheet_search');
		$search_payments_add = '';
		$search_invoice_add = '';
		if($search_status == 1)
		{
			$date_from = $this->session->userdata('date_from_balance_sheet');
			$date_to = $this->session->userdata('date_to_balance_sheet');

			if(!empty($date_from) AND !empty($date_to))
			{
				$search_invoice_add =  ' AND (transactionDate >= \''.$date_from.'\' AND transactionDate <= \''.$date_to.'\') ';
			}
			else if(!empty($date_from))
			{
				$search_invoice_add = ' AND transactionDate = \''.$date_from.'\'';
			}
			else if(!empty($date_to))
			{
				$search_invoice_add = ' AND transactionDate = \''.$date_to.'\'';
			}
		}
		else
		{
			$search_invoice_add =  ' AND (transactionDate >= \''.date("Y-01-01").'\' AND transactionDate <= \''.date("Y-m-d").'\') ';

		}
		//retrieve all users
		$this->db->from('v_general_ledger,account');
		$this->db->select('(SUM(dr_amount) - SUM(cr_amount) + account.account_opening_balance) AS total_amount,accountName,account_id');
		$this->db->where('accountParentId = 2 AND v_general_ledger.accountId = account.account_id AND account.paying_account = 0  '.$search_invoice_add);
		$this->db->group_by('accountId');
		$query = $this->db->get();

		return $query;

	}


	public function get_accounts_receivables()
	{


		$search_status = $this->session->userdata('balance_sheet_search');
		$search_payments_add = '';
		$search_invoice_add = '';
		if($search_status == 1)
		{
			$date_from = $this->session->userdata('date_from_balance_sheet');
			$date_to = $this->session->userdata('date_to_balance_sheet');

			if(!empty($date_from) AND !empty($date_to))
			{
				$search_invoice_add =  ' AND (transaction_date >= \''.$date_from.'\' AND transaction_date <= \''.$date_to.'\') ';
			}
			else if(!empty($date_from))
			{
				$search_invoice_add = ' AND transaction_date = \''.$date_from.'\'';
			}
			else if(!empty($date_to))
			{
				$search_invoice_add = ' AND transaction_date = \''.$date_to.'\'';
			}
		}
		else
		{
			$search_invoice_add =  ' AND (transaction_date >= \''.date("Y-01-01").'\' AND transaction_date <= \''.date("Y-m-d").'\') ';

		}
		//retrieve all users
		$this->db->from('v_transactions');
		$this->db->select('SUM(dr_amount) - SUM(cr_amount) AS total_amount');
		$this->db->where('(party = "Patient") '.$search_invoice_add);
		// $this->db->group_by('accountId');
		$query = $this->db->get();
		$query_row = $query->row();
		$total_invoices_balance = $query_row->total_amount;

		return $total_invoices_balance ;

	}

	public function get_cash_on_hand()
	{

		$search_status = $this->session->userdata('balance_sheet_search');
		$search_payments_add = '';
		$search_invoice_add = '';
		if($search_status == 1)
		{
			$date_from = $this->session->userdata('date_from_balance_sheet');
			$date_to = $this->session->userdata('date_to_balance_sheet');

			if(!empty($date_from) AND !empty($date_to))
			{
				$search_invoice_add =  ' AND (transaction_date >= \''.$date_from.'\' AND transaction_date <= \''.$date_to.'\') ';
			}
			else if(!empty($date_from))
			{
				$search_invoice_add = ' AND transaction_date = \''.$date_from.'\'';
			}
			else if(!empty($date_to))
			{
				$search_invoice_add = ' AND transaction_date = \''.$date_to.'\'';
			}
		}
		else
		{
			$search_invoice_add =  ' AND (transaction_date >= \''.date("Y-01-01").'\' AND transaction_date <= \''.date("Y-m-d").'\') ';

		}
		$this->db->from('v_transactions');
		$this->db->select('SUM(cr_amount) AS total_amount');
		$this->db->where('(party = "Patient" AND payment_method_name ="Cash") '.$search_invoice_add);
		// $this->db->group_by('accountId');
		$query = $this->db->get();
		$query_row = $query->row();
		$total_invoices_balance = $query_row->total_amount;

		$search_status = $this->session->userdata('balance_sheet_search');
		$search_payments_add = '';
		$search_invoice_add = '';
		if($search_status == 1)
		{
			$date_from = $this->session->userdata('date_from_balance_sheet');
			$date_to = $this->session->userdata('date_to_balance_sheet');

			if(!empty($date_from) AND !empty($date_to))
			{
				$search_invoice_addold =  ' AND (transactionDate >= \''.$date_from.'\' AND transactionDate <= \''.$date_to.'\') ';
			}
			else if(!empty($date_from))
			{
				$search_invoice_addold = ' AND transactionDate = \''.$date_from.'\'';
			}
			else if(!empty($date_to))
			{
				$search_invoice_addold = ' AND transactionDate = \''.$date_to.'\'';
			}
		}
		else
		{
			$search_invoice_addold =  ' AND (transactionDate >= \''.date("Y-01-01").'\' AND transactionDate <= \''.date("Y-m-d").'\') ';

		}


		$this->db->from('v_general_ledger');
		$this->db->select('SUM(cr_amount) AS total_amount');
		$this->db->where('(accountName = "Cash Account" AND transactionCategory ="Transfer") '.$search_invoice_addold);
		// $this->db->group_by('accountId');
		$query = $this->db->get();
		$query_row2 = $query->row();
		$total_transfer_balance = $query_row2->total_amount;

		return $total_invoices_balance - $total_transfer_balance;
	}
	public function get_accounts_payable()
	{
		//retrieve all users

		$search_status = $this->session->userdata('balance_sheet_search');
		$search_payments_add = '';
		$search_invoice_add = '';
		if($search_status == 1)
		{
			$date_from = $this->session->userdata('date_from_balance_sheet');
			$date_to = $this->session->userdata('date_to_balance_sheet');

			if(!empty($date_from) AND !empty($date_to))
			{
				$search_invoice_add =  ' AND (transactionDate >= \''.$date_from.'\' AND transactionDate <= \''.$date_to.'\') ';
			}
			else if(!empty($date_from))
			{
				$search_invoice_add = ' AND transactionDate = \''.$date_from.'\'';
			}
			else if(!empty($date_to))
			{
				$search_invoice_add = ' AND transactionDate = \''.$date_to.'\'';
			}
		}
		else
		{
			$search_invoice_add =  ' AND (transactionDate >= \''.date("Y-01-01").'\' AND transactionDate <= \''.date("Y-m-d").'\') ';

		}


		$this->db->from('v_general_ledger');
		$this->db->select('(SUM(dr_amount) - SUM(cr_amount)) AS total_amount');
		$this->db->where('recepientId > 0  '.$search_invoice_add);
		// $this->db->group_by('accountId');
		$query = $this->db->get();
		$query_row = $query->row();
		$total_invoices_balance = $query_row->total_amount;

		//
		// $this->db->from('v_general_ledger');
		// $this->db->select('(SUM(cr_amount)) AS total_payments');
		// $this->db->where('recepientId > 0 AND transactionCategory = "Expense Payment" '.$search_invoice_add);
		// // $this->db->group_by('accountId');
		// $query = $this->db->get();
		// $query_credit_row = $query->row();
		// $total_payments = $query_credit_row->total_payments;

		return $total_invoices_balance;
	}


	public function get_total_wht_tax()
	{
		$search_status = $this->session->userdata('balance_sheet_search');
		$search_payments_add = '';
		$search_invoice_add = '';
		if($search_status == 1)
		{
			$date_from = $this->session->userdata('date_from_balance_sheet');
			$date_to = $this->session->userdata('date_to_balance_sheet');

			if(!empty($date_from) AND !empty($date_to))
			{
				$search_invoice_add =  ' AND (created >= \''.$date_from.'\' AND created <= \''.$date_to.'\') ';
			}
			else if(!empty($date_from))
			{
				$search_invoice_add = ' AND created = \''.$date_from.'\'';
			}
			else if(!empty($date_to))
			{
				$search_invoice_add = ' AND created = \''.$date_to.'\'';
			}
		}
		else
		{
			$search_invoice_add =  ' AND (created >= \''.date("Y-01-01").'\' AND created <= \''.date("Y-m-d").'\') ';

		}
		//retrieve all users
		$this->db->from('creditor_invoice_item');
		$this->db->select('SUM(vat_amount) AS total_amount');
		$this->db->where('vat_type_id = 2 AND vat_amount > 0 '.$search_invoice_add);
		// $this->db->group_by('accountId');
		$query = $this->db->get();
		$query_row = $query->row();
		$total_invoices_balance = $query_row->total_amount;

		$search_status = $this->session->userdata('balance_sheet_search');
		$search_payments_add = '';
		$search_invoice_add = '';
		if($search_status == 1)
		{
			$date_from = $this->session->userdata('date_from_balance_sheet');
			$date_to = $this->session->userdata('date_to_balance_sheet');

			if(!empty($date_from) AND !empty($date_to))
			{
				$search_invoice_add =  ' AND (created >= \''.$date_from.'\' AND created <= \''.$date_to.'\') ';
			}
			else if(!empty($date_from))
			{
				$search_invoice_add = ' AND created = \''.$date_from.'\'';
			}
			else if(!empty($date_to))
			{
				$search_invoice_add = ' AND created = \''.$date_to.'\'';
			}
		}
		else
		{
			$search_invoice_add =  ' AND (created >= \''.date("Y-01-01").'\' AND created <= \''.date("Y-m-d").'\') ';

		}

		//retrieve all users
		$this->db->from('creditor_credit_note_item');
		$this->db->select('SUM(credit_note_charged_vat) AS total_amount');
		$this->db->where('vat_type_id = 2 AND credit_note_charged_vat > 0 '.$search_invoice_add);
		// $this->db->group_by('accountId');
		$query2 = $this->db->get();
		$query_row2 = $query2->row();
		$total_credit_note_balance = $query_row2->total_amount;

		return $total_invoices_balance - $total_credit_note_balance;

	}

	public function get_total_vat_tax()
	{

		$search_status = $this->session->userdata('balance_sheet_search');
		$search_payments_add = '';
		$search_invoice_add = '';
		if($search_status == 1)
		{
			$date_from = $this->session->userdata('date_from_balance_sheet');
			$date_to = $this->session->userdata('date_to_balance_sheet');

			if(!empty($date_from) AND !empty($date_to))
			{
				$search_invoice_add =  ' AND (created >= \''.$date_from.'\' AND created <= \''.$date_to.'\') ';
			}
			else if(!empty($date_from))
			{
				$search_invoice_add = ' AND created = \''.$date_from.'\'';
			}
			else if(!empty($date_to))
			{
				$search_invoice_add = ' AND created = \''.$date_to.'\'';
			}
		}
		else
		{
			$search_invoice_add =  ' AND (created >= \''.date("Y-01-01").'\' AND created <= \''.date("Y-m-d").'\') ';

		}
		//retrieve all users
		$this->db->from('creditor_invoice_item');
		$this->db->select('SUM(vat_amount) AS total_amount');
		$this->db->where('vat_type_id = 1 AND vat_amount > 0 '.$search_invoice_add);
		// $this->db->group_by('accountId');
		$query = $this->db->get();
		$query_row = $query->row();
		$total_invoices_balance = $query_row->total_amount;

		$search_status = $this->session->userdata('balance_sheet_search');
		$search_payments_add = '';
		$search_invoice_add = '';
		if($search_status == 1)
		{
			$date_from = $this->session->userdata('date_from_balance_sheet');
			$date_to = $this->session->userdata('date_to_balance_sheet');

			if(!empty($date_from) AND !empty($date_to))
			{
				$search_invoice_add =  ' AND (created >= \''.$date_from.'\' AND created <= \''.$date_to.'\') ';
			}
			else if(!empty($date_from))
			{
				$search_invoice_add = ' AND created = \''.$date_from.'\'';
			}
			else if(!empty($date_to))
			{
				$search_invoice_add = ' AND created = \''.$date_to.'\'';
			}
		}
		else
		{
			$search_invoice_add =  ' AND (created >= \''.date("Y-01-01").'\' AND created <= \''.date("Y-m-d").'\') ';

		}

		//retrieve all users
		$this->db->from('creditor_credit_note_item');
		$this->db->select('SUM(credit_note_charged_vat) AS total_amount');
		$this->db->where('vat_type_id = 1 AND credit_note_charged_vat > 0 '.$search_invoice_add);
		// $this->db->group_by('accountId');
		$query2 = $this->db->get();
		$query_row2 = $query2->row();
		$total_credit_note_balance = $query_row2->total_amount;

		// var_dump($total_credit_note_balance);die();
		return $total_invoices_balance - $total_credit_note_balance;

	}


	public function get_tax_total_wht_tax()
	{
		$search_status = $this->session->userdata('tax_search');
		$search_payments_add = '';
		$search_invoice_add = '';
		if($search_status == 1)
		{
			$date_from = $this->session->userdata('date_from_tax');
			$date_to = $this->session->userdata('date_to_tax');

			if(!empty($date_from) AND !empty($date_to))
			{
				$search_invoice_add =  ' AND (created >= \''.$date_from.'\' AND created <= \''.$date_to.'\') ';
			}
			else if(!empty($date_from))
			{
				$search_invoice_add = ' AND created = \''.$date_from.'\'';
			}
			else if(!empty($date_to))
			{
				$search_invoice_add = ' AND created = \''.$date_to.'\'';
			}
		}
		else
		{
			$search_invoice_add =  ' AND (created >= \''.date("Y-01-01").'\' AND created <= \''.date("Y-m-d").'\') ';

		}
		//retrieve all users
		$this->db->from('creditor_invoice_item');
		$this->db->select('SUM(vat_amount) AS total_amount');
		$this->db->where('vat_type_id = 2 AND vat_amount > 0 '.$search_invoice_add);
		// $this->db->group_by('accountId');
		$query = $this->db->get();
		$query_row = $query->row();
		$total_invoices_balance = $query_row->total_amount;


		return $total_invoices_balance;

	}


	public function get_tax_total_vat_tax()
	{

		$search_status = $this->session->userdata('tax_search');
		$search_payments_add = '';
		$search_invoice_add = '';
		if($search_status == 1)
		{
			$date_from = $this->session->userdata('date_from_tax');
			$date_to = $this->session->userdata('date_to_tax');

			if(!empty($date_from) AND !empty($date_to))
			{
				$search_invoice_add =  ' AND (created >= \''.$date_from.'\' AND created <= \''.$date_to.'\') ';
			}
			else if(!empty($date_from))
			{
				$search_invoice_add = ' AND created = \''.$date_from.'\'';
			}
			else if(!empty($date_to))
			{
				$search_invoice_add = ' AND created = \''.$date_to.'\'';
			}
		}
		else
		{
			$search_invoice_add =  ' AND (created >= \''.date("Y-01-01").'\' AND created <= \''.date("Y-m-d").'\') ';

		}
		//retrieve all users
		$this->db->from('creditor_invoice_item');
		$this->db->select('SUM(vat_amount) AS total_amount');
		$this->db->where('vat_type_id = 1 AND vat_amount > 0 '.$search_invoice_add);
		// $this->db->group_by('accountId');
		$query = $this->db->get();
		$query_row = $query->row();
		$total_invoices_balance = $query_row->total_amount;




		return $total_invoices_balance;

	}
	public function get_all_vendors()
	{
		$this->db->from('creditor');
		$this->db->select('*');
		$this->db->where('creditor_id > 0');
		$query = $this->db->get();

		return $query;
	}

	public function get_creditor_expenses($creditor_id)
	{

		$search_status = $this->session->userdata('vendor_expense_search');
		$search_payments_add = '';
		$search_invoice_add = '';
		if($search_status == 1)
		{
			$date_from = $this->session->userdata('date_from_vendor_expense');
			$date_to = $this->session->userdata('date_to_vendor_expense');

			if(!empty($date_from) AND !empty($date_to))
			{
				$search_invoice_add =  ' AND (transactionDate >= \''.$date_from.'\' AND transactionDate <= \''.$date_to.'\') ';
			}
			else if(!empty($date_from))
			{
				$search_invoice_add = ' AND transactionDate = \''.$date_from.'\'';
			}
			else if(!empty($date_to))
			{
				$search_invoice_add = ' AND transactionDate = \''.$date_to.'\'';
			}
		}
		else
		{
			$search_invoice_add =  ' AND (transactionDate >= \''.date("Y-01-01").'\' AND transactionDate <= \''.date("Y-m-d").'\') ';

		}
		//retrieve all users
		$this->db->from('v_general_ledger');
		$this->db->select('(SUM(dr_amount) - SUM(cr_amount)) AS total_amount');
		$this->db->where('recepientId = '.$creditor_id.''.$search_invoice_add);
		// $this->db->group_by('accountId');
		$query = $this->db->get();
		$query_row = $query->row();
		$total_invoices_balance = $query_row->total_amount;


		return $total_invoices_balance ;
	}



	public function get_all_visit_types()
	{
		$this->db->from('visit_type');
		$this->db->select('*');
		$this->db->where('visit_type_id > 0');
		$query = $this->db->get();

		return $query;
	}

	public function get_receivable_balances($visit_type_id)
	{
		$search_status = $this->session->userdata('customer_income_search');
		$search_payments_add = '';
		$search_invoice_add = '';
		if($search_status == 1)
		{
			$date_from = $this->session->userdata('date_from_customer_income');
			$date_to = $this->session->userdata('date_to_customer_income');

			if(!empty($date_from) AND !empty($date_to))
			{
				$search_invoice_add =  ' AND (transaction_date >= \''.$date_from.'\' AND transaction_date <= \''.$date_to.'\') ';
			}
			else if(!empty($date_from))
			{
				$search_invoice_add = ' AND transaction_date = \''.$date_from.'\'';
			}
			else if(!empty($date_to))
			{
				$search_invoice_add = ' AND transaction_date = \''.$date_to.'\'';
			}
		}
		else
		{
			$search_invoice_add = ' AND transaction_date = \''.date('Y-m-d').'\'';

		}

		//retrieve all users
		$this->db->from('v_transactions');
		$this->db->select('SUM(dr_amount) AS total_amount');
		$this->db->where('payment_type = '.$visit_type_id.'  AND party = "Patient" '.$search_invoice_add);
		// $this->db->group_by('accountId');
		$query = $this->db->get();
		$query_row = $query->row();
		$total_invoices_balance = $query_row->total_amount;



		return $total_invoices_balance;

	}

	public function get_total_receivable_wht_tax()
	{
		//retrieve all users
		$this->db->from('lease_invoice,invoice');
		$this->db->select('SUM(tax_amount) AS total_amount');
		$this->db->where('tax_amount > 0 AND lease_invoice.lease_invoice_id = invoice.lease_invoice_id');
		// $this->db->group_by('accountId');
		$query = $this->db->get();
		$query_row = $query->row();
		$total_invoices_balance = $query_row->total_amount;


		return $total_invoices_balance;

	}

	public function get_opening_stock()
	{

		$search_status = $this->session->userdata('income_statement_search');
		$search_payments_add = '';
		$search_invoice_add = '';
		if($search_status == 1)
		{
			$date_from = $this->session->userdata('date_from_income_statement');
			$date_to = $this->session->userdata('date_to_income_statement');

			if(!empty($date_from) AND !empty($date_to))
			{
				$start_date = date('Y-m-01', strtotime($date_from));
				$search_invoice_add =  '  AND transactionDate < \''.$start_date.'\' ';
			}
			else if(!empty($date_from))
			{
				$search_invoice_add = ' AND transactionDate < \''.$date_from.'\'';
			}
			else if(!empty($date_to))
			{
				$search_invoice_add = ' AND transactionDate < \''.$date_to.'\'';
			}
		}
		else
		{
			$search_invoice_add = '';

		}
		$table = 'v_product_stock';
		$where = '(v_product_stock.transactionClassification = "Product Opening Stock" OR  v_product_stock.transactionClassification = "Supplier Purchases" OR  v_product_stock.transactionClassification = "Product Addition" OR v_product_stock.transactionClassification = "Supplier Credit Note" OR v_product_stock.transactionClassification = "Product Deductions" OR v_product_stock.transactionClassification = "Drug Sales")  '.$search_invoice_add;
		$this->db->select('SUM(dr_amount) - SUM(cr_amount)  AS starting_value');
		$this->db->where($where);
		$query = $this->db->get($table);


		$result = $query->row();

		$starting_value  =0 ;
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$starting_value = $value->starting_value;
			}
		}



		return  $starting_value;
	}



	public function get_product_balance_brought_forward()
	{

		$search_status = $this->session->userdata('income_statement_search');
		$search_payments_add = '';
		$search_invoice_add = '';
		if($search_status == 1)
		{
			$date_from = $this->session->userdata('date_from_income_statement');
			$date_to = $this->session->userdata('date_to_income_statement');

			if(!empty($date_from) AND !empty($date_to))
			{
				$start_date = date('Y-m-01', strtotime($date_from));
				$search_invoice_add =  '  AND transactionDate < \''.$start_date.'\' ';
			}
			else if(!empty($date_from))
			{
				$search_invoice_add = ' AND transactionDate < \''.$date_from.'\'';
			}
			else if(!empty($date_to))
			{
				$search_invoice_add = ' AND transactionDate < \''.$date_to.'\'';
			}
		}
		else
		{
			$search_invoice_add = '';

		}
		$table = 'v_product_stock';
		$where = '(v_product_stock.transactionClassification = "Product Opening Stock" OR  v_product_stock.transactionClassification = "Supplier Purchases" OR  v_product_stock.transactionClassification = "Product Addition" OR v_product_stock.transactionClassification = "Supplier Credit Note" OR v_product_stock.transactionClassification = "Product Deductions" OR v_product_stock.transactionClassification = "Drug Sales")  '.$search_invoice_add;
		$this->db->select('SUM(dr_amount) AS dr_amount, SUM(cr_amount)  AS cr_amount, SUM(dr_quantity) AS dr_quantity,SUM(cr_quantity) AS cr_quantity');
		$this->db->where($where);
		$query = $this->db->get($table);



		return  $query;
	}
	public function get_product_purchases($start_date=null)
	{
		if($start_date == NULL)
		{
			$start_date = date('Y-m-d');
		}


		$search_status = $this->session->userdata('income_statement_search');
		$search_payments_add = '';
		$search_invoice_add = '';
		if($search_status == 1)
		{
			$date_from = $this->session->userdata('date_from_income_statement');
			$date_to = $this->session->userdata('date_to_income_statement');

			if(!empty($date_from) AND !empty($date_to))
			{
				$search_invoice_add =  ' AND (transactionDate >= \''.$date_from.'\' AND transactionDate <= \''.$date_to.'\') ';
			}
			else if(!empty($date_from))
			{
				$search_invoice_add = ' AND transactionDate = \''.$date_from.'\'';
			}
			else if(!empty($date_to))
			{
				$search_invoice_add = ' AND transactionDate = \''.$date_to.'\'';
			}
		}
		else
		{
			$search_invoice_add = '';

		}

		$table = 'v_product_stock';
		$where = '(v_product_stock.transactionClassification = "Product Opening Stock" OR  v_product_stock.transactionClassification = "Supplier Purchases" )  '.$search_invoice_add;
		$this->db->select('SUM(dr_amount) - SUM(cr_amount)  AS starting_value');
		$this->db->where($where);
		$query = $this->db->get($table);


		$result = $query->row();

		$starting_value  =0 ;
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$starting_value = $value->starting_value;
			}
		}
		return  $starting_value;
	}



	public function get_product_other_purchases($start_date=null)
	{
		if($start_date == NULL)
		{
			$start_date = date('Y-m-d');
		}


		$search_status = $this->session->userdata('income_statement_search');
		$search_payments_add = '';
		$search_invoice_add = '';
		if($search_status == 1)
		{
			$date_from = $this->session->userdata('date_from_income_statement');
			$date_to = $this->session->userdata('date_to_income_statement');

			if(!empty($date_from) AND !empty($date_to))
			{
				$search_invoice_add =  ' AND (transactionDate >= \''.$date_from.'\' AND transactionDate <= \''.$date_to.'\') ';
			}
			else if(!empty($date_from))
			{
				$search_invoice_add = ' AND transactionDate = \''.$date_from.'\'';
			}
			else if(!empty($date_to))
			{
				$search_invoice_add = ' AND transactionDate = \''.$date_to.'\'';
			}
		}
		else
		{
			$search_invoice_add = '';

		}

		$table = 'v_product_stock';
		$where = '(v_product_stock.transactionClassification = "Product Addition" )  '.$search_invoice_add;
		$this->db->select('SUM(dr_amount)  AS starting_value');
		$this->db->where($where);
		$query = $this->db->get($table);


		$result = $query->row();

		$starting_value  =0 ;
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$starting_value = $value->starting_value;
			}
		}
		return  $starting_value;
	}


	public function get_product_return_outwards($start_date=null)
	{
		if($start_date == NULL)
		{
			$start_date = date('Y-m-d');
		}


		$search_status = $this->session->userdata('income_statement_search');
		$search_payments_add = '';
		$search_invoice_add = '';
		if($search_status == 1)
		{
			$date_from = $this->session->userdata('date_from_income_statement');
			$date_to = $this->session->userdata('date_to_income_statement');

			if(!empty($date_from) AND !empty($date_to))
			{
				$search_invoice_add =  ' AND (transactionDate >= \''.$date_from.'\' AND transactionDate <= \''.$date_to.'\') ';
			}
			else if(!empty($date_from))
			{
				$search_invoice_add = ' AND transactionDate = \''.$date_from.'\'';
			}
			else if(!empty($date_to))
			{
				$search_invoice_add = ' AND transactionDate = \''.$date_to.'\'';
			}
		}
		else
		{
			$search_invoice_add = '';

		}

		$table = 'v_product_stock';
		$where = '(v_product_stock.transactionClassification = "Supplier Credit Note" )  '.$search_invoice_add;
		$this->db->select('SUM(cr_amount)  AS starting_value');
		$this->db->where($where);
		$query = $this->db->get($table);


		$result = $query->row();

		$starting_value  =0 ;
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$starting_value = $value->starting_value;
			}
		}
		return  $starting_value;
	}


	public function get_total_other_deductions($start_date=null)
	{
		if($start_date == NULL)
		{
			$start_date = date('Y-m-d');
		}


		$search_status = $this->session->userdata('income_statement_search');
		$search_payments_add = '';
		$search_invoice_add = '';
		if($search_status == 1)
		{
			$date_from = $this->session->userdata('date_from_income_statement');
			$date_to = $this->session->userdata('date_to_income_statement');

			if(!empty($date_from) AND !empty($date_to))
			{
				$search_invoice_add =  ' AND (transactionDate >= \''.$date_from.'\' AND transactionDate <= \''.$date_to.'\') ';
			}
			else if(!empty($date_from))
			{
				$search_invoice_add = ' AND transactionDate = \''.$date_from.'\'';
			}
			else if(!empty($date_to))
			{
				$search_invoice_add = ' AND transactionDate = \''.$date_to.'\'';
			}
		}
		else
		{
			$search_invoice_add = '';

		}

		$table = 'v_product_stock';
		$where = '(v_product_stock.transactionClassification = "Product Deductions")  '.$search_invoice_add;
		$this->db->select('SUM(cr_amount)  AS starting_value');
		$this->db->where($where);
		$query = $this->db->get($table);


		$result = $query->row();

		$starting_value  =0 ;
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$starting_value = $value->starting_value;
			}
		}
		return  $starting_value;
	}
	public function get_product_sales($start_date=null)
	{
		if($start_date == NULL)
		{
			$start_date = date('Y-m-d');
		}


		$search_status = $this->session->userdata('income_statement_search');
		$search_payments_add = '';
		$search_invoice_add = '';
		if($search_status == 1)
		{
			$date_from = $this->session->userdata('date_from_income_statement');
			$date_to = $this->session->userdata('date_to_income_statement');

			if(!empty($date_from) AND !empty($date_to))
			{
				$search_invoice_add =  ' AND (transactionDate >= \''.$date_from.'\' AND transactionDate <= \''.$date_to.'\') ';
			}
			else if(!empty($date_from))
			{
				$search_invoice_add = ' AND transactionDate = \''.$date_from.'\'';
			}
			else if(!empty($date_to))
			{
				$search_invoice_add = ' AND transactionDate = \''.$date_to.'\'';
			}
		}
		else
		{
			$search_invoice_add = '';

		}

		$table = 'v_product_stock';
		$where = '(v_product_stock.transactionClassification = "Drug Sales")  '.$search_invoice_add;
		$this->db->select('SUM(cr_amount)  AS starting_value');
		$this->db->where($where);
		$query = $this->db->get($table);


		$result = $query->row();

		$starting_value  =0 ;
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$starting_value = $value->starting_value;
			}
		}
		return  $starting_value;
	}


	public function get_closing_stock_old()
	{

		$table = 'product,store_product';
		$where = 'product.product_status = 1 AND product.product_deleted = 0 AND product.product_id = store_product.product_id';
		$this->db->select('SUM((store_product.store_quantity * product.product_unitprice)) AS starting_value');
		$this->db->where($where);
		$query = $this->db->get($table);


		$result = $query->row();

		$starting_value  =0 ;
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$starting_value = $value->starting_value;
			}
		}

		$inventory_start_date = $this->company_financial_model->get_inventory_start_date();
		$sales_value = $this->company_financial_model->get_prev_drug_units_sold_value($inventory_start_date);
		$procurred_amount = $this->company_financial_model->get_prev_total_purchases();

		return ($procurred_amount + $starting_value) - $sales_value;
	}


	public function get_stock_value()
	{
		$inventory_start_date = $this->company_financial_model->get_inventory_start_date();

		$sales_value = $this->company_financial_model->get_drug_units_sold_value($inventory_start_date);
		// $procurred_amount = $this->company_financial_model->get_total_purchases();

		return $sales_value;
	}

	public function get_prev_drug_units_sold_value($inventory_start_date, $product_id=NULL, $start_date = NULL, $end_date = NULL, $branch_code = NULL)
	{

		$search_status = $this->session->userdata('income_statement_search');
		$search_payments_add = '';
		$search_invoice_add = '';
		if($search_status == 1)
		{
			$date_from = $this->session->userdata('date_from_income_statement');
			$date_to = $this->session->userdata('date_to_income_statement');

			if(!empty($date_from) AND !empty($date_to))
			{
				$search_invoice_add =  ' AND (visit_charge.date >= \''.$date_from.'\' AND visit_charge.date <= \''.$date_to.'\') ';
			}
			else if(!empty($date_from))
			{
				$search_invoice_add = ' AND visit_charge.date = \''.$date_from.'\'';
			}
			else if(!empty($date_to))
			{
				$search_invoice_add = ' AND visit_charge.date = \''.$date_to.'\'';
			}
		}
		else
		{
			$search_invoice_add = '';

		}

		$table = "visit_charge, service_charge,product";
		$where = 'visit_charge.service_charge_id = service_charge.service_charge_id AND visit_charge.charged = 1 AND service_charge.product_id > 0 AND visit_charge.visit_charge_delete = 0 AND product.product_id = service_charge.product_id AND product.product_deleted = 0 '.$search_invoice_add;



		$items = "SUM((visit_charge.visit_charge_units * visit_charge.visit_charge_amount)) AS amount";
		$order = "date";

		$result = $this->database->select_entries_where($table, $where, $items, $order);
		$total_sold = 0;
		if(count($result) > 0)
		{
			foreach ($result as $key) {
				# code...
				$amount = $key->amount;

				$total_sold =$amount;
			}
		}
		return $total_sold;
	}

	public function get_inventory_start_date()
	{
		$this->db->where('branch_code', $this->session->userdata('branch_code'));
		$query = $this->db->get('branch');

		$inventory_start_date = '';
		if($query->num_rows() > 0)
		{
			$row = $query->row();
			$inventory_start_date = $row->inventory_start_date;
		}

		return $inventory_start_date;
	}

	public function get_total_purchases_old($start_date=null)
	{
		if($start_date == NULL)
		{
			$start_date = date('Y-m-d');
		}


		$search_status = $this->session->userdata('income_statement_search');
		$search_payments_add = '';
		$search_invoice_add = '';
		if($search_status == 1)
		{
			$date_from = $this->session->userdata('date_from_income_statement');
			$date_to = $this->session->userdata('date_to_income_statement');

			if(!empty($date_from) AND !empty($date_to))
			{
				$search_invoice_add =  ' AND (orders.supplier_invoice_date >= \''.$date_from.'\' AND orders.supplier_invoice_date <= \''.$date_to.'\') ';
			}
			else if(!empty($date_from))
			{
				$search_invoice_add = ' AND orders.supplier_invoice_date = \''.$date_from.'\'';
			}
			else if(!empty($date_to))
			{
				$search_invoice_add = ' AND orders.supplier_invoice_date = \''.$date_to.'\'';
			}
		}
		else
		{
			$search_invoice_add = '';

		}

		// $table = 'v_general_ledger';
		// $where = '(transactionCategory = "Expense" AND accountName="Catering Expenses") '.$search_invoice_add;
		// $this->db->select('SUM(dr_amount) AS total_amount');
		// $this->db->where($where);
		// $query = $this->db->get($table);
		// $row = $query->row();
		//
		// $total_value = $row->total_amount;

		$table = 'order_supplier,orders,order_item';
		$where = 'invoice_number <> "" AND orders.order_id = order_supplier.order_id AND orders.supplier_id > 0 AND orders.order_approval_status = 7 AND order_supplier.order_item_id = order_item.order_item_id AND orders.supplier_invoice_date <> "0000-00-00" '.$search_invoice_add;
		$this->db->select('SUM(order_supplier.less_vat) AS total_amount');
		$this->db->where($where);
		$query = $this->db->get($table);
		$row = $query->row();

		$total_value = $row->total_amount;

		return $total_value;
	}

	public function get_prev_total_purchases($start_date=null)
	{


		$search_status = $this->session->userdata('income_statement_search');
		$search_payments_add = '';
		$search_invoice_add = '';
		if($search_status == 1)
		{
			$date_from = $this->session->userdata('date_from_income_statement');
			$date_to = $this->session->userdata('date_to_income_statement');


			if(!empty($date_from))
			{
				$search_invoice_add = ' AND orders.supplier_invoice_date < \''.$date_from.'\'';
			}
			else if(!empty($date_to))
			{
				$search_invoice_add = ' AND orders.supplier_invoice_date < \''.$date_to.'\'';
			}
		}
		else
		{
			$search_invoice_add = '';

		}

		$table = 'order_supplier,orders,order_item';
		$where = 'invoice_number <> "" AND orders.order_id = order_supplier.order_id AND orders.supplier_id > 0 AND orders.order_approval_status = 7 AND order_supplier.order_item_id = order_item.order_item_id AND orders.supplier_invoice_date <> "0000-00-00" '.$search_invoice_add;
		$this->db->select('SUM(order_supplier.less_vat) AS total_amount');
		$this->db->where($where);
		$query = $this->db->get($table);
		$row = $query->row();

		$total_value = $row->total_amount;

		return $total_value;
	}

	public function get_drug_units_sold_value($inventory_start_date, $product_id=NULL, $start_date = NULL, $end_date = NULL, $branch_code = NULL)
	{

		$search_status = $this->session->userdata('income_statement_search');
		$search_payments_add = '';
		$search_invoice_add = '';
		if($search_status == 1)
		{
			$date_from = $this->session->userdata('date_from_income_statement');
			$date_to = $this->session->userdata('date_to_income_statement');

			if(!empty($date_from) AND !empty($date_to))
			{
				$search_invoice_add =  ' AND (visit_charge.date >= \''.$date_from.'\' AND visit_charge.date <= \''.$date_to.'\') ';
			}
			else if(!empty($date_from))
			{
				$search_invoice_add = ' AND visit_charge.date = \''.$date_from.'\'';
			}
			else if(!empty($date_to))
			{
				$search_invoice_add = ' AND visit_charge.date = \''.$date_to.'\'';
			}
		}
		else
		{
			$search_invoice_add = '';

		}

		$table = "visit_charge, service_charge,product";
		$where = 'visit_charge.service_charge_id = service_charge.service_charge_id AND visit_charge.charged = 1 AND service_charge.product_id > 0 AND visit_charge.visit_charge_delete = 0 AND product.product_id = service_charge.product_id AND product.product_deleted = 0 '.$search_invoice_add;



		$items = "SUM((visit_charge.visit_charge_units * visit_charge.visit_charge_amount)) AS amount";
		$order = "date";

		$result = $this->database->select_entries_where($table, $where, $items, $order);
		$total_sold = 0;
		if(count($result) > 0)
		{
			foreach ($result as $key) {
				# code...
				$amount = $key->amount;

				$total_sold =$amount;
			}
		}
		return $total_sold;
	}

	/*
	*	Retrieve all creditor
	*	@param string $table
	* 	@param string $where
	*
	*/
	public function get_all_service_bills($table, $where, $per_page, $page, $order = 'visit.visit_date', $order_method = 'ASC')
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by($order, $order_method);
		$this->db->join('personnel','personnel.personnel_id = visit_charge.provider_id','left');
		$query = $this->db->get('', $per_page, $page);

		return $query;
	}

	function export_services_bills($service_id)
	{
		$this->load->library('excel');



		$this->db->where('visit_charge.visit_id = visit.visit_id AND visit.patient_id = patients.patient_id AND visit.visit_type = visit_type.visit_type_id AND visit_charge.service_charge_id = service_charge.service_charge_id AND visit.visit_delete = 0 AND visit_charge.visit_charge_delete = 0 AND visit_charge.charged = 1 AND service_charge.service_id ='.$service_id);
		$this->db->select('*');
		$this->db->join('personnel','visit_charge.personnel_id = personnel.personnel_id','left');
		$table = 'visit_charge,visit,patients,service_charge,visit_type';
		$visits_query = $this->db->get($table);

		$title = 'Service Bill Export '.date('jS M Y H:i a',strtotime(date('Y-m-d H:i:s')));
		$col_count = 0;

		if($visits_query->num_rows() > 0)
		{
			$count = 0;
			/*
				-----------------------------------------------------------------------------------------
				Document Header
				-----------------------------------------------------------------------------------------
			*/
			$row_count = 0;
			$report[$row_count][$col_count] = '#';
			$col_count++;
			$report[$row_count][$col_count] = 'Invoice Date';
			$col_count++;
			$report[$row_count][$col_count] = 'Name';
			$col_count++;
			$report[$row_count][$col_count] = 'Category';
			$col_count++;
			$current_column = $col_count ;

			$report[$row_count][$current_column] = 'Provider';
			$current_column++;
			$report[$row_count][$current_column] = 'Units';
			$current_column++;
			$report[$row_count][$current_column] = 'Charge Amount';
			$current_column++;
			//display all patient data in the leftmost columns
			foreach($visits_query->result() as $row)
			{
				$row_count++;
					$visit_date = date('jS M Y',strtotime($row->visit_date));
				$visit_time = date('H:i a',strtotime($row->visit_time));

				if($row->visit_time_out != '0000-00-00 00:00:00')
				{
					$visit_time_out = date('H:i a',strtotime($row->visit_time_out));
				}
				else
				{
					$visit_time_out = '-';
				}

				$visit_id = $row->visit_id;
				$date = $row->date;
				$invoice_date = date('jS M Y',strtotime($row->date));
				$patient_id = $row->patient_id;
				$personnel_id = $row->personnel_id;
				$dependant_id = $row->dependant_id;
				$strath_no = $row->strath_no;
				$visit_type_id = $row->visit_type;
				$patient_number = $row->patient_number;
				$visit_type = $row->visit_type;
				$visit_table_visit_type = $visit_type;
				$patient_table_visit_type = $visit_type_id;
				$rejected_amount = $row->amount_rejected;
				$invoice_number = $row->invoice_number;
				$visit_charge_amount = $row->visit_charge_amount;
				$visit_charge_units = $row->visit_charge_units;
				$visit_type_name = $row->visit_type_name;
				$personnel = $row->personnel_fname.' '.$row->personnel_onames;


				$count++;

				//display services charged to patient
				$total_invoiced2 = 0;


				//display the patient data
				$report[$row_count][$col_count] = $count;
				$col_count++;
				$report[$row_count][$col_count] = $invoice_date;
				$col_count++;
				$report[$row_count][$col_count] = $row->patient_surname.' '.$row->patient_othernames;
				$col_count++;
				$report[$row_count][$col_count] = $visit_type_name;
				$col_count++;
				$current_column = $col_count;


				$report[$row_count][$current_column] = $personnel;
				$current_column++;
				$report[$row_count][$current_column] = $visit_charge_units;
				$current_column++;
				$report[$row_count][$current_column] = round($visit_charge_amount);
				$current_column++;
				$report[$row_count][$current_column] = round($visit_charge_amount * $visit_charge_units);
				$current_column++;

			}
		}

		//create the excel document
		$this->excel->addArray ( $report );
		$this->excel->generateXML ($title);
	}
	public function get_account_name($from_account_id)
	{
		$account_name = '';
		$this->db->select('account_name');
		$this->db->where('account_id = '.$from_account_id);
		$query = $this->db->get('account');

		$account_details = $query->row();
		$account_name = $account_details->account_name;

		return $account_name;
	}
	public function get_expense_ledger($account_id)
	{
		$search_status = $this->session->userdata('income_statement_search');
		$search_payments_add = '';
		$search_invoice_add = '';
		if($search_status == 1)
		{
			$date_from = $this->session->userdata('date_from_income_statement');
			$date_to = $this->session->userdata('date_to_income_statement');

			if(!empty($date_from) AND !empty($date_to))
			{
				$search_invoice_add =  ' AND (transactionDate >= \''.$date_from.'\' AND transactionDate <= \''.$date_to.'\') ';
			}
			else if(!empty($date_from))
			{
				$search_invoice_add = ' AND transactionDate = \''.$date_from.'\'';
			}
			else if(!empty($date_to))
			{
				$search_invoice_add = ' AND transactionDate = \''.$date_to.'\'';
			}
		}
		else
		{
			$search_invoice_add = '';

		}
		//retrieve all users
		$this->db->from('v_general_ledger');
		$this->db->select('dr_amount AS total_amount,v_general_ledger.*');
		$this->db->where('transactionCategory = "Expense" AND accountId = '.$account_id.'  '.$search_invoice_add);
		$query = $this->db->get();

		return $query;
	}



	public function get_accounts_ledger($account_id)
	{
		$search_status = $this->session->userdata('balance_sheet_search');
		$search_payments_add = '';
		$search_invoice_add = '';
		if($search_status == 1)
		{
			$date_from = $this->session->userdata('date_from_balance_sheet');
			$date_to = $this->session->userdata('date_to_balance_sheet');

			if(!empty($date_from) AND !empty($date_to))
			{
				$search_invoice_add =  ' AND (transactionDate >= \''.$date_from.'\' AND transactionDate <= \''.$date_to.'\') ';
			}
			else if(!empty($date_from))
			{
				$search_invoice_add = ' AND transactionDate = \''.$date_from.'\'';
			}
			else if(!empty($date_to))
			{
				$search_invoice_add = ' AND transactionDate = \''.$date_to.'\'';
			}
		}
		else
		{
			$search_invoice_add = '';

		}
		//retrieve all users
		$this->db->from('v_general_ledger');
		$this->db->select('v_general_ledger.*');
		$this->db->where('accountsclassfication = "Bank" AND accountId = '.$account_id.'  '.$search_invoice_add);
		$query = $this->db->get();

		return $query;
	}


	public function get_account_opening_balance($account_id)
	{

		$this->db->from('account');
		$this->db->select('account.account_opening_balance AS total_amount');
		$this->db->where('account_id = '.$account_id);

		$query = $this->db->get();
		$balance = $query->row();

		$total_amount = $balance->total_amount;
		return $total_amount;
	}

	public function get_creditor_statement_balance($creditor_id,$start_date=null)
	{

		$search_status = $this->session->userdata('vendor_expense_search');
		$search_payments_add = '';
		$search_invoice_add = '';

		// var_dump($search_status);die();
		if($search_status == 1)
		{
			$date_from = $this->session->userdata('date_from_vendor_expense');
			$date_to = $this->session->userdata('date_to_vendor_expense');

			if(!empty($date_from) AND !empty($date_to))
			{
				$search_invoice_add =  ' AND (transactionDate < \''.$date_from.'\') ';
			}
			else if(!empty($date_from))
			{
				$search_invoice_add = ' AND transactionDate < \''.$date_from.'\'';
			}
			else if(!empty($date_to))
			{
				$search_invoice_add = ' AND transactionDate < \''.$date_to.'\'';
			}
		}
		else
		{
		
			$search_invoice_add =  '';

		}
		//retrieve all users
		$this->db->from('v_general_ledger_by_date,creditor');
		$this->db->select('SUM(`dr_amount`) AS dr_amount,SUM(cr_amount) AS cr_amount');
		$this->db->where('creditor.creditor_id = v_general_ledger_by_date.recepientId AND v_general_ledger_by_date.transactionDate >= creditor.start_date AND recepientId = '.$creditor_id.$search_invoice_add);
		$this->db->order_by('transactionDate','ASC');
		$query = $this->db->get();



		return $query;
	}


	public function get_creditor_statement($creditor_id,$start_date=null)
	{

		$search_status = $this->session->userdata('vendor_expense_search');
		$search_payments_add = '';
		$search_invoice_add = '';

		// var_dump($search_status);die();
		if($search_status == 1)
		{
			$date_from = $this->session->userdata('date_from_vendor_expense');
			$date_to = $this->session->userdata('date_to_vendor_expense');

			if(!empty($date_from) AND !empty($date_to))
			{
				$search_invoice_add =  ' AND (transactionDate >= \''.$date_from.'\' AND transactionDate <= \''.$date_to.'\') ';
			}
			else if(!empty($date_from))
			{
				$search_invoice_add = ' AND transactionDate = \''.$date_from.'\'';
			}
			else if(!empty($date_to))
			{
				$search_invoice_add = ' AND transactionDate = \''.$date_to.'\'';
			}
		}
		else
		{
		
			$search_invoice_add =  '';

		}
		//retrieve all users
		$this->db->from('v_general_ledger_by_date,creditor');
		$this->db->select('*');
		$this->db->where('creditor.creditor_id = v_general_ledger_by_date.recepientId AND v_general_ledger_by_date.transactionDate >= creditor.start_date AND recepientId = '.$creditor_id.$search_invoice_add);
		$this->db->order_by('transactionDate','ASC');
		$query = $this->db->get();



		return $query;
	}

	/*
	*	get a single creditor's details
	*	@param int $creditor_id
	*
	*/
	public function get_creditor($creditor_id)
	{
		//retrieve all users
		$this->db->from('creditor');
		$this->db->select('*');
		$this->db->where('creditor_id = '.$creditor_id);
		$query = $this->db->get();

		return $query;
	}

	/*
	*	Retrieve all creditor
	*	@param string $table
	* 	@param string $where
	*
	*/
	public function get_all_income_statement_views($table, $where, $per_page, $page, $order = 'v_product_stock.transactionDate', $order_method = 'ASC')
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by($order, $order_method);
		// $this->db->join('personnel','personnel.personnel_id = visit_charge.provider_id','left');
		$query = $this->db->get('', $per_page, $page);

		return $query;
	}


	public function get_salary_expenses()
	{
		$search_status = $this->session->userdata('income_statement_search');
		$search_payments_add = '';
		$search_invoice_add = '';
		if($search_status == 1)
		{
			$date_from = $this->session->userdata('date_from_income_statement');
			$date_to = $this->session->userdata('date_to_income_statement');

			if(!empty($date_from) AND !empty($date_to))
			{
				$search_invoice_add =  ' AND (payroll_created_for >= \''.$date_from.'\' AND payroll_created_for <= \''.$date_to.'\') ';
			}
			else if(!empty($date_from))
			{
				$search_invoice_add = ' AND payroll_created_for = \''.$date_from.'\'';
			}
			else if(!empty($date_to))
			{
				$search_invoice_add = ' AND payroll_created_for = \''.$date_to.'\'';
			}
		}
		else
		{
			$search_invoice_add = '';

		}
		//retrieve all users
		$this->db->from('v_payroll');
		$this->db->select('SUM(total_additions) AS total_amount');
		$this->db->where('payroll_id > 0 '.$search_invoice_add);
		// $this->db->group_by('accountId');
		$total_amount = 0;
		$query = $this->db->get();
		if($query->num_rows() > 0 )
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$total_amount = $value->total_amount;
			}
		}

		return $total_amount;
	}


	public function get_statutories($statutory_id)
	{
		$search_status = $this->session->userdata('income_statement_search');
		$search_payments_add = '';
		$search_invoice_add = '';
		if($search_status == 1)
		{
			$date_from = $this->session->userdata('date_from_income_statement');
			$date_to = $this->session->userdata('date_to_income_statement');

			if(!empty($date_from) AND !empty($date_to))
			{
				$search_invoice_add =  ' AND (payroll_created_for >= \''.$date_from.'\' AND payroll_created_for <= \''.$date_to.'\') ';
			}
			else if(!empty($date_from))
			{
				$search_invoice_add = ' AND payroll_created_for = \''.$date_from.'\'';
			}
			else if(!empty($date_to))
			{
				$search_invoice_add = ' AND payroll_created_for = \''.$date_to.'\'';
			}
		}
		else
		{
			$search_invoice_add = '';

		}
		if($statutory_id == 1)
		{
			$column = 'total_nssf';
		}
		else if($statutory_id == 2)
		{
			$column = 'total_nhif';
		}
		else if($statutory_id == 3)
		{
			$column = 'total_paye';
		}
		else if($statutory_id == 4)
		{
			$column = 'total_relief';
		}
		//retrieve all users
		$this->db->from('v_payroll');
		$this->db->select('SUM('.$column.') AS total_amount');
		$this->db->where('payroll_id > 0 '.$search_invoice_add);
		// $this->db->group_by('accountId');
		$total_amount = 0;
		$query = $this->db->get();
		if($query->num_rows() > 0 )
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$total_amount = $value->total_amount;
			}
		}

		return $total_amount;
	}
}
?>
